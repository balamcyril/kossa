-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 24 Juin 2020 à 23:33
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kossa`
--

-- --------------------------------------------------------

--
-- Structure de la table `home_banniere`
--

CREATE TABLE `home_banniere` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lien` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `date_enreg` date NOT NULL,
  `random` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_commentaire_actualite`
--

CREATE TABLE `home_commentaire_actualite` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `actualite_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `contenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_commentaire_genre_musicaux`
--

CREATE TABLE `home_commentaire_genre_musicaux` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `genre_musicaux_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `contenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_commentaire_single`
--

CREATE TABLE `home_commentaire_single` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `single_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `contenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_commentaire_video`
--

CREATE TABLE `home_commentaire_video` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `contenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_favorie_album`
--

CREATE TABLE `home_favorie_album` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL,
  `id_temporaire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_favorie_artiste`
--

CREATE TABLE `home_favorie_artiste` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `artiste_id` int(11) NOT NULL,
  `ide_temporaire` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_favorie_genre_musicaux`
--

CREATE TABLE `home_favorie_genre_musicaux` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `genre_musicaux_id` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL,
  `id_temporaire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_favorie_single`
--

CREATE TABLE `home_favorie_single` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `single_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_favorie_video`
--

CREATE TABLE `home_favorie_video` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_actualite`
--

CREATE TABLE `home_jaime_actualite` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `actualite_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_album`
--

CREATE TABLE `home_jaime_album` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_artiste`
--

CREATE TABLE `home_jaime_artiste` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `artiste_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_genre_musicaux`
--

CREATE TABLE `home_jaime_genre_musicaux` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `genre_musicaux_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_pas_actualite`
--

CREATE TABLE `home_jaime_pas_actualite` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `actualite_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_pas_album`
--

CREATE TABLE `home_jaime_pas_album` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_pas_artiste`
--

CREATE TABLE `home_jaime_pas_artiste` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `artiste_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_pas_genre_musicaux`
--

CREATE TABLE `home_jaime_pas_genre_musicaux` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `genre_musicaux_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_pas_single`
--

CREATE TABLE `home_jaime_pas_single` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `single_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_pas_video`
--

CREATE TABLE `home_jaime_pas_video` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_single`
--

CREATE TABLE `home_jaime_single` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `single_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_jaime_video`
--

CREATE TABLE `home_jaime_video` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_pays`
--

CREATE TABLE `home_pays` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_replay_single`
--

CREATE TABLE `home_replay_single` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `single_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_replay_video`
--

CREATE TABLE `home_replay_video` (
  `id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `id_temporaire` int(11) NOT NULL,
  `date_enreg` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `home_utilisateur`
--

CREATE TABLE `home_utilisateur` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `home_utilisateur`
--

INSERT INTO `home_utilisateur` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `description`) VALUES
(1, 'admin', 'admin', 'grmcteam@gmail.com', 'grmcteam@gmail.com', 1, 'qQ9BgsM9AwxDevXOq7J/vAuPKVNoE1W5cb8Ty83c9Fc', 'jh44AqI8TJ5IJ+Oiwn9enaQHf8/N9X6VRSir9xBwZmsmV5b822oXrh4ThkiDMvW9kV3QE+EYPASiMPl3Q8n8yw==', '2020-05-10 18:35:43', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news_actualite`
--

CREATE TABLE `news_actualite` (
  `id` int(11) NOT NULL,
  `new_categorie_actualite_id` int(11) NOT NULL,
  `play_single_id` int(11) DEFAULT NULL,
  `play_video_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `date_de_publication` datetime NOT NULL,
  `date_enreg` datetime NOT NULL,
  `vue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news_categorie_actualite`
--

CREATE TABLE `news_categorie_actualite` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `couleur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `news_categorie_actualite`
--

INSERT INTO `news_categorie_actualite` (`id`, `nom`, `couleur`) VALUES
(1, 'Nouveautés', 'red'),
(2, 'News', 'blue'),
(3, 'Showbusiness', 'Orange'),
(4, 'Zoom', 'green'),
(5, 'Events', 'purple');

-- --------------------------------------------------------

--
-- Structure de la table `news_tag`
--

CREATE TABLE `news_tag` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `news_tag_actualite`
--

CREATE TABLE `news_tag_actualite` (
  `id` int(11) NOT NULL,
  `news_actualite_id` int(11) NOT NULL,
  `news_tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_album`
--

CREATE TABLE `play_album` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_artiste`
--

CREATE TABLE `play_artiste` (
  `id` int(11) NOT NULL,
  `home_pays_id` int(11) NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_genre_musicaux`
--

CREATE TABLE `play_genre_musicaux` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vue` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_single`
--

CREATE TABLE `play_single` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `fichier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extrait` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vue` int(11) NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_single_artiste`
--

CREATE TABLE `play_single_artiste` (
  `id` int(11) NOT NULL,
  `play_single_id` int(11) NOT NULL,
  `play_artiste_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_single_genre_musicaux`
--

CREATE TABLE `play_single_genre_musicaux` (
  `id` int(11) NOT NULL,
  `play_single_id` int(11) NOT NULL,
  `play_genre_musicaux_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_top_single`
--

CREATE TABLE `play_top_single` (
  `id` int(11) NOT NULL,
  `single1_id` int(11) NOT NULL,
  `single2_id` int(11) NOT NULL,
  `single3_id` int(11) NOT NULL,
  `single4_id` int(11) NOT NULL,
  `single5_id` int(11) NOT NULL,
  `single6_id` int(11) NOT NULL,
  `single7_id` int(11) NOT NULL,
  `single8_id` int(11) NOT NULL,
  `single9_id` int(11) NOT NULL,
  `single10_id` int(11) NOT NULL,
  `auteur_id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_top_video`
--

CREATE TABLE `play_top_video` (
  `id` int(11) NOT NULL,
  `video1_id` int(11) NOT NULL,
  `video2_id` int(11) NOT NULL,
  `video3_id` int(11) NOT NULL,
  `video4_id` int(11) NOT NULL,
  `video5_id` int(11) NOT NULL,
  `video6_id` int(11) NOT NULL,
  `video7_id` int(11) NOT NULL,
  `video8_id` int(11) NOT NULL,
  `video9_id` int(11) NOT NULL,
  `video10_id` int(11) NOT NULL,
  `auteur_id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_enreg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `play_video`
--

CREATE TABLE `play_video` (
  `id` int(11) NOT NULL,
  `play_artiste_id` int(11) NOT NULL,
  `play_single_id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fichier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lien` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_enreg` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `home_banniere`
--
ALTER TABLE `home_banniere`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `home_commentaire_actualite`
--
ALTER TABLE `home_commentaire_actualite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_394D92DFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_394D92DA2843073` (`actualite_id`);

--
-- Index pour la table `home_commentaire_genre_musicaux`
--
ALTER TABLE `home_commentaire_genre_musicaux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F3FC0DFFFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_F3FC0DFFD21AAC2D` (`genre_musicaux_id`);

--
-- Index pour la table `home_commentaire_single`
--
ALTER TABLE `home_commentaire_single`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_80E664C9FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_80E664C9E7C1D92B` (`single_id`);

--
-- Index pour la table `home_commentaire_video`
--
ALTER TABLE `home_commentaire_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E2214B48FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_E2214B4829C1004E` (`video_id`);

--
-- Index pour la table `home_favorie_album`
--
ALTER TABLE `home_favorie_album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F34C7443FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_F34C74431137ABCF` (`album_id`);

--
-- Index pour la table `home_favorie_artiste`
--
ALTER TABLE `home_favorie_artiste`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_616506E1FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_616506E121D25844` (`artiste_id`);

--
-- Index pour la table `home_favorie_genre_musicaux`
--
ALTER TABLE `home_favorie_genre_musicaux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_54C1BA41FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_54C1BA41D21AAC2D` (`genre_musicaux_id`);

--
-- Index pour la table `home_favorie_single`
--
ALTER TABLE `home_favorie_single`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CA6DF303FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_CA6DF303E7C1D92B` (`single_id`);

--
-- Index pour la table `home_favorie_video`
--
ALTER TABLE `home_favorie_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B613C02CFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_B613C02C29C1004E` (`video_id`);

--
-- Index pour la table `home_jaime_actualite`
--
ALTER TABLE `home_jaime_actualite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_90FE3BCBFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_90FE3BCBA2843073` (`actualite_id`);

--
-- Index pour la table `home_jaime_album`
--
ALTER TABLE `home_jaime_album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3AD7EFF8FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_3AD7EFF81137ABCF` (`album_id`);

--
-- Index pour la table `home_jaime_artiste`
--
ALTER TABLE `home_jaime_artiste`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_189F2C5FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_189F2C521D25844` (`artiste_id`);

--
-- Index pour la table `home_jaime_genre_musicaux`
--
ALTER TABLE `home_jaime_genre_musicaux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_3732211CFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_3732211CD21AAC2D` (`genre_musicaux_id`);

--
-- Index pour la table `home_jaime_pas_actualite`
--
ALTER TABLE `home_jaime_pas_actualite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_87608FB1FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_87608FB1A2843073` (`actualite_id`);

--
-- Index pour la table `home_jaime_pas_album`
--
ALTER TABLE `home_jaime_pas_album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_116524C8FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_116524C81137ABCF` (`album_id`);

--
-- Index pour la table `home_jaime_pas_artiste`
--
ALTER TABLE `home_jaime_pas_artiste`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D279F4BCFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_D279F4BC21D25844` (`artiste_id`);

--
-- Index pour la table `home_jaime_pas_genre_musicaux`
--
ALTER TABLE `home_jaime_pas_genre_musicaux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E44E11D3FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_E44E11D3D21AAC2D` (`genre_musicaux_id`);

--
-- Index pour la table `home_jaime_pas_single`
--
ALTER TABLE `home_jaime_pas_single`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B0E580FBFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_B0E580FBE7C1D92B` (`single_id`);

--
-- Index pour la table `home_jaime_pas_video`
--
ALTER TABLE `home_jaime_pas_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_543A90A7FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_543A90A729C1004E` (`video_id`);

--
-- Index pour la table `home_jaime_single`
--
ALTER TABLE `home_jaime_single`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9617029CFB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_9617029CE7C1D92B` (`single_id`);

--
-- Index pour la table `home_jaime_video`
--
ALTER TABLE `home_jaime_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7F885B97FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_7F885B9729C1004E` (`video_id`);

--
-- Index pour la table `home_pays`
--
ALTER TABLE `home_pays`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `home_replay_single`
--
ALTER TABLE `home_replay_single`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1B94E275FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_1B94E275E7C1D92B` (`single_id`);

--
-- Index pour la table `home_replay_video`
--
ALTER TABLE `home_replay_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F4654788FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_F465478829C1004E` (`video_id`);

--
-- Index pour la table `home_utilisateur`
--
ALTER TABLE `home_utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_A0D05EFE92FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_A0D05EFEA0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_A0D05EFEC05FB297` (`confirmation_token`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `news_actualite`
--
ALTER TABLE `news_actualite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_48B7DE564C4E591D` (`new_categorie_actualite_id`),
  ADD KEY `IDX_48B7DE56D0D2F576` (`play_single_id`),
  ADD KEY `IDX_48B7DE56E2D6ADA5` (`play_video_id`);

--
-- Index pour la table `news_categorie_actualite`
--
ALTER TABLE `news_categorie_actualite`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `news_tag`
--
ALTER TABLE `news_tag`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `news_tag_actualite`
--
ALTER TABLE `news_tag_actualite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1046D7F644CE59C4` (`news_actualite_id`),
  ADD KEY `IDX_1046D7F6367BA065` (`news_tag_id`);

--
-- Index pour la table `play_album`
--
ALTER TABLE `play_album`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `play_artiste`
--
ALTER TABLE `play_artiste`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EA1952FF13C4891C` (`home_pays_id`);

--
-- Index pour la table `play_genre_musicaux`
--
ALTER TABLE `play_genre_musicaux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `play_single`
--
ALTER TABLE `play_single`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `play_single_artiste`
--
ALTER TABLE `play_single_artiste`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_821D8EEDD0D2F576` (`play_single_id`),
  ADD KEY `IDX_821D8EED343F6621` (`play_artiste_id`);

--
-- Index pour la table `play_single_genre_musicaux`
--
ALTER TABLE `play_single_genre_musicaux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6CEA0358D0D2F576` (`play_single_id`),
  ADD KEY `IDX_6CEA035882670D71` (`play_genre_musicaux_id`);

--
-- Index pour la table `play_top_single`
--
ALTER TABLE `play_top_single`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_88B7B3F07A37150A` (`single1_id`),
  ADD KEY `IDX_88B7B3F06882BAE4` (`single2_id`),
  ADD KEY `IDX_88B7B3F0D03EDD81` (`single3_id`),
  ADD KEY `IDX_88B7B3F04DE9E538` (`single4_id`),
  ADD KEY `IDX_88B7B3F0F555825D` (`single5_id`),
  ADD KEY `IDX_88B7B3F0E7E02DB3` (`single6_id`),
  ADD KEY `IDX_88B7B3F05F5C4AD6` (`single7_id`),
  ADD KEY `IDX_88B7B3F073F5A80` (`single8_id`),
  ADD KEY `IDX_88B7B3F0BF833DE5` (`single9_id`),
  ADD KEY `IDX_88B7B3F08E7F94FD` (`single10_id`),
  ADD KEY `IDX_88B7B3F060BB6FE6` (`auteur_id`);

--
-- Index pour la table `play_top_video`
--
ALTER TABLE `play_top_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DECB5F0547218004` (`video1_id`),
  ADD KEY `IDX_DECB5F0555942FEA` (`video2_id`),
  ADD KEY `IDX_DECB5F05ED28488F` (`video3_id`),
  ADD KEY `IDX_DECB5F0570FF7036` (`video4_id`),
  ADD KEY `IDX_DECB5F05C8431753` (`video5_id`),
  ADD KEY `IDX_DECB5F05DAF6B8BD` (`video6_id`),
  ADD KEY `IDX_DECB5F05624ADFD8` (`video7_id`),
  ADD KEY `IDX_DECB5F053A29CF8E` (`video8_id`),
  ADD KEY `IDX_DECB5F058295A8EB` (`video9_id`),
  ADD KEY `IDX_DECB5F0569FAAF6F` (`video10_id`),
  ADD KEY `IDX_DECB5F0560BB6FE6` (`auteur_id`);

--
-- Index pour la table `play_video`
--
ALTER TABLE `play_video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8D183D90343F6621` (`play_artiste_id`),
  ADD KEY `IDX_8D183D90D0D2F576` (`play_single_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `home_banniere`
--
ALTER TABLE `home_banniere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_commentaire_actualite`
--
ALTER TABLE `home_commentaire_actualite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_commentaire_genre_musicaux`
--
ALTER TABLE `home_commentaire_genre_musicaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_commentaire_single`
--
ALTER TABLE `home_commentaire_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_commentaire_video`
--
ALTER TABLE `home_commentaire_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_favorie_album`
--
ALTER TABLE `home_favorie_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_favorie_artiste`
--
ALTER TABLE `home_favorie_artiste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_favorie_genre_musicaux`
--
ALTER TABLE `home_favorie_genre_musicaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_favorie_single`
--
ALTER TABLE `home_favorie_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_favorie_video`
--
ALTER TABLE `home_favorie_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_actualite`
--
ALTER TABLE `home_jaime_actualite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_album`
--
ALTER TABLE `home_jaime_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_artiste`
--
ALTER TABLE `home_jaime_artiste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_genre_musicaux`
--
ALTER TABLE `home_jaime_genre_musicaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_pas_actualite`
--
ALTER TABLE `home_jaime_pas_actualite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_pas_album`
--
ALTER TABLE `home_jaime_pas_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_pas_artiste`
--
ALTER TABLE `home_jaime_pas_artiste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_pas_genre_musicaux`
--
ALTER TABLE `home_jaime_pas_genre_musicaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_pas_single`
--
ALTER TABLE `home_jaime_pas_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_pas_video`
--
ALTER TABLE `home_jaime_pas_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_single`
--
ALTER TABLE `home_jaime_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_jaime_video`
--
ALTER TABLE `home_jaime_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_pays`
--
ALTER TABLE `home_pays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_replay_single`
--
ALTER TABLE `home_replay_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_replay_video`
--
ALTER TABLE `home_replay_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `home_utilisateur`
--
ALTER TABLE `home_utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `news_actualite`
--
ALTER TABLE `news_actualite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `news_categorie_actualite`
--
ALTER TABLE `news_categorie_actualite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `news_tag`
--
ALTER TABLE `news_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `news_tag_actualite`
--
ALTER TABLE `news_tag_actualite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_album`
--
ALTER TABLE `play_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_artiste`
--
ALTER TABLE `play_artiste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_genre_musicaux`
--
ALTER TABLE `play_genre_musicaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_single`
--
ALTER TABLE `play_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_single_artiste`
--
ALTER TABLE `play_single_artiste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_single_genre_musicaux`
--
ALTER TABLE `play_single_genre_musicaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_top_single`
--
ALTER TABLE `play_top_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_top_video`
--
ALTER TABLE `play_top_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `play_video`
--
ALTER TABLE `play_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `home_commentaire_actualite`
--
ALTER TABLE `home_commentaire_actualite`
  ADD CONSTRAINT `FK_394D92DA2843073` FOREIGN KEY (`actualite_id`) REFERENCES `news_actualite` (`id`),
  ADD CONSTRAINT `FK_394D92DFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_commentaire_genre_musicaux`
--
ALTER TABLE `home_commentaire_genre_musicaux`
  ADD CONSTRAINT `FK_F3FC0DFFD21AAC2D` FOREIGN KEY (`genre_musicaux_id`) REFERENCES `play_genre_musicaux` (`id`),
  ADD CONSTRAINT `FK_F3FC0DFFFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_commentaire_single`
--
ALTER TABLE `home_commentaire_single`
  ADD CONSTRAINT `FK_80E664C9E7C1D92B` FOREIGN KEY (`single_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_80E664C9FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_commentaire_video`
--
ALTER TABLE `home_commentaire_video`
  ADD CONSTRAINT `FK_E2214B4829C1004E` FOREIGN KEY (`video_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_E2214B48FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_favorie_album`
--
ALTER TABLE `home_favorie_album`
  ADD CONSTRAINT `FK_F34C74431137ABCF` FOREIGN KEY (`album_id`) REFERENCES `play_album` (`id`),
  ADD CONSTRAINT `FK_F34C7443FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_favorie_artiste`
--
ALTER TABLE `home_favorie_artiste`
  ADD CONSTRAINT `FK_616506E121D25844` FOREIGN KEY (`artiste_id`) REFERENCES `play_artiste` (`id`),
  ADD CONSTRAINT `FK_616506E1FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_favorie_genre_musicaux`
--
ALTER TABLE `home_favorie_genre_musicaux`
  ADD CONSTRAINT `FK_54C1BA41D21AAC2D` FOREIGN KEY (`genre_musicaux_id`) REFERENCES `play_genre_musicaux` (`id`),
  ADD CONSTRAINT `FK_54C1BA41FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_favorie_single`
--
ALTER TABLE `home_favorie_single`
  ADD CONSTRAINT `FK_CA6DF303E7C1D92B` FOREIGN KEY (`single_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_CA6DF303FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_favorie_video`
--
ALTER TABLE `home_favorie_video`
  ADD CONSTRAINT `FK_B613C02C29C1004E` FOREIGN KEY (`video_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_B613C02CFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_actualite`
--
ALTER TABLE `home_jaime_actualite`
  ADD CONSTRAINT `FK_90FE3BCBA2843073` FOREIGN KEY (`actualite_id`) REFERENCES `news_actualite` (`id`),
  ADD CONSTRAINT `FK_90FE3BCBFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_album`
--
ALTER TABLE `home_jaime_album`
  ADD CONSTRAINT `FK_3AD7EFF81137ABCF` FOREIGN KEY (`album_id`) REFERENCES `play_album` (`id`),
  ADD CONSTRAINT `FK_3AD7EFF8FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_artiste`
--
ALTER TABLE `home_jaime_artiste`
  ADD CONSTRAINT `FK_189F2C521D25844` FOREIGN KEY (`artiste_id`) REFERENCES `play_artiste` (`id`),
  ADD CONSTRAINT `FK_189F2C5FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_genre_musicaux`
--
ALTER TABLE `home_jaime_genre_musicaux`
  ADD CONSTRAINT `FK_3732211CD21AAC2D` FOREIGN KEY (`genre_musicaux_id`) REFERENCES `play_genre_musicaux` (`id`),
  ADD CONSTRAINT `FK_3732211CFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_pas_actualite`
--
ALTER TABLE `home_jaime_pas_actualite`
  ADD CONSTRAINT `FK_87608FB1A2843073` FOREIGN KEY (`actualite_id`) REFERENCES `news_actualite` (`id`),
  ADD CONSTRAINT `FK_87608FB1FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_pas_album`
--
ALTER TABLE `home_jaime_pas_album`
  ADD CONSTRAINT `FK_116524C81137ABCF` FOREIGN KEY (`album_id`) REFERENCES `play_album` (`id`),
  ADD CONSTRAINT `FK_116524C8FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_pas_artiste`
--
ALTER TABLE `home_jaime_pas_artiste`
  ADD CONSTRAINT `FK_D279F4BC21D25844` FOREIGN KEY (`artiste_id`) REFERENCES `play_artiste` (`id`),
  ADD CONSTRAINT `FK_D279F4BCFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_pas_genre_musicaux`
--
ALTER TABLE `home_jaime_pas_genre_musicaux`
  ADD CONSTRAINT `FK_E44E11D3D21AAC2D` FOREIGN KEY (`genre_musicaux_id`) REFERENCES `play_genre_musicaux` (`id`),
  ADD CONSTRAINT `FK_E44E11D3FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_pas_single`
--
ALTER TABLE `home_jaime_pas_single`
  ADD CONSTRAINT `FK_B0E580FBE7C1D92B` FOREIGN KEY (`single_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_B0E580FBFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_pas_video`
--
ALTER TABLE `home_jaime_pas_video`
  ADD CONSTRAINT `FK_543A90A729C1004E` FOREIGN KEY (`video_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_543A90A7FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_single`
--
ALTER TABLE `home_jaime_single`
  ADD CONSTRAINT `FK_9617029CE7C1D92B` FOREIGN KEY (`single_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_9617029CFB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_jaime_video`
--
ALTER TABLE `home_jaime_video`
  ADD CONSTRAINT `FK_7F885B9729C1004E` FOREIGN KEY (`video_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_7F885B97FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_replay_single`
--
ALTER TABLE `home_replay_single`
  ADD CONSTRAINT `FK_1B94E275E7C1D92B` FOREIGN KEY (`single_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_1B94E275FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `home_replay_video`
--
ALTER TABLE `home_replay_video`
  ADD CONSTRAINT `FK_F465478829C1004E` FOREIGN KEY (`video_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_F4654788FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `home_utilisateur` (`id`);

--
-- Contraintes pour la table `news_actualite`
--
ALTER TABLE `news_actualite`
  ADD CONSTRAINT `FK_48B7DE564C4E591D` FOREIGN KEY (`new_categorie_actualite_id`) REFERENCES `news_categorie_actualite` (`id`),
  ADD CONSTRAINT `FK_48B7DE56D0D2F576` FOREIGN KEY (`play_single_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_48B7DE56E2D6ADA5` FOREIGN KEY (`play_video_id`) REFERENCES `play_video` (`id`);

--
-- Contraintes pour la table `news_tag_actualite`
--
ALTER TABLE `news_tag_actualite`
  ADD CONSTRAINT `FK_1046D7F6367BA065` FOREIGN KEY (`news_tag_id`) REFERENCES `news_tag` (`id`),
  ADD CONSTRAINT `FK_1046D7F644CE59C4` FOREIGN KEY (`news_actualite_id`) REFERENCES `news_actualite` (`id`);

--
-- Contraintes pour la table `play_artiste`
--
ALTER TABLE `play_artiste`
  ADD CONSTRAINT `FK_EA1952FF13C4891C` FOREIGN KEY (`home_pays_id`) REFERENCES `home_pays` (`id`);

--
-- Contraintes pour la table `play_single_artiste`
--
ALTER TABLE `play_single_artiste`
  ADD CONSTRAINT `FK_821D8EED343F6621` FOREIGN KEY (`play_artiste_id`) REFERENCES `play_artiste` (`id`),
  ADD CONSTRAINT `FK_821D8EEDD0D2F576` FOREIGN KEY (`play_single_id`) REFERENCES `play_single` (`id`);

--
-- Contraintes pour la table `play_single_genre_musicaux`
--
ALTER TABLE `play_single_genre_musicaux`
  ADD CONSTRAINT `FK_6CEA035882670D71` FOREIGN KEY (`play_genre_musicaux_id`) REFERENCES `play_genre_musicaux` (`id`),
  ADD CONSTRAINT `FK_6CEA0358D0D2F576` FOREIGN KEY (`play_single_id`) REFERENCES `play_single` (`id`);

--
-- Contraintes pour la table `play_top_single`
--
ALTER TABLE `play_top_single`
  ADD CONSTRAINT `FK_88B7B3F04DE9E538` FOREIGN KEY (`single4_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F05F5C4AD6` FOREIGN KEY (`single7_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F060BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `home_utilisateur` (`id`),
  ADD CONSTRAINT `FK_88B7B3F06882BAE4` FOREIGN KEY (`single2_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F073F5A80` FOREIGN KEY (`single8_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F07A37150A` FOREIGN KEY (`single1_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F08E7F94FD` FOREIGN KEY (`single10_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F0BF833DE5` FOREIGN KEY (`single9_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F0D03EDD81` FOREIGN KEY (`single3_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F0E7E02DB3` FOREIGN KEY (`single6_id`) REFERENCES `play_single` (`id`),
  ADD CONSTRAINT `FK_88B7B3F0F555825D` FOREIGN KEY (`single5_id`) REFERENCES `play_single` (`id`);

--
-- Contraintes pour la table `play_top_video`
--
ALTER TABLE `play_top_video`
  ADD CONSTRAINT `FK_DECB5F053A29CF8E` FOREIGN KEY (`video8_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F0547218004` FOREIGN KEY (`video1_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F0555942FEA` FOREIGN KEY (`video2_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F0560BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `home_utilisateur` (`id`),
  ADD CONSTRAINT `FK_DECB5F05624ADFD8` FOREIGN KEY (`video7_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F0569FAAF6F` FOREIGN KEY (`video10_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F0570FF7036` FOREIGN KEY (`video4_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F058295A8EB` FOREIGN KEY (`video9_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F05C8431753` FOREIGN KEY (`video5_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F05DAF6B8BD` FOREIGN KEY (`video6_id`) REFERENCES `play_video` (`id`),
  ADD CONSTRAINT `FK_DECB5F05ED28488F` FOREIGN KEY (`video3_id`) REFERENCES `play_video` (`id`);

--
-- Contraintes pour la table `play_video`
--
ALTER TABLE `play_video`
  ADD CONSTRAINT `FK_8D183D90343F6621` FOREIGN KEY (`play_artiste_id`) REFERENCES `play_artiste` (`id`),
  ADD CONSTRAINT `FK_8D183D90D0D2F576` FOREIGN KEY (`play_single_id`) REFERENCES `play_single` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
