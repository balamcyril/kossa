<?php

namespace App\Controller;

use App\Entity\HomeCommentaireGenreMusicaux;
use App\Form\HomeCommentaireGenreMusicauxType;
use App\Repository\HomeCommentaireGenreMusicauxRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/commentaire/genre/musicaux")
 */
class HomeCommentaireGenreMusicauxController extends AbstractController
{
    /**
     * @Route("/", name="home_commentaire_genre_musicaux_index", methods={"GET"})
     */
    public function index(HomeCommentaireGenreMusicauxRepository $homeCommentaireGenreMusicauxRepository): Response
    {
        return $this->render('home_commentaire_genre_musicaux/index.html.twig', [
            'home_commentaire_genre_musicauxes' => $homeCommentaireGenreMusicauxRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_commentaire_genre_musicaux_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeCommentaireGenreMusicaux = new HomeCommentaireGenreMusicaux();
        $form = $this->createForm(HomeCommentaireGenreMusicauxType::class, $homeCommentaireGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeCommentaireGenreMusicaux);
            $entityManager->flush();

            return $this->redirectToRoute('home_commentaire_genre_musicaux_index');
        }

        return $this->render('home_commentaire_genre_musicaux/new.html.twig', [
            'home_commentaire_genre_musicaux' => $homeCommentaireGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_genre_musicaux_show", methods={"GET"})
     */
    public function show(HomeCommentaireGenreMusicaux $homeCommentaireGenreMusicaux): Response
    {
        return $this->render('home_commentaire_genre_musicaux/show.html.twig', [
            'home_commentaire_genre_musicaux' => $homeCommentaireGenreMusicaux,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_commentaire_genre_musicaux_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeCommentaireGenreMusicaux $homeCommentaireGenreMusicaux): Response
    {
        $form = $this->createForm(HomeCommentaireGenreMusicauxType::class, $homeCommentaireGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_commentaire_genre_musicaux_index');
        }

        return $this->render('home_commentaire_genre_musicaux/edit.html.twig', [
            'home_commentaire_genre_musicaux' => $homeCommentaireGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_genre_musicaux_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeCommentaireGenreMusicaux $homeCommentaireGenreMusicaux): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeCommentaireGenreMusicaux->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeCommentaireGenreMusicaux);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_commentaire_genre_musicaux_index');
    }
}
