<?php

namespace App\Controller;

use App\Entity\HomeFavorieSingle;
use App\Form\HomeFavorieSingleType;
use App\Repository\HomeFavorieSingleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/favorie/single")
 */
class HomeFavorieSingleController extends AbstractController
{
    /**
     * @Route("/", name="home_favorie_single_index", methods={"GET"})
     */
    public function index(HomeFavorieSingleRepository $homeFavorieSingleRepository): Response
    {
        return $this->render('home_favorie_single/index.html.twig', [
            'home_favorie_singles' => $homeFavorieSingleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_favorie_single_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeFavorieSingle = new HomeFavorieSingle();
        $form = $this->createForm(HomeFavorieSingleType::class, $homeFavorieSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeFavorieSingle);
            $entityManager->flush();

            return $this->redirectToRoute('home_favorie_single_index');
        }

        return $this->render('home_favorie_single/new.html.twig', [
            'home_favorie_single' => $homeFavorieSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_single_show", methods={"GET"})
     */
    public function show(HomeFavorieSingle $homeFavorieSingle): Response
    {
        return $this->render('home_favorie_single/show.html.twig', [
            'home_favorie_single' => $homeFavorieSingle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_favorie_single_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeFavorieSingle $homeFavorieSingle): Response
    {
        $form = $this->createForm(HomeFavorieSingleType::class, $homeFavorieSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_favorie_single_index');
        }

        return $this->render('home_favorie_single/edit.html.twig', [
            'home_favorie_single' => $homeFavorieSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_single_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeFavorieSingle $homeFavorieSingle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeFavorieSingle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeFavorieSingle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_favorie_single_index');
    }
}
