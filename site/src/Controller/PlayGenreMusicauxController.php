<?php

namespace App\Controller;

use App\Entity\PlayGenreMusicaux;
use App\Form\PlayGenreMusicauxType;
use App\Repository\PlayGenreMusicauxRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/play/genre/musicaux")
 */
class PlayGenreMusicauxController extends AbstractController
{
    /**
     * @Route("/", name="play_genre_musicaux_index", methods={"GET"})
     */
    public function index(PlayGenreMusicauxRepository $playGenreMusicauxRepository): Response
    {
        return $this->render('play_genre_musicaux/index.html.twig', [
            'play_genre_musicauxes' => $playGenreMusicauxRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="play_genre_musicaux_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playGenreMusicaux = new PlayGenreMusicaux();
        $form = $this->createForm(PlayGenreMusicauxType::class, $playGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playGenreMusicaux);
            $entityManager->flush();

            return $this->redirectToRoute('play_genre_musicaux_index');
        }

        return $this->render('play_genre_musicaux/new.html.twig', [
            'play_genre_musicaux' => $playGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_genre_musicaux_show", methods={"GET"})
     */
    public function show(PlayGenreMusicaux $playGenreMusicaux): Response
    {
        return $this->render('play_genre_musicaux/show.html.twig', [
            'play_genre_musicaux' => $playGenreMusicaux,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_genre_musicaux_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlayGenreMusicaux $playGenreMusicaux): Response
    {
        $playGenreMusicaux2=$playGenreMusicaux;
        $form = $this->createForm(PlayGenreMusicauxType::class, $playGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($playGenreMusicaux->getPhoto()==null)
            {
                $playGenreMusicaux->setPhoto($playGenreMusicaux2->getPhoto());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_genre_musicaux_index');
        }

        return $this->render('play_genre_musicaux/edit.html.twig', [
            'play_genre_musicaux' => $playGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/Photo", name="genre_musicaux_Photo", methods={"GET","POST"})
     */
    public function photo(Request $request,  PlayGenreMusicaux $playGenreMusicaux)
    {
        
           $playGenreMusicaux->setPhoto(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_genre_musicaux_edit', array('id' => $playGenreMusicaux->getId()));
    }

    /**
     * @Route("/delete/{id}", name="play_genre_musicaux_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, PlayGenreMusicaux $playGenreMusicaux): Response
    {
         $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playGenreMusicaux);
            $entityManager->flush();
        
        return $this->redirectToRoute('play_genre_musicaux_index');
    }
}
