<?php

namespace App\Controller;

use App\Entity\HomeJaimeSingle;
use App\Form\HomeJaimeSingleType;
use App\Repository\HomeJaimeSingleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/single")
 */
class HomeJaimeSingleController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_single_index", methods={"GET"})
     */
    public function index(HomeJaimeSingleRepository $homeJaimeSingleRepository): Response
    {
        return $this->render('home_jaime_single/index.html.twig', [
            'home_jaime_singles' => $homeJaimeSingleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_single_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimeSingle = new HomeJaimeSingle();
        $form = $this->createForm(HomeJaimeSingleType::class, $homeJaimeSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimeSingle);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_single_index');
        }

        return $this->render('home_jaime_single/new.html.twig', [
            'home_jaime_single' => $homeJaimeSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_single_show", methods={"GET"})
     */
    public function show(HomeJaimeSingle $homeJaimeSingle): Response
    {
        return $this->render('home_jaime_single/show.html.twig', [
            'home_jaime_single' => $homeJaimeSingle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_single_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimeSingle $homeJaimeSingle): Response
    {
        $form = $this->createForm(HomeJaimeSingleType::class, $homeJaimeSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_single_index');
        }

        return $this->render('home_jaime_single/edit.html.twig', [
            'home_jaime_single' => $homeJaimeSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_single_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimeSingle $homeJaimeSingle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimeSingle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimeSingle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_single_index');
    }
}
