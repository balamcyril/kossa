<?php

namespace App\Controller;

use App\Entity\PlayTopVideo;
use App\Form\PlayTopVideoType;
use App\Repository\PlayTopVideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/play/top/video")
 */
class PlayTopVideoController extends AbstractController
{
    /**
     * @Route("/", name="play_top_video_index", methods={"GET"})
     */
    public function index(PlayTopVideoRepository $playTopVideoRepository): Response
    {
        return $this->render('play_top_video/index.html.twig', [
            'play_top_videos' => $playTopVideoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="play_top_video_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playTopVideo = new PlayTopVideo();
        $form = $this->createForm(PlayTopVideoType::class, $playTopVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playTopVideo);
            $entityManager->flush();

            return $this->redirectToRoute('play_top_video_index');
        }

        return $this->render('play_top_video/new.html.twig', [
            'play_top_video' => $playTopVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_top_video_show", methods={"GET"})
     */
    public function show(PlayTopVideo $playTopVideo): Response
    {
        return $this->render('play_top_video/show.html.twig', [
            'play_top_video' => $playTopVideo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_top_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlayTopVideo $playTopVideo): Response
    {
        $playTopVideo2 = $playTopVideo;
        $form = $this->createForm(PlayTopVideoType::class, $playTopVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($playTopVideo->getPhoto()==null)
            {
                $playTopVideo->setPhoto($playTopVideo2->getPhoto());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_top_video_index');
        }

        return $this->render('play_top_video/edit.html.twig', [
            'play_top_video' => $playTopVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/Photo", name="play_top_video_Photo", methods={"GET","POST"})
     */
    public function photo(Request $request,  PlayTopVideo $playTopVideo)
    {
        
           $playTopVideo->setPhoto(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_top_video_edit', array('id' => $playTopVideo->getId()));
    }

    /**
     * @Route("/delete/{id}", name="play_top_video_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PlayTopVideo $playTopVideo): Response
    {
         $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playTopVideo);
            $entityManager->flush();
        

        return $this->redirectToRoute('play_top_video_index');
    }
}
