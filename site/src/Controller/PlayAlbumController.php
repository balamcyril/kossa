<?php

namespace App\Controller;

use App\Entity\PlayAlbum;
use App\Form\PlayAlbumType;
use App\Repository\PlayAlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/play/album")
 */
class PlayAlbumController extends AbstractController
{
    /**
     * @Route("/", name="play_album_index", methods={"GET"})
     */
    public function index(PlayAlbumRepository $playAlbumRepository): Response
    {
        return $this->render('play_album/index.html.twig', [
            'play_albums' => $playAlbumRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="play_album_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playAlbum = new PlayAlbum();
        $form = $this->createForm(PlayAlbumType::class, $playAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playAlbum);
            $entityManager->flush();

            return $this->redirectToRoute('play_album_index');
        }

        return $this->render('play_album/new.html.twig', [
            'play_album' => $playAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_album_show", methods={"GET"})
     */
    public function show(PlayAlbum $playAlbum): Response
    {
        return $this->render('play_album/show.html.twig', [
            'play_album' => $playAlbum,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_album_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlayAlbum $playAlbum): Response
    {
        $playAlbum2=$playAlbum;
        $form = $this->createForm(PlayAlbumType::class, $playAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($playAlbum->getCover1()==null)
            {
                $playAlbum->setCover1($playAlbum2->getCover1());
            }

            if($playAlbum->getCover2()==null)
            {
                $playAlbum->setCover2($playAlbum2->getCover2());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_album_index');
        }

        return $this->render('play_album/edit.html.twig', [
            'play_album' => $playAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/cover1", name="play_album_cover1", methods={"GET","POST"})
     */
    public function cover1(Request $request,  PlayAlbum $playAlbum )
    {
        
           $playAlbum->setCover1(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_album_edit', array('id' => $playAlbum->getId()));
    }

    /**
     * @Route("/{id}/cover2", name="play_album_cover2", methods={"GET","POST"})
     */
    public function cover2(Request $request,  PlayAlbum $playAlbum)
    {
        
           $playAlbum->setCover2(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_album_edit', array('id' => $playAlbum->getId()));
    }

    /**
     * @Route("/delete/{id}", name="play_album_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, PlayAlbum $playAlbum): Response
    {
         $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playAlbum);
            $entityManager->flush();
       

        return $this->redirectToRoute('play_album_index');
    }
}
