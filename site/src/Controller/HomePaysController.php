<?php

namespace App\Controller;

use App\Entity\HomePays;
use App\Form\HomePaysType;
use App\Repository\HomePaysRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/pays")
 */
class HomePaysController extends AbstractController
{
    /**
     * @Route("/", name="home_pays_index", methods={"GET"})
     */
    public function index(HomePaysRepository $homePaysRepository): Response
    {
        return $this->render('home_pays/index.html.twig', [
            'home_pays' => $homePaysRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_pays_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homePay = new HomePays();
        $form = $this->createForm(HomePaysType::class, $homePay);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homePay);
            $entityManager->flush();

            return $this->redirectToRoute('home_pays_index');
        }

        return $this->render('home_pays/new.html.twig', [
            'home_pay' => $homePay,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_pays_show", methods={"GET"})
     */
    public function show(HomePays $homePay): Response
    {
        return $this->render('home_pays/show.html.twig', [
            'home_pay' => $homePay,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_pays_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomePays $homePay): Response
    {
        $form = $this->createForm(HomePaysType::class, $homePay);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_pays_index');
        }

        return $this->render('home_pays/edit.html.twig', [
            'home_pay' => $homePay,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="home_pays_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, HomePays $homePay): Response
    {
         $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homePay);
            $entityManager->flush();
        
        return $this->redirectToRoute('home_pays_index');
    }
}
