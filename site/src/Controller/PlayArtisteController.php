<?php

namespace App\Controller;

use App\Entity\PlayArtiste;
use App\Form\PlayArtisteType;
use App\Repository\PlayArtisteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/play/artiste")
 */
class PlayArtisteController extends AbstractController
{
    /**
     * @Route("/", name="play_artiste_index", methods={"GET"})
     */
    public function index(PlayArtisteRepository $playArtisteRepository): Response
    {
        return $this->render('play_artiste/index.html.twig', [
            'play_artistes' => $playArtisteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="play_artiste_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playArtiste = new PlayArtiste();
        $form = $this->createForm(PlayArtisteType::class, $playArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playArtiste);
            $entityManager->flush();

            return $this->redirectToRoute('play_artiste_index');
        }

        return $this->render('play_artiste/new.html.twig', [
            'play_artiste' => $playArtiste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_artiste_show", methods={"GET"})
     */
    public function show(PlayArtiste $playArtiste): Response
    {
        return $this->render('play_artiste/show.html.twig', [
            'play_artiste' => $playArtiste,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_artiste_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlayArtiste $playArtiste): Response
    {
        $playArtiste2=$playArtiste;
        $form = $this->createForm(PlayArtisteType::class, $playArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($playArtiste->getPhoto1()==null)
            {
                $playArtiste->setPhoto1($playArtiste2->getPhoto1());
            }

            if($playArtiste->getPhoto2()==null)
            {
                $playArtiste->setPhoto2($playArtiste2->getPhoto2());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_artiste_index');
        }

        return $this->render('play_artiste/edit.html.twig', [
            'play_artiste' => $playArtiste,
            'form' => $form->createView(),
        ]);
    }

     /**
     * @Route("/{id}/Photo1", name="play_artiste_Photo1", methods={"GET","POST"})
     */
    public function photo1(Request $request,  PlayArtiste $playArtiste)
    {
        
           $playArtiste->setPhoto1(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_artiste_edit', array('id' => $playArtiste->getId()));
    }

    /**
     * @Route("/{id}/Photo2", name="play_artiste_Photo2", methods={"GET","POST"})
     */
    public function photo2(Request $request,  PlayArtiste $playArtiste)
    {
        
           $playArtiste->setPhoto2(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_artiste_edit', array('id' => $playArtiste->getId()));
    }

    /**
     * @Route("/delete/{id}", name="play_artiste_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, PlayArtiste $playArtiste): Response
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playArtiste);
            $entityManager->flush();
        

        return $this->redirectToRoute('play_artiste_index');
    }
}
