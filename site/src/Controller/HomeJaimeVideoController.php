<?php

namespace App\Controller;

use App\Entity\HomeJaimeVideo;
use App\Form\HomeJaimeVideoType;
use App\Repository\HomeJaimeVideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/video")
 */
class HomeJaimeVideoController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_video_index", methods={"GET"})
     */
    public function index(HomeJaimeVideoRepository $homeJaimeVideoRepository): Response
    {
        return $this->render('home_jaime_video/index.html.twig', [
            'home_jaime_videos' => $homeJaimeVideoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_video_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimeVideo = new HomeJaimeVideo();
        $form = $this->createForm(HomeJaimeVideoType::class, $homeJaimeVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimeVideo);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_video_index');
        }

        return $this->render('home_jaime_video/new.html.twig', [
            'home_jaime_video' => $homeJaimeVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_video_show", methods={"GET"})
     */
    public function show(HomeJaimeVideo $homeJaimeVideo): Response
    {
        return $this->render('home_jaime_video/show.html.twig', [
            'home_jaime_video' => $homeJaimeVideo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimeVideo $homeJaimeVideo): Response
    {
        $form = $this->createForm(HomeJaimeVideoType::class, $homeJaimeVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_video_index');
        }

        return $this->render('home_jaime_video/edit.html.twig', [
            'home_jaime_video' => $homeJaimeVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_video_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimeVideo $homeJaimeVideo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimeVideo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimeVideo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_video_index');
    }
}
