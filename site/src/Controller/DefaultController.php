<?php

namespace App\Controller;

/*use App\Entity\Eleve;
use App\Entity\Tranche;
use App\Entity\Inscription;
use App\Entity\Pansion;
use App\Repository\AcheterRepository;
use App\Repository\AnneeRepository;
use App\Repository\EleveRepository;
use App\Repository\DepenseRepository;
use App\Repository\TrancheRepository;*/
use App\Repository\NewsActualiteRepository;
use App\Repository\PlayArtisteRepository;
use App\Repository\PlayTopSingleRepository;
use App\Repository\PlayTopVideoRepository;
use App\Repository\PlaySingleRepository;
use App\Repository\PlayVideoRepository;
use App\Repository\PlayAlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    // page d'accueil
    /**
     * @Route("/admin", name="admin", methods={"GET"})
     */
    public function index(PlayVideoRepository $playVideoRepository,
                          PlaySingleRepository $playSingleRepository,
                          PlayTopVideoRepository $playTopVideoRepository,
                          PlayTopSingleRepository $playTopSingleRepository,
                          PlayArtisteRepository $playArtisteRepository,
                          NewsActualiteRepository $newsActualiteRepository,
                          PlayAlbumRepository $PlayAlbumRepository): Response
        
                           

    {
        
        return $this->render('accueil.html.twig', [
            'news_actualite' => $newsActualiteRepository->findAll(),
            'play_artiste'   => $playArtisteRepository->findAll(),
            'play_single'  => $playSingleRepository->findAll(),
            'play_video'    =>  $playVideoRepository->findAll(),
            'play_top_single' => $playTopSingleRepository->findAll(),
            'play_top_video'  => $playTopVideoRepository->findAll(),
            'play_album'  => $PlayAlbumRepository->findAll(),
        ]);  
        
      
    }
    
      
    // page d'accueil
    /**
     * @Route("/", name="accueil", methods={"GET","POST"})
     */
    public function accueil()
    {      
        $em = $this->getDoctrine()->getManager();
            
        return $this->render('front-end/index.html.twig');                    
        
    }
    
    
        // coming_soon
    /**
     * @Route("/coming_soon/{page}", name="coming_soon", methods={"GET"})
     */
    public function coming_soon(Request $request, $page)
    {      
        $ancienUrl = $request->headers->get('referer');
        
        if ($page='play') {
            return $this->render('front-end/kossa-play/soon_play.html.twig', ['ancienUrl'=>$ancienUrl]);
        }
        elseif ($page='moovie') {
            return $this->render('front-end/kossa-moovie/soon_moovie.html.twig', ['ancienUrl'=>$ancienUrl]);
        }
        elseif ($page='event') {
            return $this->render('front-end/kossa-event/soon_event.html.twig', ['ancienUrl'=>$ancienUrl]);
        }
        else {
             return $this->redirectToRoute('accueil');
        }                    
        
    }
    
}
