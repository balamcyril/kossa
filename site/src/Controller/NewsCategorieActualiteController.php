<?php

namespace App\Controller;

use App\Entity\NewsCategorieActualite;
use App\Form\NewsCategorieActualiteType;
use App\Repository\NewsCategorieActualiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news/categorie/actualite")
 */
class NewsCategorieActualiteController extends AbstractController
{
    /**
     * @Route("/", name="news_categorie_actualite_index", methods={"GET"})
     */
    public function index(NewsCategorieActualiteRepository $newsCategorieActualiteRepository): Response
    {
        return $this->render('news_categorie_actualite/index.html.twig', [
            'news_categorie_actualites' => $newsCategorieActualiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="news_categorie_actualite_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $newsCategorieActualite = new NewsCategorieActualite();
        $form = $this->createForm(NewsCategorieActualiteType::class, $newsCategorieActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newsCategorieActualite);
            $entityManager->flush();

            return $this->redirectToRoute('news_categorie_actualite_index');
        }

        return $this->render('news_categorie_actualite/new.html.twig', [
            'news_categorie_actualite' => $newsCategorieActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="news_categorie_actualite_show", methods={"GET"})
     */
    public function show(NewsCategorieActualite $newsCategorieActualite): Response
    {
        return $this->render('news_categorie_actualite/show.html.twig', [
            'news_categorie_actualite' => $newsCategorieActualite,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="news_categorie_actualite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NewsCategorieActualite $newsCategorieActualite): Response
    {
        $form = $this->createForm(NewsCategorieActualiteType::class, $newsCategorieActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('news_categorie_actualite_index');
        }

        return $this->render('news_categorie_actualite/edit.html.twig', [
            'news_categorie_actualite' => $newsCategorieActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="news_categorie_actualite_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, NewsCategorieActualite $newsCategorieActualite): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($newsCategorieActualite);
            $entityManager->flush();
        

        return $this->redirectToRoute('news_categorie_actualite_index');
    }
}
