<?php

namespace App\Controller;

use App\Entity\HomeFavorieAlbum;
use App\Form\HomeFavorieAlbumType;
use App\Repository\HomeFavorieAlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/favorie/album")
 */
class HomeFavorieAlbumController extends AbstractController
{
    /**
     * @Route("/", name="home_favorie_album_index", methods={"GET"})
     */
    public function index(HomeFavorieAlbumRepository $homeFavorieAlbumRepository): Response
    {
        return $this->render('home_favorie_album/index.html.twig', [
            'home_favorie_albums' => $homeFavorieAlbumRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_favorie_album_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeFavorieAlbum = new HomeFavorieAlbum();
        $form = $this->createForm(HomeFavorieAlbumType::class, $homeFavorieAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeFavorieAlbum);
            $entityManager->flush();

            return $this->redirectToRoute('home_favorie_album_index');
        }

        return $this->render('home_favorie_album/new.html.twig', [
            'home_favorie_album' => $homeFavorieAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_album_show", methods={"GET"})
     */
    public function show(HomeFavorieAlbum $homeFavorieAlbum): Response
    {
        return $this->render('home_favorie_album/show.html.twig', [
            'home_favorie_album' => $homeFavorieAlbum,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_favorie_album_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeFavorieAlbum $homeFavorieAlbum): Response
    {
        $form = $this->createForm(HomeFavorieAlbumType::class, $homeFavorieAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_favorie_album_index');
        }

        return $this->render('home_favorie_album/edit.html.twig', [
            'home_favorie_album' => $homeFavorieAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_album_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeFavorieAlbum $homeFavorieAlbum): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeFavorieAlbum->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeFavorieAlbum);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_favorie_album_index');
    }
}
