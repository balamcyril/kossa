<?php

namespace App\Controller;

use App\Entity\HomeJaimePasSingle;
use App\Form\HomeJaimePasSingleType;
use App\Repository\HomeJaimePasSingleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/pas/single")
 */
class HomeJaimePasSingleController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_pas_single_index", methods={"GET"})
     */
    public function index(HomeJaimePasSingleRepository $homeJaimePasSingleRepository): Response
    {
        return $this->render('home_jaime_pas_single/index.html.twig', [
            'home_jaime_pas_singles' => $homeJaimePasSingleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_pas_single_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimePasSingle = new HomeJaimePasSingle();
        $form = $this->createForm(HomeJaimePasSingleType::class, $homeJaimePasSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimePasSingle);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_pas_single_index');
        }

        return $this->render('home_jaime_pas_single/new.html.twig', [
            'home_jaime_pas_single' => $homeJaimePasSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_single_show", methods={"GET"})
     */
    public function show(HomeJaimePasSingle $homeJaimePasSingle): Response
    {
        return $this->render('home_jaime_pas_single/show.html.twig', [
            'home_jaime_pas_single' => $homeJaimePasSingle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_pas_single_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimePasSingle $homeJaimePasSingle): Response
    {
        $form = $this->createForm(HomeJaimePasSingleType::class, $homeJaimePasSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_pas_single_index');
        }

        return $this->render('home_jaime_pas_single/edit.html.twig', [
            'home_jaime_pas_single' => $homeJaimePasSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_single_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimePasSingle $homeJaimePasSingle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimePasSingle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimePasSingle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_pas_single_index');
    }
}
