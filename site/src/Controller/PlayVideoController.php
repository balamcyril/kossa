<?php

namespace App\Controller;

use App\Entity\PlayVideo;
use App\Form\PlayVideoType;
use App\Repository\PlayVideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/play/video")
 */
class PlayVideoController extends AbstractController
{
    /**
     * @Route("/", name="play_video_index", methods={"GET"})
     */
    public function index(PlayVideoRepository $playVideoRepository): Response
    {
        return $this->render('play_video/index.html.twig', [
            'play_videos' => $playVideoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="play_video_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playVideo = new PlayVideo();
        $form = $this->createForm(PlayVideoType::class, $playVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playVideo);
            $entityManager->flush();

            return $this->redirectToRoute('play_video_index');
        }

        return $this->render('play_video/new.html.twig', [
            'play_video' => $playVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_video_show", methods={"GET"})
     */
    public function show(PlayVideo $playVideo): Response
    {
        return $this->render('play_video/show.html.twig', [
            'play_video' => $playVideo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlayVideo $playVideo): Response
    {
        $playVideo2 = $playVideo;
        $form = $this->createForm(PlayVideoType::class, $playVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($playVideo->getPhoto()==null)
            {
                $playVideo->setPhoto($playVideo2->getPhoto());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_video_index');
        }

        return $this->render('play_video/edit.html.twig', [
            'play_video' => $playVideo,
            'form' => $form->createView(),
        ]);
    }

     /**
     * @Route("/{id}/Photo", name="play_video_Photo", methods={"GET","POST"})
     */
    public function photo1(Request $request,  PlayVideo $playVideo)
    {
        
           $playVideo->setPhoto(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_video_edit', array('id' => $playVideo->getId()));
    }

    /**
     * @Route("/delete/{id}", name="play_video_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, PlayVideo $playVideo): Response
    {
       $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playVideo);
            $entityManager->flush();
        
        return $this->redirectToRoute('play_video_index');
    }
}
