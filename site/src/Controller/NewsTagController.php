<?php

namespace App\Controller;

use App\Entity\NewsTag;
use App\Form\NewsTagType;
use App\Repository\NewsTagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news/tag")
 */
class NewsTagController extends AbstractController
{
    /**
     * @Route("/", name="news_tag_index", methods={"GET"})
     */
    public function index(NewsTagRepository $newsTagRepository): Response
    {
        return $this->render('news_tag/index.html.twig', [
            'news_tags' => $newsTagRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="news_tag_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $newsTag = new NewsTag();
        $form = $this->createForm(NewsTagType::class, $newsTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newsTag);
            $entityManager->flush();

            return $this->redirectToRoute('news_tag_index');
        }

        return $this->render('news_tag/new.html.twig', [
            'news_tag' => $newsTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="news_tag_show", methods={"GET"})
     */
    public function show(NewsTag $newsTag): Response
    {
        return $this->render('news_tag/show.html.twig', [
            'news_tag' => $newsTag,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="news_tag_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NewsTag $newsTag): Response
    {
        $form = $this->createForm(NewsTagType::class, $newsTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('news_tag_index');
        }

        return $this->render('news_tag/edit.html.twig', [
            'news_tag' => $newsTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="news_tag_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, NewsTag $newsTag): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($newsTag);
            $entityManager->flush();
        

        return $this->redirectToRoute('news_tag_index');
    }
}
