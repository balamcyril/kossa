<?php

namespace App\Controller;

use App\Entity\HomeReplaySingle;
use App\Form\HomeReplaySingleType;
use App\Repository\HomeReplaySingleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/replay/single")
 */
class HomeReplaySingleController extends AbstractController
{
    /**
     * @Route("/", name="home_replay_single_index", methods={"GET"})
     */
    public function index(HomeReplaySingleRepository $homeReplaySingleRepository): Response
    {
        return $this->render('home_replay_single/index.html.twig', [
            'home_replay_singles' => $homeReplaySingleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_replay_single_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeReplaySingle = new HomeReplaySingle();
        $form = $this->createForm(HomeReplaySingleType::class, $homeReplaySingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeReplaySingle);
            $entityManager->flush();

            return $this->redirectToRoute('home_replay_single_index');
        }

        return $this->render('home_replay_single/new.html.twig', [
            'home_replay_single' => $homeReplaySingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_replay_single_show", methods={"GET"})
     */
    public function show(HomeReplaySingle $homeReplaySingle): Response
    {
        return $this->render('home_replay_single/show.html.twig', [
            'home_replay_single' => $homeReplaySingle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_replay_single_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeReplaySingle $homeReplaySingle): Response
    {
        $form = $this->createForm(HomeReplaySingleType::class, $homeReplaySingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_replay_single_index');
        }

        return $this->render('home_replay_single/edit.html.twig', [
            'home_replay_single' => $homeReplaySingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_replay_single_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeReplaySingle $homeReplaySingle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeReplaySingle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeReplaySingle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_replay_single_index');
    }
}
