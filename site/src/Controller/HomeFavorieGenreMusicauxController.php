<?php

namespace App\Controller;

use App\Entity\HomeFavorieGenreMusicaux;
use App\Form\HomeFavorieGenreMusicauxType;
use App\Repository\HomeFavorieGenreMusicauxRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/favorie/genre/musicaux")
 */
class HomeFavorieGenreMusicauxController extends AbstractController
{
    /**
     * @Route("/", name="home_favorie_genre_musicaux_index", methods={"GET"})
     */
    public function index(HomeFavorieGenreMusicauxRepository $homeFavorieGenreMusicauxRepository): Response
    {
        return $this->render('home_favorie_genre_musicaux/index.html.twig', [
            'home_favorie_genre_musicauxes' => $homeFavorieGenreMusicauxRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_favorie_genre_musicaux_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeFavorieGenreMusicaux = new HomeFavorieGenreMusicaux();
        $form = $this->createForm(HomeFavorieGenreMusicauxType::class, $homeFavorieGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeFavorieGenreMusicaux);
            $entityManager->flush();

            return $this->redirectToRoute('home_favorie_genre_musicaux_index');
        }

        return $this->render('home_favorie_genre_musicaux/new.html.twig', [
            'home_favorie_genre_musicaux' => $homeFavorieGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_genre_musicaux_show", methods={"GET"})
     */
    public function show(HomeFavorieGenreMusicaux $homeFavorieGenreMusicaux): Response
    {
        return $this->render('home_favorie_genre_musicaux/show.html.twig', [
            'home_favorie_genre_musicaux' => $homeFavorieGenreMusicaux,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_favorie_genre_musicaux_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeFavorieGenreMusicaux $homeFavorieGenreMusicaux): Response
    {
        $form = $this->createForm(HomeFavorieGenreMusicauxType::class, $homeFavorieGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_favorie_genre_musicaux_index');
        }

        return $this->render('home_favorie_genre_musicaux/edit.html.twig', [
            'home_favorie_genre_musicaux' => $homeFavorieGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_genre_musicaux_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeFavorieGenreMusicaux $homeFavorieGenreMusicaux): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeFavorieGenreMusicaux->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeFavorieGenreMusicaux);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_favorie_genre_musicaux_index');
    }
}
