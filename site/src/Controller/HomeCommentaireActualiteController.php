<?php

namespace App\Controller;

use App\Entity\HomeCommentaireActualite;
use App\Form\HomeCommentaireActualiteType;
use App\Repository\HomeCommentaireActualiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/commentaire/actualite")
 */
class HomeCommentaireActualiteController extends AbstractController
{
    /**
     * @Route("/", name="home_commentaire_actualite_index", methods={"GET"})
     */
    public function index(HomeCommentaireActualiteRepository $homeCommentaireActualiteRepository): Response
    {
        return $this->render('home_commentaire_actualite/index.html.twig', [
            'home_commentaire_actualites' => $homeCommentaireActualiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_commentaire_actualite_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeCommentaireActualite = new HomeCommentaireActualite();
        $form = $this->createForm(HomeCommentaireActualiteType::class, $homeCommentaireActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeCommentaireActualite);
            $entityManager->flush();

            return $this->redirectToRoute('home_commentaire_actualite_index');
        }

        return $this->render('home_commentaire_actualite/new.html.twig', [
            'home_commentaire_actualite' => $homeCommentaireActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_actualite_show", methods={"GET"})
     */
    public function show(HomeCommentaireActualite $homeCommentaireActualite): Response
    {
        return $this->render('home_commentaire_actualite/show.html.twig', [
            'home_commentaire_actualite' => $homeCommentaireActualite,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_commentaire_actualite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeCommentaireActualite $homeCommentaireActualite): Response
    {
        $form = $this->createForm(HomeCommentaireActualiteType::class, $homeCommentaireActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_commentaire_actualite_index');
        }

        return $this->render('home_commentaire_actualite/edit.html.twig', [
            'home_commentaire_actualite' => $homeCommentaireActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_actualite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeCommentaireActualite $homeCommentaireActualite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeCommentaireActualite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeCommentaireActualite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_commentaire_actualite_index');
    }
}
