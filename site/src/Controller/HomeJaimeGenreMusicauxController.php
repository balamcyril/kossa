<?php

namespace App\Controller;

use App\Entity\HomeJaimeGenreMusicaux;
use App\Form\HomeJaimeGenreMusicauxType;
use App\Repository\HomeJaimeGenreMusicauxRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/genre/musicaux")
 */
class HomeJaimeGenreMusicauxController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_genre_musicaux_index", methods={"GET"})
     */
    public function index(HomeJaimeGenreMusicauxRepository $homeJaimeGenreMusicauxRepository): Response
    {
        return $this->render('home_jaime_genre_musicaux/index.html.twig', [
            'home_jaime_genre_musicauxes' => $homeJaimeGenreMusicauxRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_genre_musicaux_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimeGenreMusicaux = new HomeJaimeGenreMusicaux();
        $form = $this->createForm(HomeJaimeGenreMusicauxType::class, $homeJaimeGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimeGenreMusicaux);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_genre_musicaux_index');
        }

        return $this->render('home_jaime_genre_musicaux/new.html.twig', [
            'home_jaime_genre_musicaux' => $homeJaimeGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_genre_musicaux_show", methods={"GET"})
     */
    public function show(HomeJaimeGenreMusicaux $homeJaimeGenreMusicaux): Response
    {
        return $this->render('home_jaime_genre_musicaux/show.html.twig', [
            'home_jaime_genre_musicaux' => $homeJaimeGenreMusicaux,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_genre_musicaux_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimeGenreMusicaux $homeJaimeGenreMusicaux): Response
    {
        $form = $this->createForm(HomeJaimeGenreMusicauxType::class, $homeJaimeGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_genre_musicaux_index');
        }

        return $this->render('home_jaime_genre_musicaux/edit.html.twig', [
            'home_jaime_genre_musicaux' => $homeJaimeGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_genre_musicaux_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimeGenreMusicaux $homeJaimeGenreMusicaux): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimeGenreMusicaux->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimeGenreMusicaux);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_genre_musicaux_index');
    }
}
