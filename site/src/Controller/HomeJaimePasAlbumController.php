<?php

namespace App\Controller;

use App\Entity\HomeJaimePasAlbum;
use App\Form\HomeJaimePasAlbumType;
use App\Repository\HomeJaimePasAlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/pas/album")
 */
class HomeJaimePasAlbumController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_pas_album_index", methods={"GET"})
     */
    public function index(HomeJaimePasAlbumRepository $homeJaimePasAlbumRepository): Response
    {
        return $this->render('home_jaime_pas_album/index.html.twig', [
            'home_jaime_pas_albums' => $homeJaimePasAlbumRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_pas_album_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimePasAlbum = new HomeJaimePasAlbum();
        $form = $this->createForm(HomeJaimePasAlbumType::class, $homeJaimePasAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimePasAlbum);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_pas_album_index');
        }

        return $this->render('home_jaime_pas_album/new.html.twig', [
            'home_jaime_pas_album' => $homeJaimePasAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_album_show", methods={"GET"})
     */
    public function show(HomeJaimePasAlbum $homeJaimePasAlbum): Response
    {
        return $this->render('home_jaime_pas_album/show.html.twig', [
            'home_jaime_pas_album' => $homeJaimePasAlbum,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_pas_album_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimePasAlbum $homeJaimePasAlbum): Response
    {
        $form = $this->createForm(HomeJaimePasAlbumType::class, $homeJaimePasAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_pas_album_index');
        }

        return $this->render('home_jaime_pas_album/edit.html.twig', [
            'home_jaime_pas_album' => $homeJaimePasAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_album_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimePasAlbum $homeJaimePasAlbum): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimePasAlbum->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimePasAlbum);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_pas_album_index');
    }
}
