<?php

namespace App\Controller;

use App\Entity\HomeJaimePasGenreMusicaux;
use App\Form\HomeJaimePasGenreMusicauxType;
use App\Repository\HomeJaimePasGenreMusicauxRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/pas/genre/musicaux")
 */
class HomeJaimePasGenreMusicauxController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_pas_genre_musicaux_index", methods={"GET"})
     */
    public function index(HomeJaimePasGenreMusicauxRepository $homeJaimePasGenreMusicauxRepository): Response
    {
        return $this->render('home_jaime_pas_genre_musicaux/index.html.twig', [
            'home_jaime_pas_genre_musicauxes' => $homeJaimePasGenreMusicauxRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_pas_genre_musicaux_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimePasGenreMusicaux = new HomeJaimePasGenreMusicaux();
        $form = $this->createForm(HomeJaimePasGenreMusicauxType::class, $homeJaimePasGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimePasGenreMusicaux);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_pas_genre_musicaux_index');
        }

        return $this->render('home_jaime_pas_genre_musicaux/new.html.twig', [
            'home_jaime_pas_genre_musicaux' => $homeJaimePasGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_genre_musicaux_show", methods={"GET"})
     */
    public function show(HomeJaimePasGenreMusicaux $homeJaimePasGenreMusicaux): Response
    {
        return $this->render('home_jaime_pas_genre_musicaux/show.html.twig', [
            'home_jaime_pas_genre_musicaux' => $homeJaimePasGenreMusicaux,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_pas_genre_musicaux_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimePasGenreMusicaux $homeJaimePasGenreMusicaux): Response
    {
        $form = $this->createForm(HomeJaimePasGenreMusicauxType::class, $homeJaimePasGenreMusicaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_pas_genre_musicaux_index');
        }

        return $this->render('home_jaime_pas_genre_musicaux/edit.html.twig', [
            'home_jaime_pas_genre_musicaux' => $homeJaimePasGenreMusicaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_genre_musicaux_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimePasGenreMusicaux $homeJaimePasGenreMusicaux): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimePasGenreMusicaux->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimePasGenreMusicaux);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_pas_genre_musicaux_index');
    }
}
