<?php

namespace App\Controller;

use App\Entity\HomeJaimeArtiste;
use App\Form\HomeJaimeArtisteType;
use App\Repository\HomeJaimeArtisteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/artiste")
 */
class HomeJaimeArtisteController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_artiste_index", methods={"GET"})
     */
    public function index(HomeJaimeArtisteRepository $homeJaimeArtisteRepository): Response
    {
        return $this->render('home_jaime_artiste/index.html.twig', [
            'home_jaime_artistes' => $homeJaimeArtisteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_artiste_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimeArtiste = new HomeJaimeArtiste();
        $form = $this->createForm(HomeJaimeArtisteType::class, $homeJaimeArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimeArtiste);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_artiste_index');
        }

        return $this->render('home_jaime_artiste/new.html.twig', [
            'home_jaime_artiste' => $homeJaimeArtiste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_artiste_show", methods={"GET"})
     */
    public function show(HomeJaimeArtiste $homeJaimeArtiste): Response
    {
        return $this->render('home_jaime_artiste/show.html.twig', [
            'home_jaime_artiste' => $homeJaimeArtiste,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_artiste_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimeArtiste $homeJaimeArtiste): Response
    {
        $form = $this->createForm(HomeJaimeArtisteType::class, $homeJaimeArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_artiste_index');
        }

        return $this->render('home_jaime_artiste/edit.html.twig', [
            'home_jaime_artiste' => $homeJaimeArtiste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_artiste_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimeArtiste $homeJaimeArtiste): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimeArtiste->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimeArtiste);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_artiste_index');
    }
}
