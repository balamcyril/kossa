<?php

namespace App\Controller;

use App\Entity\NewsActualite;
use App\Form\NewsActualiteType;
use App\Repository\NewsActualiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/news/actualite")
 */
class NewsActualiteController extends AbstractController
{
    /**
     * @Route("/", name="news_actualite_index", methods={"GET"})
     */
    public function index(NewsActualiteRepository $newsActualiteRepository): Response
    {
        return $this->render('news_actualite/index.html.twig', [
            'news_actualites' => $newsActualiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="news_actualite_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $newsActualite = new NewsActualite();
        $form = $this->createForm(NewsActualiteType::class, $newsActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newsActualite);
            $entityManager->flush();

            return $this->redirectToRoute('news_actualite_index');
        }

        return $this->render('news_actualite/new.html.twig', [
            'news_actualite' => $newsActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="news_actualite_show", methods={"GET"})
     */
    public function show(NewsActualite $newsActualite): Response
    {
        return $this->render('news_actualite/show.html.twig', [
            'news_actualite' => $newsActualite,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="news_actualite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, NewsActualite $newsActualite): Response
    {
        $newsActualite2 = $newsActualite;
        $form = $this->createForm(NewsActualiteType::class, $newsActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($newsActualite->getPhoto1()==null)
            {
                $newsActualite->setPhoto1($newsActualite2->getPhoto1());
            }
            if($newsActualite->getPhoto2()==null)
            {
                $newsActualite->setPhoto2($newsActualite2->getPhoto2());
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('news_actualite_index');
        }

        return $this->render('news_actualite/edit.html.twig', [
            'news_actualite' => $newsActualite,
            'form' => $form->createView(),
        ]);
    }

     /**
     * @Route("/{id}/Photo1", name="actualite_Photo1", methods={"GET","POST"})
     */
    public function photo1(Request $request, NewsActualite $newsActualite)
    {
        
           $NewsActualite->setPhoto1(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('news_actualite_edit', array('id' => $NewsActualite->getId()));
    }

     /**
     * @Route("/{id}/Photo2", name="actualite_Photo2", methods={"GET","POST"})
     */
    public function photo2(Request $request, NewsActualite $newsActualite)
    {
        
           $NewsActualite->setPhoto2(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('news_actualite_edit', array('id' => $NewsActualite->getId()));
    }

    /**
     * @Route("/delete/{id}", name="news_actualite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, NewsActualite $newsActualite): Response
    {
       $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($newsActualite);
            $entityManager->flush();

        return $this->redirectToRoute('news_actualite_index');
    }
}
