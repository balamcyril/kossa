<?php

namespace App\Controller;

use App\Entity\HomeFavorieVideo;
use App\Form\HomeFavorieVideoType;
use App\Repository\HomeFavorieVideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/favorie/video")
 */
class HomeFavorieVideoController extends AbstractController
{
    /**
     * @Route("/", name="home_favorie_video_index", methods={"GET"})
     */
    public function index(HomeFavorieVideoRepository $homeFavorieVideoRepository): Response
    {
        return $this->render('home_favorie_video/index.html.twig', [
            'home_favorie_videos' => $homeFavorieVideoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_favorie_video_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeFavorieVideo = new HomeFavorieVideo();
        $form = $this->createForm(HomeFavorieVideoType::class, $homeFavorieVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeFavorieVideo);
            $entityManager->flush();

            return $this->redirectToRoute('home_favorie_video_index');
        }

        return $this->render('home_favorie_video/new.html.twig', [
            'home_favorie_video' => $homeFavorieVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_video_show", methods={"GET"})
     */
    public function show(HomeFavorieVideo $homeFavorieVideo): Response
    {
        return $this->render('home_favorie_video/show.html.twig', [
            'home_favorie_video' => $homeFavorieVideo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_favorie_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeFavorieVideo $homeFavorieVideo): Response
    {
        $form = $this->createForm(HomeFavorieVideoType::class, $homeFavorieVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_favorie_video_index');
        }

        return $this->render('home_favorie_video/edit.html.twig', [
            'home_favorie_video' => $homeFavorieVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_video_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeFavorieVideo $homeFavorieVideo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeFavorieVideo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeFavorieVideo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_favorie_video_index');
    }
}
