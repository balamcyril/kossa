<?php

namespace App\Controller;

use App\Entity\HomeReplayVideo;
use App\Form\HomeReplayVideoType;
use App\Repository\HomeReplayVideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/replay/video")
 */
class HomeReplayVideoController extends AbstractController
{
    /**
     * @Route("/", name="home_replay_video_index", methods={"GET"})
     */
    public function index(HomeReplayVideoRepository $homeReplayVideoRepository): Response
    {
        return $this->render('home_replay_video/index.html.twig', [
            'home_replay_videos' => $homeReplayVideoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_replay_video_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeReplayVideo = new HomeReplayVideo();
        $form = $this->createForm(HomeReplayVideoType::class, $homeReplayVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeReplayVideo);
            $entityManager->flush();

            return $this->redirectToRoute('home_replay_video_index');
        }

        return $this->render('home_replay_video/new.html.twig', [
            'home_replay_video' => $homeReplayVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_replay_video_show", methods={"GET"})
     */
    public function show(HomeReplayVideo $homeReplayVideo): Response
    {
        return $this->render('home_replay_video/show.html.twig', [
            'home_replay_video' => $homeReplayVideo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_replay_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeReplayVideo $homeReplayVideo): Response
    {
        $form = $this->createForm(HomeReplayVideoType::class, $homeReplayVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_replay_video_index');
        }

        return $this->render('home_replay_video/edit.html.twig', [
            'home_replay_video' => $homeReplayVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_replay_video_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeReplayVideo $homeReplayVideo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeReplayVideo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeReplayVideo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_replay_video_index');
    }
}
