<?php

namespace App\Controller;

use App\Entity\HomeBanniere;
use App\Form\HomeBanniereType;
use App\Repository\HomeBanniereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/banniere")
 */
class HomeBanniereController extends AbstractController
{
    /**
     * @Route("/", name="home_banniere_index", methods={"GET"})
     */
    public function index(HomeBanniereRepository $homeBanniereRepository): Response
    {
        return $this->render('home_banniere/index.html.twig', [
            'home_bannieres' => $homeBanniereRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_banniere_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeBanniere = new HomeBanniere();
        $form = $this->createForm(HomeBanniereType::class, $homeBanniere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeBanniere);
            $entityManager->flush();
           // $this->addFlash('success', 'Enregistrement réussi avec succès');

            return $this->redirectToRoute('home_banniere_index');
        }

        return $this->render('home_banniere/new.html.twig', [
            'home_banniere' => $homeBanniere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_banniere_show", methods={"GET"})
     */
    public function show(HomeBanniere $homeBanniere): Response
    {
        return $this->render('home_banniere/show.html.twig', [
            'home_banniere' => $homeBanniere,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_banniere_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeBanniere $homeBanniere): Response
    {
        $homeBanniere2=$homeBanniere;
       // $deleteForm = $this->createDeleteForm($homeBanniere);
        $form = $this->createForm(HomeBanniereType::class, $homeBanniere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($homeBanniere->getPhoto()==null)
            {
                $homeBanniere->setPhoto($homeBanniere2->getPhoto());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_banniere_index');
        }

        return $this->render('home_banniere/edit.html.twig', [
            'home_banniere' => $homeBanniere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/Photo", name="bannière_Photo", methods={"GET","POST"})
     */
    public function photo(Request $request, HomeBanniere $homeBanniere)
    {
        
           $homeBanniere->setPhoto(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_banniere_edit', array('id' => $homeBanniere->getId()));
    }

    /**
     * @Route("/delete/{id}", name="home_banniere_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, HomeBanniere $homeBanniere): Response
    {
         $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeBanniere);
            $entityManager->flush();

        return $this->redirectToRoute('home_banniere_index');
    }
}
