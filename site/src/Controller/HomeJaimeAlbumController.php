<?php

namespace App\Controller;

use App\Entity\HomeJaimeAlbum;
use App\Form\HomeJaimeAlbumType;
use App\Repository\HomeJaimeAlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/album")
 */
class HomeJaimeAlbumController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_album_index", methods={"GET"})
     */
    public function index(HomeJaimeAlbumRepository $homeJaimeAlbumRepository): Response
    {
        return $this->render('home_jaime_album/index.html.twig', [
            'home_jaime_albums' => $homeJaimeAlbumRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_album_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimeAlbum = new HomeJaimeAlbum();
        $form = $this->createForm(HomeJaimeAlbumType::class, $homeJaimeAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimeAlbum);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_album_index');
        }

        return $this->render('home_jaime_album/new.html.twig', [
            'home_jaime_album' => $homeJaimeAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_album_show", methods={"GET"})
     */
    public function show(HomeJaimeAlbum $homeJaimeAlbum): Response
    {
        return $this->render('home_jaime_album/show.html.twig', [
            'home_jaime_album' => $homeJaimeAlbum,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_album_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimeAlbum $homeJaimeAlbum): Response
    {
        $form = $this->createForm(HomeJaimeAlbumType::class, $homeJaimeAlbum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_album_index');
        }

        return $this->render('home_jaime_album/edit.html.twig', [
            'home_jaime_album' => $homeJaimeAlbum,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_album_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimeAlbum $homeJaimeAlbum): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimeAlbum->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimeAlbum);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_album_index');
    }
}
