<?php

namespace App\Controller;

use App\Entity\HomeJaimePasVideo;
use App\Form\HomeJaimePasVideoType;
use App\Repository\HomeJaimePasVideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/pas/video")
 */
class HomeJaimePasVideoController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_pas_video_index", methods={"GET"})
     */
    public function index(HomeJaimePasVideoRepository $homeJaimePasVideoRepository): Response
    {
        return $this->render('home_jaime_pas_video/index.html.twig', [
            'home_jaime_pas_videos' => $homeJaimePasVideoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_pas_video_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimePasVideo = new HomeJaimePasVideo();
        $form = $this->createForm(HomeJaimePasVideoType::class, $homeJaimePasVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimePasVideo);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_pas_video_index');
        }

        return $this->render('home_jaime_pas_video/new.html.twig', [
            'home_jaime_pas_video' => $homeJaimePasVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_video_show", methods={"GET"})
     */
    public function show(HomeJaimePasVideo $homeJaimePasVideo): Response
    {
        return $this->render('home_jaime_pas_video/show.html.twig', [
            'home_jaime_pas_video' => $homeJaimePasVideo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_pas_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimePasVideo $homeJaimePasVideo): Response
    {
        $form = $this->createForm(HomeJaimePasVideoType::class, $homeJaimePasVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_pas_video_index');
        }

        return $this->render('home_jaime_pas_video/edit.html.twig', [
            'home_jaime_pas_video' => $homeJaimePasVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_video_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimePasVideo $homeJaimePasVideo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimePasVideo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimePasVideo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_pas_video_index');
    }
}
