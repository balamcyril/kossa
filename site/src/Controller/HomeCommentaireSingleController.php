<?php

namespace App\Controller;

use App\Entity\HomeCommentaireSingle;
use App\Form\HomeCommentaireSingleType;
use App\Repository\HomeCommentaireSingleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/commentaire/single")
 */
class HomeCommentaireSingleController extends AbstractController
{
    /**
     * @Route("/", name="home_commentaire_single_index", methods={"GET"})
     */
    public function index(HomeCommentaireSingleRepository $homeCommentaireSingleRepository): Response
    {
        return $this->render('home_commentaire_single/index.html.twig', [
            'home_commentaire_singles' => $homeCommentaireSingleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_commentaire_single_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeCommentaireSingle = new HomeCommentaireSingle();
        $form = $this->createForm(HomeCommentaireSingleType::class, $homeCommentaireSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeCommentaireSingle);
            $entityManager->flush();

            return $this->redirectToRoute('home_commentaire_single_index');
        }

        return $this->render('home_commentaire_single/new.html.twig', [
            'home_commentaire_single' => $homeCommentaireSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_single_show", methods={"GET"})
     */
    public function show(HomeCommentaireSingle $homeCommentaireSingle): Response
    {
        return $this->render('home_commentaire_single/show.html.twig', [
            'home_commentaire_single' => $homeCommentaireSingle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_commentaire_single_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeCommentaireSingle $homeCommentaireSingle): Response
    {
        $form = $this->createForm(HomeCommentaireSingleType::class, $homeCommentaireSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_commentaire_single_index');
        }

        return $this->render('home_commentaire_single/edit.html.twig', [
            'home_commentaire_single' => $homeCommentaireSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_single_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeCommentaireSingle $homeCommentaireSingle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeCommentaireSingle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeCommentaireSingle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_commentaire_single_index');
    }
}
