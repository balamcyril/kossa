<?php

namespace App\Controller;

use App\Entity\PlaySingle;
use App\Form\PlaySingleType;
use App\Repository\PlaySingleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/play/single")
 */
class PlaySingleController extends AbstractController
{
    /**
     * @Route("/", name="play_single_index", methods={"GET"})
     */
    public function index(PlaySingleRepository $playSingleRepository): Response
    {
        return $this->render('play_single/index.html.twig', [
            'play_singles' => $playSingleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="play_single_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playSingle = new PlaySingle();
        $form = $this->createForm(PlaySingleType::class, $playSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playSingle);
            $entityManager->flush();

            return $this->redirectToRoute('play_single_index');
        }

        return $this->render('play_single/new.html.twig', [
            'play_single' => $playSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_single_show", methods={"GET"})
     */
    public function show(PlaySingle $playSingle): Response
    {
        return $this->render('play_single/show.html.twig', [
            'play_single' => $playSingle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_single_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlaySingle $playSingle): Response
    {
        $playSingle2 = $playSingle;
        $form = $this->createForm(PlaySingleType::class, $playSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($playSingle->getPhoto()==null)
            {
                $playSingle->setPhoto($playSingle2->getPhoto());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_single_index');
        }

        return $this->render('play_single/edit.html.twig', [
            'play_single' => $playSingle,
            'form' => $form->createView(),
        ]);
    }

     /**
     * @Route("/{id}/Photo", name="play_single_Photo", methods={"GET","POST"})
     */
    public function photo1(Request $request,  PlaySingle $playSingle)
    {
        
           $playSingle->setPhoto(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('playSingle2_edit', array('id' => $playSingle->getId()));
    }

    /**
     * @Route("/delete/{id}", name="play_single_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, PlaySingle $playSingle): Response
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playSingle);
            $entityManager->flush();
       

        return $this->redirectToRoute('play_single_index');
    }
}
