<?php

namespace App\Controller;

use App\Entity\HomeJaimeActualite;
use App\Form\HomeJaimeActualiteType;
use App\Repository\HomeJaimeActualiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/actualite")
 */
class HomeJaimeActualiteController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_actualite_index", methods={"GET"})
     */
    public function index(HomeJaimeActualiteRepository $homeJaimeActualiteRepository): Response
    {
        return $this->render('home_jaime_actualite/index.html.twig', [
            'home_jaime_actualites' => $homeJaimeActualiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_actualite_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimeActualite = new HomeJaimeActualite();
        $form = $this->createForm(HomeJaimeActualiteType::class, $homeJaimeActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimeActualite);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_actualite_index');
        }

        return $this->render('home_jaime_actualite/new.html.twig', [
            'home_jaime_actualite' => $homeJaimeActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_actualite_show", methods={"GET"})
     */
    public function show(HomeJaimeActualite $homeJaimeActualite): Response
    {
        return $this->render('home_jaime_actualite/show.html.twig', [
            'home_jaime_actualite' => $homeJaimeActualite,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_actualite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimeActualite $homeJaimeActualite): Response
    {
        $form = $this->createForm(HomeJaimeActualiteType::class, $homeJaimeActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_actualite_index');
        }

        return $this->render('home_jaime_actualite/edit.html.twig', [
            'home_jaime_actualite' => $homeJaimeActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_actualite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimeActualite $homeJaimeActualite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimeActualite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimeActualite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_actualite_index');
    }
}
