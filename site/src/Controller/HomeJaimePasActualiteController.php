<?php

namespace App\Controller;

use App\Entity\HomeJaimePasActualite;
use App\Form\HomeJaimePasActualiteType;
use App\Repository\HomeJaimePasActualiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/pas/actualite")
 */
class HomeJaimePasActualiteController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_pas_actualite_index", methods={"GET"})
     */
    public function index(HomeJaimePasActualiteRepository $homeJaimePasActualiteRepository): Response
    {
        return $this->render('home_jaime_pas_actualite/index.html.twig', [
            'home_jaime_pas_actualites' => $homeJaimePasActualiteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_pas_actualite_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimePasActualite = new HomeJaimePasActualite();
        $form = $this->createForm(HomeJaimePasActualiteType::class, $homeJaimePasActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimePasActualite);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_pas_actualite_index');
        }

        return $this->render('home_jaime_pas_actualite/new.html.twig', [
            'home_jaime_pas_actualite' => $homeJaimePasActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_actualite_show", methods={"GET"})
     */
    public function show(HomeJaimePasActualite $homeJaimePasActualite): Response
    {
        return $this->render('home_jaime_pas_actualite/show.html.twig', [
            'home_jaime_pas_actualite' => $homeJaimePasActualite,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_pas_actualite_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimePasActualite $homeJaimePasActualite): Response
    {
        $form = $this->createForm(HomeJaimePasActualiteType::class, $homeJaimePasActualite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_pas_actualite_index');
        }

        return $this->render('home_jaime_pas_actualite/edit.html.twig', [
            'home_jaime_pas_actualite' => $homeJaimePasActualite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_actualite_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimePasActualite $homeJaimePasActualite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimePasActualite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimePasActualite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_pas_actualite_index');
    }
}
