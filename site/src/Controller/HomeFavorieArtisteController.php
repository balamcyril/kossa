<?php

namespace App\Controller;

use App\Entity\HomeFavorieArtiste;
use App\Form\HomeFavorieArtisteType;
use App\Repository\HomeFavorieArtisteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/favorie/artiste")
 */
class HomeFavorieArtisteController extends AbstractController
{
    /**
     * @Route("/", name="home_favorie_artiste_index", methods={"GET"})
     */
    public function index(HomeFavorieArtisteRepository $homeFavorieArtisteRepository): Response
    {
        return $this->render('home_favorie_artiste/index.html.twig', [
            'home_favorie_artistes' => $homeFavorieArtisteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_favorie_artiste_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeFavorieArtiste = new HomeFavorieArtiste();
        $form = $this->createForm(HomeFavorieArtisteType::class, $homeFavorieArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeFavorieArtiste);
            $entityManager->flush();

            return $this->redirectToRoute('home_favorie_artiste_index');
        }

        return $this->render('home_favorie_artiste/new.html.twig', [
            'home_favorie_artiste' => $homeFavorieArtiste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_artiste_show", methods={"GET"})
     */
    public function show(HomeFavorieArtiste $homeFavorieArtiste): Response
    {
        return $this->render('home_favorie_artiste/show.html.twig', [
            'home_favorie_artiste' => $homeFavorieArtiste,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_favorie_artiste_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeFavorieArtiste $homeFavorieArtiste): Response
    {
        $form = $this->createForm(HomeFavorieArtisteType::class, $homeFavorieArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_favorie_artiste_index');
        }

        return $this->render('home_favorie_artiste/edit.html.twig', [
            'home_favorie_artiste' => $homeFavorieArtiste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_favorie_artiste_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeFavorieArtiste $homeFavorieArtiste): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeFavorieArtiste->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeFavorieArtiste);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_favorie_artiste_index');
    }
}
