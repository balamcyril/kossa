<?php

namespace App\Controller;

use App\Entity\HomeCommentaireVideo;
use App\Form\HomeCommentaireVideoType;
use App\Repository\HomeCommentaireVideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/commentaire/video")
 */
class HomeCommentaireVideoController extends AbstractController
{
    /**
     * @Route("/", name="home_commentaire_video_index", methods={"GET"})
     */
    public function index(HomeCommentaireVideoRepository $homeCommentaireVideoRepository): Response
    {
        return $this->render('home_commentaire_video/index.html.twig', [
            'home_commentaire_videos' => $homeCommentaireVideoRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_commentaire_video_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeCommentaireVideo = new HomeCommentaireVideo();
        $form = $this->createForm(HomeCommentaireVideoType::class, $homeCommentaireVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeCommentaireVideo);
            $entityManager->flush();

            return $this->redirectToRoute('home_commentaire_video_index');
        }

        return $this->render('home_commentaire_video/new.html.twig', [
            'home_commentaire_video' => $homeCommentaireVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_video_show", methods={"GET"})
     */
    public function show(HomeCommentaireVideo $homeCommentaireVideo): Response
    {
        return $this->render('home_commentaire_video/show.html.twig', [
            'home_commentaire_video' => $homeCommentaireVideo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_commentaire_video_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeCommentaireVideo $homeCommentaireVideo): Response
    {
        $form = $this->createForm(HomeCommentaireVideoType::class, $homeCommentaireVideo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_commentaire_video_index');
        }

        return $this->render('home_commentaire_video/edit.html.twig', [
            'home_commentaire_video' => $homeCommentaireVideo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_commentaire_video_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeCommentaireVideo $homeCommentaireVideo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeCommentaireVideo->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeCommentaireVideo);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_commentaire_video_index');
    }
}
