<?php

namespace App\Controller;

use App\Entity\HomeJaimePasArtiste;
use App\Form\HomeJaimePasArtisteType;
use App\Repository\HomeJaimePasArtisteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home/jaime/pas/artiste")
 */
class HomeJaimePasArtisteController extends AbstractController
{
    /**
     * @Route("/", name="home_jaime_pas_artiste_index", methods={"GET"})
     */
    public function index(HomeJaimePasArtisteRepository $homeJaimePasArtisteRepository): Response
    {
        return $this->render('home_jaime_pas_artiste/index.html.twig', [
            'home_jaime_pas_artistes' => $homeJaimePasArtisteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="home_jaime_pas_artiste_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $homeJaimePasArtiste = new HomeJaimePasArtiste();
        $form = $this->createForm(HomeJaimePasArtisteType::class, $homeJaimePasArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($homeJaimePasArtiste);
            $entityManager->flush();

            return $this->redirectToRoute('home_jaime_pas_artiste_index');
        }

        return $this->render('home_jaime_pas_artiste/new.html.twig', [
            'home_jaime_pas_artiste' => $homeJaimePasArtiste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_artiste_show", methods={"GET"})
     */
    public function show(HomeJaimePasArtiste $homeJaimePasArtiste): Response
    {
        return $this->render('home_jaime_pas_artiste/show.html.twig', [
            'home_jaime_pas_artiste' => $homeJaimePasArtiste,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_jaime_pas_artiste_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, HomeJaimePasArtiste $homeJaimePasArtiste): Response
    {
        $form = $this->createForm(HomeJaimePasArtisteType::class, $homeJaimePasArtiste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_jaime_pas_artiste_index');
        }

        return $this->render('home_jaime_pas_artiste/edit.html.twig', [
            'home_jaime_pas_artiste' => $homeJaimePasArtiste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_jaime_pas_artiste_delete", methods={"DELETE"})
     */
    public function delete(Request $request, HomeJaimePasArtiste $homeJaimePasArtiste): Response
    {
        if ($this->isCsrfTokenValid('delete'.$homeJaimePasArtiste->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($homeJaimePasArtiste);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_jaime_pas_artiste_index');
    }
}
