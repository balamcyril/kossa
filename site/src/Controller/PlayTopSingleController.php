<?php

namespace App\Controller;

use App\Entity\PlayTopSingle;
use App\Form\PlayTopSingleType;
use App\Repository\PlayTopSingleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/play/top/single")
 */
class PlayTopSingleController extends AbstractController
{
    /**
     * @Route("/", name="play_top_single_index", methods={"GET"})
     */
    public function index(PlayTopSingleRepository $playTopSingleRepository): Response
    {
        return $this->render('play_top_single/index.html.twig', [
            'play_top_singles' => $playTopSingleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="play_top_single_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $playTopSingle = new PlayTopSingle();
        $form = $this->createForm(PlayTopSingleType::class, $playTopSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($playTopSingle);
            $entityManager->flush();

            return $this->redirectToRoute('play_top_single_index');
        }

        return $this->render('play_top_single/new.html.twig', [
            'play_top_single' => $playTopSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="play_top_single_show", methods={"GET"})
     */
    public function show(PlayTopSingle $playTopSingle): Response
    {
        return $this->render('play_top_single/show.html.twig', [
            'play_top_single' => $playTopSingle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="play_top_single_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PlayTopSingle $playTopSingle): Response
    {
        $playTopSingle2 = $playTopSingle;
        $form = $this->createForm(PlayTopSingleType::class, $playTopSingle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            if($playTopSingle->getPhoto()==null)
            {
                $playTopSingle->setPhoto($playTopSingle2->getPhoto());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_top_single_index');
        }

        return $this->render('play_top_single/edit.html.twig', [
            'play_top_single' => $playTopSingle,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/Photo", name="play_top_single_Photo", methods={"GET","POST"})
     */
    public function photo(Request $request,  PlayTopSingle $playTopSingle)
    {
        
           $playTopSingle->setPhoto(null);
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('play_top_single_edit', array('id' => $playTopSingle->getId()));
    }

    /**
     * @Route("/delete/{id}", name="play_top_single_delete", methods={"GET","POST"})
     */
    public function delete(Request $request, PlayTopSingle $playTopSingle): Response
    {
         $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($playTopSingle);
            $entityManager->flush();


        return $this->redirectToRoute('play_top_single_index');
    }
}
