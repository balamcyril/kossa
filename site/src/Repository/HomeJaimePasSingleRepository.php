<?php

namespace App\Repository;

use App\Entity\HomeJaimePasSingle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeJaimePasSingle|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeJaimePasSingle|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeJaimePasSingle[]    findAll()
 * @method HomeJaimePasSingle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeJaimePasSingleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeJaimePasSingle::class);
    }

    // /**
    //  * @return HomeJaimePasSingle[] Returns an array of HomeJaimePasSingle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeJaimePasSingle
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
