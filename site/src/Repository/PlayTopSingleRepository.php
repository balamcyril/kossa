<?php

namespace App\Repository;

use App\Entity\PlayTopSingle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayTopSingle|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayTopSingle|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayTopSingle[]    findAll()
 * @method PlayTopSingle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayTopSingleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayTopSingle::class);
    }

    // /**
    //  * @return PlayTopSingle[] Returns an array of PlayTopSingle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayTopSingle
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
