<?php

namespace App\Repository;

use App\Entity\PlayTopVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayTopVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayTopVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayTopVideo[]    findAll()
 * @method PlayTopVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayTopVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayTopVideo::class);
    }

    // /**
    //  * @return PlayTopVideo[] Returns an array of PlayTopVideo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayTopVideo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
