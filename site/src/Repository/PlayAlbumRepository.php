<?php

namespace App\Repository;

use App\Entity\PlayAlbum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayAlbum|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayAlbum|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayAlbum[]    findAll()
 * @method PlayAlbum[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayAlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayAlbum::class);
    }

    // /**
    //  * @return PlayAlbum[] Returns an array of PlayAlbum objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayAlbum
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
