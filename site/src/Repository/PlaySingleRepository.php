<?php

namespace App\Repository;

use App\Entity\PlaySingle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaySingle|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaySingle|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaySingle[]    findAll()
 * @method PlaySingle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaySingleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaySingle::class);
    }

    // /**
    //  * @return PlaySingle[] Returns an array of PlaySingle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaySingle
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
