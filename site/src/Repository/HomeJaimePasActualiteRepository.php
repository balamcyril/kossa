<?php

namespace App\Repository;

use App\Entity\HomeJaimePasActualite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeJaimePasActualite|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeJaimePasActualite|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeJaimePasActualite[]    findAll()
 * @method HomeJaimePasActualite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeJaimePasActualiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeJaimePasActualite::class);
    }

    // /**
    //  * @return HomeJaimePasActualite[] Returns an array of HomeJaimePasActualite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeJaimePasActualite
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
