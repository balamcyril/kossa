<?php

namespace App\Repository;

use App\Entity\HomeJaimeGenreMusicaux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeJaimeGenreMusicaux|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeJaimeGenreMusicaux|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeJaimeGenreMusicaux[]    findAll()
 * @method HomeJaimeGenreMusicaux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeJaimeGenreMusicauxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeJaimeGenreMusicaux::class);
    }

    // /**
    //  * @return HomeJaimeGenreMusicaux[] Returns an array of HomeJaimeGenreMusicaux objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeJaimeGenreMusicaux
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
