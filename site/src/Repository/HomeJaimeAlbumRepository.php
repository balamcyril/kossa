<?php

namespace App\Repository;

use App\Entity\HomeJaimeAlbum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeJaimeAlbum|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeJaimeAlbum|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeJaimeAlbum[]    findAll()
 * @method HomeJaimeAlbum[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeJaimeAlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeJaimeAlbum::class);
    }

    // /**
    //  * @return HomeJaimeAlbum[] Returns an array of HomeJaimeAlbum objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeJaimeAlbum
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
