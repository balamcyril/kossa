<?php

namespace App\Repository;

use App\Entity\HomeFavorieGenreMusicaux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeFavorieGenreMusicaux|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeFavorieGenreMusicaux|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeFavorieGenreMusicaux[]    findAll()
 * @method HomeFavorieGenreMusicaux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeFavorieGenreMusicauxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeFavorieGenreMusicaux::class);
    }

    // /**
    //  * @return HomeFavorieGenreMusicaux[] Returns an array of HomeFavorieGenreMusicaux objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeFavorieGenreMusicaux
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
