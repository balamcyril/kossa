<?php

namespace App\Repository;

use App\Entity\HomeJaimeArtiste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeJaimeArtiste|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeJaimeArtiste|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeJaimeArtiste[]    findAll()
 * @method HomeJaimeArtiste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeJaimeArtisteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeJaimeArtiste::class);
    }

    // /**
    //  * @return HomeJaimeArtiste[] Returns an array of HomeJaimeArtiste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeJaimeArtiste
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
