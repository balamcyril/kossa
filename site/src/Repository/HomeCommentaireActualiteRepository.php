<?php

namespace App\Repository;

use App\Entity\HomeCommentaireActualite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeCommentaireActualite|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeCommentaireActualite|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeCommentaireActualite[]    findAll()
 * @method HomeCommentaireActualite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeCommentaireActualiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeCommentaireActualite::class);
    }

    // /**
    //  * @return HomeCommentaireActualite[] Returns an array of HomeCommentaireActualite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeCommentaireActualite
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
