<?php

namespace App\Repository;

use App\Entity\HomeBanniere;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeBanniere|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeBanniere|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeBanniere[]    findAll()
 * @method HomeBanniere[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeBanniereRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeBanniere::class);
    }

    // /**
    //  * @return HomeBanniere[] Returns an array of HomeBanniere objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeBanniere
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
