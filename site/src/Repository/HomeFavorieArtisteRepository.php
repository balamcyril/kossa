<?php

namespace App\Repository;

use App\Entity\HomeFavorieArtiste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeFavorieArtiste|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeFavorieArtiste|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeFavorieArtiste[]    findAll()
 * @method HomeFavorieArtiste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeFavorieArtisteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeFavorieArtiste::class);
    }

    // /**
    //  * @return HomeFavorieArtiste[] Returns an array of HomeFavorieArtiste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeFavorieArtiste
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
