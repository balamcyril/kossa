<?php

namespace App\Repository;

use App\Entity\NewsCategorieActualite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NewsCategorieActualite|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsCategorieActualite|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsCategorieActualite[]    findAll()
 * @method NewsCategorieActualite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsCategorieActualiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsCategorieActualite::class);
    }

    // /**
    //  * @return NewsCategorieActualite[] Returns an array of NewsCategorieActualite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewsCategorieActualite
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
