<?php

namespace App\Repository;

use App\Entity\HomeFavorieVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeFavorieVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeFavorieVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeFavorieVideo[]    findAll()
 * @method HomeFavorieVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeFavorieVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeFavorieVideo::class);
    }

    // /**
    //  * @return HomeFavorieVideo[] Returns an array of HomeFavorieVideo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeFavorieVideo
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
