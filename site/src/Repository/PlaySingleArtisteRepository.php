<?php

namespace App\Repository;

use App\Entity\PlaySingleArtiste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaySingleArtiste|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaySingleArtiste|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaySingleArtiste[]    findAll()
 * @method PlaySingleArtiste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaySingleArtisteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaySingleArtiste::class);
    }

    // /**
    //  * @return PlaySingleArtiste[] Returns an array of PlaySingleArtiste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaySingleArtiste
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
