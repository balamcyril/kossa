<?php

namespace App\Repository;

use App\Entity\HomeFavorieSingle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeFavorieSingle|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeFavorieSingle|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeFavorieSingle[]    findAll()
 * @method HomeFavorieSingle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeFavorieSingleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeFavorieSingle::class);
    }

    // /**
    //  * @return HomeFavorieSingle[] Returns an array of HomeFavorieSingle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeFavorieSingle
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
