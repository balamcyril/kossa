<?php

namespace App\Repository;

use App\Entity\PlayGenreMusicaux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayGenreMusicaux|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayGenreMusicaux|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayGenreMusicaux[]    findAll()
 * @method PlayGenreMusicaux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayGenreMusicauxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayGenreMusicaux::class);
    }

    // /**
    //  * @return PlayGenreMusicaux[] Returns an array of PlayGenreMusicaux objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayGenreMusicaux
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
