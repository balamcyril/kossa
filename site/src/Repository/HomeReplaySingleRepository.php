<?php

namespace App\Repository;

use App\Entity\HomeReplaySingle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeReplaySingle|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeReplaySingle|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeReplaySingle[]    findAll()
 * @method HomeReplaySingle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeReplaySingleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeReplaySingle::class);
    }

    // /**
    //  * @return HomeReplaySingle[] Returns an array of HomeReplaySingle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeReplaySingle
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
