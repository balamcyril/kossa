<?php

namespace App\Repository;

use App\Entity\PlaySingleGenreMusicaux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaySingleGenreMusicaux|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaySingleGenreMusicaux|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaySingleGenreMusicaux[]    findAll()
 * @method PlaySingleGenreMusicaux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaySingleGenreMusicauxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaySingleGenreMusicaux::class);
    }

    // /**
    //  * @return PlaySingleGenreMusicaux[] Returns an array of PlaySingleGenreMusicaux objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlaySingleGenreMusicaux
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
