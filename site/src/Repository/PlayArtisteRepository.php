<?php

namespace App\Repository;

use App\Entity\PlayArtiste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayArtiste|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayArtiste|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayArtiste[]    findAll()
 * @method PlayArtiste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayArtisteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayArtiste::class);
    }

    // /**
    //  * @return PlayArtiste[] Returns an array of PlayArtiste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayArtiste
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
