<?php

namespace App\Repository;

use App\Entity\HomeFavorieAlbum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeFavorieAlbum|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeFavorieAlbum|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeFavorieAlbum[]    findAll()
 * @method HomeFavorieAlbum[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeFavorieAlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeFavorieAlbum::class);
    }

    // /**
    //  * @return HomeFavorieAlbum[] Returns an array of HomeFavorieAlbum objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeFavorieAlbum
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
