<?php

namespace App\Repository;

use App\Entity\HomeJaimePasVideo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeJaimePasVideo|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeJaimePasVideo|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeJaimePasVideo[]    findAll()
 * @method HomeJaimePasVideo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeJaimePasVideoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeJaimePasVideo::class);
    }

    // /**
    //  * @return HomeJaimePasVideo[] Returns an array of HomeJaimePasVideo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeJaimePasVideo
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
