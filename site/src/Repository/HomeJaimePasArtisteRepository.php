<?php

namespace App\Repository;

use App\Entity\HomeJaimePasArtiste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeJaimePasArtiste|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeJaimePasArtiste|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeJaimePasArtiste[]    findAll()
 * @method HomeJaimePasArtiste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeJaimePasArtisteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeJaimePasArtiste::class);
    }

    // /**
    //  * @return HomeJaimePasArtiste[] Returns an array of HomeJaimePasArtiste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeJaimePasArtiste
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
