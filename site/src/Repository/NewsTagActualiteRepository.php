<?php

namespace App\Repository;

use App\Entity\NewsTagActualite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NewsTagActualite|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsTagActualite|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsTagActualite[]    findAll()
 * @method NewsTagActualite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsTagActualiteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewsTagActualite::class);
    }

    // /**
    //  * @return NewsTagActualite[] Returns an array of NewsTagActualite objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NewsTagActualite
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
