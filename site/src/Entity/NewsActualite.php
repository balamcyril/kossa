<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsActualiteRepository")
 * @Vich\Uploadable
 */
class NewsActualite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NewsCategorieActualite", inversedBy="newsActualites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $newCategorieActualite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenu1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo1;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="actualite_photo1", fileNameProperty="photo1")
     */
    private $fichierPhoto1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contenu2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo2;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="actualite_photo2", fileNameProperty="photo2")
     */
    private $fichierPhoto2;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="newsActualites")
     * @ORM\JoinColumn(nullable=true)
     */
    private $Play_single;

    /**
     * @ORM\Column(type="boolean")
     */
    private $etat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDePublication;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    /**
     * @ORM\Column(type="integer")
     */
    private $vue = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="newsActualites")
     * @ORM\JoinColumn(nullable=true)
     */
    private $play_video;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsTagActualite", mappedBy="news_actualite", orphanRemoval=true)
     */
    private $newsTagActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeActualite", mappedBy="actualite", orphanRemoval=true)
     */
    private $homeJaimeActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasActualite", mappedBy="actualite", orphanRemoval=true)
     */
    private $homeJaimePasActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireActualite", mappedBy="actualite", orphanRemoval=true)
     */
    private $homeCommentaireActualites;

    public function __construct()
    {
        $this->dateEnreg = new \DateTime('now');
        $this->newsTagActualites = new ArrayCollection();
        $this->homeJaimeActualites = new ArrayCollection();
        $this->homeJaimePasActualites = new ArrayCollection();
        $this->homeCommentaireActualites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getNewCategorieActualite(): ?NewsCategorieActualite
    {
        return $this->newCategorieActualite;
    }

    public function setNewCategorieActualite(?NewsCategorieActualite $newCategorieActualite): self
    {
        $this->newCategorieActualite = $newCategorieActualite;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContenu1(): ?string
    {
        return $this->contenu1;
    }

    public function setContenu1(string $contenu1): self
    {
        $this->contenu1 = $contenu1;

        return $this;
    }

    public function getPhoto1(): ?string
    {
        return $this->photo1;
    }

    public function setPhoto1(string $photo1): self
    {
        $this->photo1 = $photo1;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto1
     *
     * @return NewsActualite
    */
    public function setFichierPhoto1(File $fichierPhoto1 = null)
    {
        $this->fichierPhoto1 = $fichierPhoto1;
        if ($fichierPhoto1) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto1()
    {
        return $this->fichierPhoto1;
    }

    public function getContenu2(): ?string
    {
        return $this->contenu2;
    }

    public function setContenu2(string $contenu2): self
    {
        $this->contenu2 = $contenu2;

        return $this;
    }

    public function getPhoto2(): ?string
    {
        return $this->photo2;
    }

    public function setPhoto2(string $photo2): self
    {
        $this->photo2 = $photo2;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto2
     *
     * @return NewsActualite
    */
    public function setFichierPhoto2(File $fichierPhoto2)
    {
        $this->fichierPhoto2 = $fichierPhoto2;
        if ($fichierPhoto2) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto2()
    {
        return $this->fichierPhoto2;
    }


    public function getPlaySingle(): ?PlaySingle
    {
        return $this->Play_single;
    }

    public function setPlaySingle(?PlaySingle $Play_single): self
    {
        $this->Play_single = $Play_single;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getDateDePublication(): ?\DateTimeInterface
    {
        return $this->dateDePublication;
    }

    public function setDateDePublication(\DateTimeInterface $dateDePublication): self
    {
        $this->dateDePublication = $dateDePublication;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    public function getVue(): ?int
    {
        return $this->vue;
    }

    public function setVue(int $vue): self
    {
        $this->vue = $vue;

        return $this;
    }

    public function getPlayVideo(): ?PlayVideo
    {
        return $this->play_video;
    }

    public function setPlayVideo(?PlayVideo $play_video): self
    {
        $this->play_video = $play_video;

        return $this;
    }

    /**
     * @return Collection|NewsTagActualite[]
     */
    public function getNewsTagActualites(): Collection
    {
        return $this->newsTagActualites;
    }

    public function addNewsTagActualite(NewsTagActualite $newsTagActualite): self
    {
        if (!$this->newsTagActualites->contains($newsTagActualite)) {
            $this->newsTagActualites[] = $newsTagActualite;
            $newsTagActualite->setNewsActualite($this);
        }

        return $this;
    }

    public function removeNewsTagActualite(NewsTagActualite $newsTagActualite): self
    {
        if ($this->newsTagActualites->contains($newsTagActualite)) {
            $this->newsTagActualites->removeElement($newsTagActualite);
            // set the owning side to null (unless already changed)
            if ($newsTagActualite->getNewsActualite() === $this) {
                $newsTagActualite->setNewsActualite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeActualite[]
     */
    public function getHomeJaimeActualites(): Collection
    {
        return $this->homeJaimeActualites;
    }

    public function addHomeJaimeActualite(HomeJaimeActualite $homeJaimeActualite): self
    {
        if (!$this->homeJaimeActualites->contains($homeJaimeActualite)) {
            $this->homeJaimeActualites[] = $homeJaimeActualite;
            $homeJaimeActualite->setActualite($this);
        }

        return $this;
    }

    public function removeHomeJaimeActualite(HomeJaimeActualite $homeJaimeActualite): self
    {
        if ($this->homeJaimeActualites->contains($homeJaimeActualite)) {
            $this->homeJaimeActualites->removeElement($homeJaimeActualite);
            // set the owning side to null (unless already changed)
            if ($homeJaimeActualite->getActualite() === $this) {
                $homeJaimeActualite->setActualite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasActualite[]
     */
    public function getHomeJaimePasActualites(): Collection
    {
        return $this->homeJaimePasActualites;
    }

    public function addHomeJaimePasActualite(HomeJaimePasActualite $homeJaimePasActualite): self
    {
        if (!$this->homeJaimePasActualites->contains($homeJaimePasActualite)) {
            $this->homeJaimePasActualites[] = $homeJaimePasActualite;
            $homeJaimePasActualite->setActualite($this);
        }

        return $this;
    }

    public function removeHomeJaimePasActualite(HomeJaimePasActualite $homeJaimePasActualite): self
    {
        if ($this->homeJaimePasActualites->contains($homeJaimePasActualite)) {
            $this->homeJaimePasActualites->removeElement($homeJaimePasActualite);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasActualite->getActualite() === $this) {
                $homeJaimePasActualite->setActualite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireActualite[]
     */
    public function getHomeCommentaireActualites(): Collection
    {
        return $this->homeCommentaireActualites;
    }

    public function addHomeCommentaireActualite(HomeCommentaireActualite $homeCommentaireActualite): self
    {
        if (!$this->homeCommentaireActualites->contains($homeCommentaireActualite)) {
            $this->homeCommentaireActualites[] = $homeCommentaireActualite;
            $homeCommentaireActualite->setActualite($this);
        }

        return $this;
    }

    public function removeHomeCommentaireActualite(HomeCommentaireActualite $homeCommentaireActualite): self
    {
        if ($this->homeCommentaireActualites->contains($homeCommentaireActualite)) {
            $this->homeCommentaireActualites->removeElement($homeCommentaireActualite);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireActualite->getActualite() === $this) {
                $homeCommentaireActualite->setActualite(null);
            }
        }

        return $this;
    }
}
