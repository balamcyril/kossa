<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeUtilisateurRepository")
 */
class HomeUtilisateur extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayTopSingle", mappedBy="auteur", orphanRemoval=true)
     */
    private $playTopSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayTopVideo", mappedBy="auteur", orphanRemoval=true)
     */
    private $playTopVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieArtiste", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeFavorieArtistes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieGenreMusicaux", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeFavorieGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieAlbum", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeFavorieAlbums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieSingle", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeFavorieSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieVideo", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeFavorieVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeActualite", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimeActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeArtiste", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimeArtistes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeGenreMusicaux", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimeGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeAlbum", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimeAlbums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeSingle", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimeSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeVideo", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimeVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasActualite", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimePasActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasArtiste", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimePasArtistes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasGenreMusicaux", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimePasGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasAlbum", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimePasAlbums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasSingle", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimePasSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasVideo", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeJaimePasVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireActualite", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeCommentaireActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireGenreMusicaux", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeCommentaireGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireSingle", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeCommentaireSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireVideo", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeCommentaireVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeReplaySingle", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeReplaySingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeReplayVideo", mappedBy="utilisateur", orphanRemoval=true)
     */
    private $homeReplayVideos;

    public function __construct()
    {
        parent::__construct();
        $this->playTopSingles = new ArrayCollection();
        $this->playTopVideos = new ArrayCollection();
        $this->homeFavorieArtistes = new ArrayCollection();
        $this->homeFavorieGenreMusicauxes = new ArrayCollection();
        $this->homeFavorieAlbums = new ArrayCollection();
        $this->homeFavorieSingles = new ArrayCollection();
        $this->homeFavorieVideos = new ArrayCollection();
        $this->homeJaimeActualites = new ArrayCollection();
        $this->homeJaimeArtistes = new ArrayCollection();
        $this->homeJaimeGenreMusicauxes = new ArrayCollection();
        $this->homeJaimeAlbums = new ArrayCollection();
        $this->homeJaimeSingles = new ArrayCollection();
        $this->homeJaimeVideos = new ArrayCollection();
        $this->homeJaimePasActualites = new ArrayCollection();
        $this->homeJaimePasArtistes = new ArrayCollection();
        $this->homeJaimePasGenreMusicauxes = new ArrayCollection();
        $this->homeJaimePasAlbums = new ArrayCollection();
        $this->homeJaimePasSingles = new ArrayCollection();
        $this->homeJaimePasVideos = new ArrayCollection();
        $this->homeCommentaireActualites = new ArrayCollection();
        $this->homeCommentaireGenreMusicauxes = new ArrayCollection();
        $this->homeCommentaireSingles = new ArrayCollection();
        $this->homeCommentaireVideos = new ArrayCollection();
        $this->homeReplaySingles = new ArrayCollection();
        $this->homeReplayVideos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|PlayTopSingle[]
     */
    public function getPlayTopSingles(): Collection
    {
        return $this->playTopSingles;
    }

    public function addPlayTopSingle(PlayTopSingle $playTopSingle): self
    {
        if (!$this->playTopSingles->contains($playTopSingle)) {
            $this->playTopSingles[] = $playTopSingle;
            $playTopSingle->setAuteur($this);
        }

        return $this;
    }

    public function removePlayTopSingle(PlayTopSingle $playTopSingle): self
    {
        if ($this->playTopSingles->contains($playTopSingle)) {
            $this->playTopSingles->removeElement($playTopSingle);
            // set the owning side to null (unless already changed)
            if ($playTopSingle->getAuteur() === $this) {
                $playTopSingle->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlayTopVideo[]
     */
    public function getPlayTopVideos(): Collection
    {
        return $this->playTopVideos;
    }

    public function addPlayTopVideo(PlayTopVideo $playTopVideo): self
    {
        if (!$this->playTopVideos->contains($playTopVideo)) {
            $this->playTopVideos[] = $playTopVideo;
            $playTopVideo->setAuteur($this);
        }

        return $this;
    }

    public function removePlayTopVideo(PlayTopVideo $playTopVideo): self
    {
        if ($this->playTopVideos->contains($playTopVideo)) {
            $this->playTopVideos->removeElement($playTopVideo);
            // set the owning side to null (unless already changed)
            if ($playTopVideo->getAuteur() === $this) {
                $playTopVideo->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeFavorieArtiste[]
     */
    public function getHomeFavorieArtistes(): Collection
    {
        return $this->homeFavorieArtistes;
    }

    public function addHomeFavorieArtiste(HomeFavorieArtiste $homeFavorieArtiste): self
    {
        if (!$this->homeFavorieArtistes->contains($homeFavorieArtiste)) {
            $this->homeFavorieArtistes[] = $homeFavorieArtiste;
            $homeFavorieArtiste->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeFavorieArtiste(HomeFavorieArtiste $homeFavorieArtiste): self
    {
        if ($this->homeFavorieArtistes->contains($homeFavorieArtiste)) {
            $this->homeFavorieArtistes->removeElement($homeFavorieArtiste);
            // set the owning side to null (unless already changed)
            if ($homeFavorieArtiste->getUtilisateur() === $this) {
                $homeFavorieArtiste->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeFavorieGenreMusicaux[]
     */
    public function getHomeFavorieGenreMusicauxes(): Collection
    {
        return $this->homeFavorieGenreMusicauxes;
    }

    public function addHomeFavorieGenreMusicaux(HomeFavorieGenreMusicaux $homeFavorieGenreMusicaux): self
    {
        if (!$this->homeFavorieGenreMusicauxes->contains($homeFavorieGenreMusicaux)) {
            $this->homeFavorieGenreMusicauxes[] = $homeFavorieGenreMusicaux;
            $homeFavorieGenreMusicaux->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeFavorieGenreMusicaux(HomeFavorieGenreMusicaux $homeFavorieGenreMusicaux): self
    {
        if ($this->homeFavorieGenreMusicauxes->contains($homeFavorieGenreMusicaux)) {
            $this->homeFavorieGenreMusicauxes->removeElement($homeFavorieGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeFavorieGenreMusicaux->getUtilisateur() === $this) {
                $homeFavorieGenreMusicaux->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeFavorieAlbum[]
     */
    public function getHomeFavorieAlbums(): Collection
    {
        return $this->homeFavorieAlbums;
    }

    public function addHomeFavorieAlbum(HomeFavorieAlbum $homeFavorieAlbum): self
    {
        if (!$this->homeFavorieAlbums->contains($homeFavorieAlbum)) {
            $this->homeFavorieAlbums[] = $homeFavorieAlbum;
            $homeFavorieAlbum->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeFavorieAlbum(HomeFavorieAlbum $homeFavorieAlbum): self
    {
        if ($this->homeFavorieAlbums->contains($homeFavorieAlbum)) {
            $this->homeFavorieAlbums->removeElement($homeFavorieAlbum);
            // set the owning side to null (unless already changed)
            if ($homeFavorieAlbum->getUtilisateur() === $this) {
                $homeFavorieAlbum->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeFavorieSingle[]
     */
    public function getHomeFavorieSingles(): Collection
    {
        return $this->homeFavorieSingles;
    }

    public function addHomeFavorieSingle(HomeFavorieSingle $homeFavorieSingle): self
    {
        if (!$this->homeFavorieSingles->contains($homeFavorieSingle)) {
            $this->homeFavorieSingles[] = $homeFavorieSingle;
            $homeFavorieSingle->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeFavorieSingle(HomeFavorieSingle $homeFavorieSingle): self
    {
        if ($this->homeFavorieSingles->contains($homeFavorieSingle)) {
            $this->homeFavorieSingles->removeElement($homeFavorieSingle);
            // set the owning side to null (unless already changed)
            if ($homeFavorieSingle->getUtilisateur() === $this) {
                $homeFavorieSingle->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeFavorieVideo[]
     */
    public function getHomeFavorieVideos(): Collection
    {
        return $this->homeFavorieVideos;
    }

    public function addHomeFavorieVideo(HomeFavorieVideo $homeFavorieVideo): self
    {
        if (!$this->homeFavorieVideos->contains($homeFavorieVideo)) {
            $this->homeFavorieVideos[] = $homeFavorieVideo;
            $homeFavorieVideo->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeFavorieVideo(HomeFavorieVideo $homeFavorieVideo): self
    {
        if ($this->homeFavorieVideos->contains($homeFavorieVideo)) {
            $this->homeFavorieVideos->removeElement($homeFavorieVideo);
            // set the owning side to null (unless already changed)
            if ($homeFavorieVideo->getUtilisateur() === $this) {
                $homeFavorieVideo->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeActualite[]
     */
    public function getHomeJaimeActualites(): Collection
    {
        return $this->homeJaimeActualites;
    }

    public function addHomeJaimeActualite(HomeJaimeActualite $homeJaimeActualite): self
    {
        if (!$this->homeJaimeActualites->contains($homeJaimeActualite)) {
            $this->homeJaimeActualites[] = $homeJaimeActualite;
            $homeJaimeActualite->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimeActualite(HomeJaimeActualite $homeJaimeActualite): self
    {
        if ($this->homeJaimeActualites->contains($homeJaimeActualite)) {
            $this->homeJaimeActualites->removeElement($homeJaimeActualite);
            // set the owning side to null (unless already changed)
            if ($homeJaimeActualite->getUtilisateur() === $this) {
                $homeJaimeActualite->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeArtiste[]
     */
    public function getHomeJaimeArtistes(): Collection
    {
        return $this->homeJaimeArtistes;
    }

    public function addHomeJaimeArtiste(HomeJaimeArtiste $homeJaimeArtiste): self
    {
        if (!$this->homeJaimeArtistes->contains($homeJaimeArtiste)) {
            $this->homeJaimeArtistes[] = $homeJaimeArtiste;
            $homeJaimeArtiste->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimeArtiste(HomeJaimeArtiste $homeJaimeArtiste): self
    {
        if ($this->homeJaimeArtistes->contains($homeJaimeArtiste)) {
            $this->homeJaimeArtistes->removeElement($homeJaimeArtiste);
            // set the owning side to null (unless already changed)
            if ($homeJaimeArtiste->getUtilisateur() === $this) {
                $homeJaimeArtiste->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeGenreMusicaux[]
     */
    public function getHomeJaimeGenreMusicauxes(): Collection
    {
        return $this->homeJaimeGenreMusicauxes;
    }

    public function addHomeJaimeGenreMusicaux(HomeJaimeGenreMusicaux $homeJaimeGenreMusicaux): self
    {
        if (!$this->homeJaimeGenreMusicauxes->contains($homeJaimeGenreMusicaux)) {
            $this->homeJaimeGenreMusicauxes[] = $homeJaimeGenreMusicaux;
            $homeJaimeGenreMusicaux->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimeGenreMusicaux(HomeJaimeGenreMusicaux $homeJaimeGenreMusicaux): self
    {
        if ($this->homeJaimeGenreMusicauxes->contains($homeJaimeGenreMusicaux)) {
            $this->homeJaimeGenreMusicauxes->removeElement($homeJaimeGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeJaimeGenreMusicaux->getUtilisateur() === $this) {
                $homeJaimeGenreMusicaux->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeAlbum[]
     */
    public function getHomeJaimeAlbums(): Collection
    {
        return $this->homeJaimeAlbums;
    }

    public function addHomeJaimeAlbum(HomeJaimeAlbum $homeJaimeAlbum): self
    {
        if (!$this->homeJaimeAlbums->contains($homeJaimeAlbum)) {
            $this->homeJaimeAlbums[] = $homeJaimeAlbum;
            $homeJaimeAlbum->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimeAlbum(HomeJaimeAlbum $homeJaimeAlbum): self
    {
        if ($this->homeJaimeAlbums->contains($homeJaimeAlbum)) {
            $this->homeJaimeAlbums->removeElement($homeJaimeAlbum);
            // set the owning side to null (unless already changed)
            if ($homeJaimeAlbum->getUtilisateur() === $this) {
                $homeJaimeAlbum->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeSingle[]
     */
    public function getHomeJaimeSingles(): Collection
    {
        return $this->homeJaimeSingles;
    }

    public function addHomeJaimeSingle(HomeJaimeSingle $homeJaimeSingle): self
    {
        if (!$this->homeJaimeSingles->contains($homeJaimeSingle)) {
            $this->homeJaimeSingles[] = $homeJaimeSingle;
            $homeJaimeSingle->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimeSingle(HomeJaimeSingle $homeJaimeSingle): self
    {
        if ($this->homeJaimeSingles->contains($homeJaimeSingle)) {
            $this->homeJaimeSingles->removeElement($homeJaimeSingle);
            // set the owning side to null (unless already changed)
            if ($homeJaimeSingle->getUtilisateur() === $this) {
                $homeJaimeSingle->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeVideo[]
     */
    public function getHomeJaimeVideos(): Collection
    {
        return $this->homeJaimeVideos;
    }

    public function addHomeJaimeVideo(HomeJaimeVideo $homeJaimeVideo): self
    {
        if (!$this->homeJaimeVideos->contains($homeJaimeVideo)) {
            $this->homeJaimeVideos[] = $homeJaimeVideo;
            $homeJaimeVideo->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimeVideo(HomeJaimeVideo $homeJaimeVideo): self
    {
        if ($this->homeJaimeVideos->contains($homeJaimeVideo)) {
            $this->homeJaimeVideos->removeElement($homeJaimeVideo);
            // set the owning side to null (unless already changed)
            if ($homeJaimeVideo->getUtilisateur() === $this) {
                $homeJaimeVideo->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasActualite[]
     */
    public function getHomeJaimePasActualites(): Collection
    {
        return $this->homeJaimePasActualites;
    }

    public function addHomeJaimePasActualite(HomeJaimePasActualite $homeJaimePasActualite): self
    {
        if (!$this->homeJaimePasActualites->contains($homeJaimePasActualite)) {
            $this->homeJaimePasActualites[] = $homeJaimePasActualite;
            $homeJaimePasActualite->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimePasActualite(HomeJaimePasActualite $homeJaimePasActualite): self
    {
        if ($this->homeJaimePasActualites->contains($homeJaimePasActualite)) {
            $this->homeJaimePasActualites->removeElement($homeJaimePasActualite);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasActualite->getUtilisateur() === $this) {
                $homeJaimePasActualite->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasArtiste[]
     */
    public function getHomeJaimePasArtistes(): Collection
    {
        return $this->homeJaimePasArtistes;
    }

    public function addHomeJaimePasArtiste(HomeJaimePasArtiste $homeJaimePasArtiste): self
    {
        if (!$this->homeJaimePasArtistes->contains($homeJaimePasArtiste)) {
            $this->homeJaimePasArtistes[] = $homeJaimePasArtiste;
            $homeJaimePasArtiste->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimePasArtiste(HomeJaimePasArtiste $homeJaimePasArtiste): self
    {
        if ($this->homeJaimePasArtistes->contains($homeJaimePasArtiste)) {
            $this->homeJaimePasArtistes->removeElement($homeJaimePasArtiste);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasArtiste->getUtilisateur() === $this) {
                $homeJaimePasArtiste->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasGenreMusicaux[]
     */
    public function getHomeJaimePasGenreMusicauxes(): Collection
    {
        return $this->homeJaimePasGenreMusicauxes;
    }

    public function addHomeJaimePasGenreMusicaux(HomeJaimePasGenreMusicaux $homeJaimePasGenreMusicaux): self
    {
        if (!$this->homeJaimePasGenreMusicauxes->contains($homeJaimePasGenreMusicaux)) {
            $this->homeJaimePasGenreMusicauxes[] = $homeJaimePasGenreMusicaux;
            $homeJaimePasGenreMusicaux->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimePasGenreMusicaux(HomeJaimePasGenreMusicaux $homeJaimePasGenreMusicaux): self
    {
        if ($this->homeJaimePasGenreMusicauxes->contains($homeJaimePasGenreMusicaux)) {
            $this->homeJaimePasGenreMusicauxes->removeElement($homeJaimePasGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasGenreMusicaux->getUtilisateur() === $this) {
                $homeJaimePasGenreMusicaux->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasAlbum[]
     */
    public function getHomeJaimePasAlbums(): Collection
    {
        return $this->homeJaimePasAlbums;
    }

    public function addHomeJaimePasAlbum(HomeJaimePasAlbum $homeJaimePasAlbum): self
    {
        if (!$this->homeJaimePasAlbums->contains($homeJaimePasAlbum)) {
            $this->homeJaimePasAlbums[] = $homeJaimePasAlbum;
            $homeJaimePasAlbum->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimePasAlbum(HomeJaimePasAlbum $homeJaimePasAlbum): self
    {
        if ($this->homeJaimePasAlbums->contains($homeJaimePasAlbum)) {
            $this->homeJaimePasAlbums->removeElement($homeJaimePasAlbum);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasAlbum->getUtilisateur() === $this) {
                $homeJaimePasAlbum->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasSingle[]
     */
    public function getHomeJaimePasSingles(): Collection
    {
        return $this->homeJaimePasSingles;
    }

    public function addHomeJaimePasSingle(HomeJaimePasSingle $homeJaimePasSingle): self
    {
        if (!$this->homeJaimePasSingles->contains($homeJaimePasSingle)) {
            $this->homeJaimePasSingles[] = $homeJaimePasSingle;
            $homeJaimePasSingle->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimePasSingle(HomeJaimePasSingle $homeJaimePasSingle): self
    {
        if ($this->homeJaimePasSingles->contains($homeJaimePasSingle)) {
            $this->homeJaimePasSingles->removeElement($homeJaimePasSingle);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasSingle->getUtilisateur() === $this) {
                $homeJaimePasSingle->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasVideo[]
     */
    public function getHomeJaimePasVideos(): Collection
    {
        return $this->homeJaimePasVideos;
    }

    public function addHomeJaimePasVideo(HomeJaimePasVideo $homeJaimePasVideo): self
    {
        if (!$this->homeJaimePasVideos->contains($homeJaimePasVideo)) {
            $this->homeJaimePasVideos[] = $homeJaimePasVideo;
            $homeJaimePasVideo->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeJaimePasVideo(HomeJaimePasVideo $homeJaimePasVideo): self
    {
        if ($this->homeJaimePasVideos->contains($homeJaimePasVideo)) {
            $this->homeJaimePasVideos->removeElement($homeJaimePasVideo);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasVideo->getUtilisateur() === $this) {
                $homeJaimePasVideo->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireActualite[]
     */
    public function getHomeCommentaireActualites(): Collection
    {
        return $this->homeCommentaireActualites;
    }

    public function addHomeCommentaireActualite(HomeCommentaireActualite $homeCommentaireActualite): self
    {
        if (!$this->homeCommentaireActualites->contains($homeCommentaireActualite)) {
            $this->homeCommentaireActualites[] = $homeCommentaireActualite;
            $homeCommentaireActualite->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeCommentaireActualite(HomeCommentaireActualite $homeCommentaireActualite): self
    {
        if ($this->homeCommentaireActualites->contains($homeCommentaireActualite)) {
            $this->homeCommentaireActualites->removeElement($homeCommentaireActualite);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireActualite->getUtilisateur() === $this) {
                $homeCommentaireActualite->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireGenreMusicaux[]
     */
    public function getHomeCommentaireGenreMusicauxes(): Collection
    {
        return $this->homeCommentaireGenreMusicauxes;
    }

    public function addHomeCommentaireGenreMusicaux(HomeCommentaireGenreMusicaux $homeCommentaireGenreMusicaux): self
    {
        if (!$this->homeCommentaireGenreMusicauxes->contains($homeCommentaireGenreMusicaux)) {
            $this->homeCommentaireGenreMusicauxes[] = $homeCommentaireGenreMusicaux;
            $homeCommentaireGenreMusicaux->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeCommentaireGenreMusicaux(HomeCommentaireGenreMusicaux $homeCommentaireGenreMusicaux): self
    {
        if ($this->homeCommentaireGenreMusicauxes->contains($homeCommentaireGenreMusicaux)) {
            $this->homeCommentaireGenreMusicauxes->removeElement($homeCommentaireGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireGenreMusicaux->getUtilisateur() === $this) {
                $homeCommentaireGenreMusicaux->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireSingle[]
     */
    public function getHomeCommentaireSingles(): Collection
    {
        return $this->homeCommentaireSingles;
    }

    public function addHomeCommentaireSingle(HomeCommentaireSingle $homeCommentaireSingle): self
    {
        if (!$this->homeCommentaireSingles->contains($homeCommentaireSingle)) {
            $this->homeCommentaireSingles[] = $homeCommentaireSingle;
            $homeCommentaireSingle->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeCommentaireSingle(HomeCommentaireSingle $homeCommentaireSingle): self
    {
        if ($this->homeCommentaireSingles->contains($homeCommentaireSingle)) {
            $this->homeCommentaireSingles->removeElement($homeCommentaireSingle);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireSingle->getUtilisateur() === $this) {
                $homeCommentaireSingle->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireVideo[]
     */
    public function getHomeCommentaireVideos(): Collection
    {
        return $this->homeCommentaireVideos;
    }

    public function addHomeCommentaireVideo(HomeCommentaireVideo $homeCommentaireVideo): self
    {
        if (!$this->homeCommentaireVideos->contains($homeCommentaireVideo)) {
            $this->homeCommentaireVideos[] = $homeCommentaireVideo;
            $homeCommentaireVideo->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeCommentaireVideo(HomeCommentaireVideo $homeCommentaireVideo): self
    {
        if ($this->homeCommentaireVideos->contains($homeCommentaireVideo)) {
            $this->homeCommentaireVideos->removeElement($homeCommentaireVideo);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireVideo->getUtilisateur() === $this) {
                $homeCommentaireVideo->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeReplaySingle[]
     */
    public function getHomeReplaySingles(): Collection
    {
        return $this->homeReplaySingles;
    }

    public function addHomeReplaySingle(HomeReplaySingle $homeReplaySingle): self
    {
        if (!$this->homeReplaySingles->contains($homeReplaySingle)) {
            $this->homeReplaySingles[] = $homeReplaySingle;
            $homeReplaySingle->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeReplaySingle(HomeReplaySingle $homeReplaySingle): self
    {
        if ($this->homeReplaySingles->contains($homeReplaySingle)) {
            $this->homeReplaySingles->removeElement($homeReplaySingle);
            // set the owning side to null (unless already changed)
            if ($homeReplaySingle->getUtilisateur() === $this) {
                $homeReplaySingle->setUtilisateur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeReplayVideo[]
     */
    public function getHomeReplayVideos(): Collection
    {
        return $this->homeReplayVideos;
    }

    public function addHomeReplayVideo(HomeReplayVideo $homeReplayVideo): self
    {
        if (!$this->homeReplayVideos->contains($homeReplayVideo)) {
            $this->homeReplayVideos[] = $homeReplayVideo;
            $homeReplayVideo->setUtilisateur($this);
        }

        return $this;
    }

    public function removeHomeReplayVideo(HomeReplayVideo $homeReplayVideo): self
    {
        if ($this->homeReplayVideos->contains($homeReplayVideo)) {
            $this->homeReplayVideos->removeElement($homeReplayVideo);
            // set the owning side to null (unless already changed)
            if ($homeReplayVideo->getUtilisateur() === $this) {
                $homeReplayVideo->setUtilisateur(null);
            }
        }

        return $this;
    }
}
