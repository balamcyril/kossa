<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsTagActualiteRepository")
 */
class NewsTagActualite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NewsActualite", inversedBy="newsTagActualites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $news_actualite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NewsTag", inversedBy="newsTagActualites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $news_tag;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNewsActualite(): ?NewsActualite
    {
        return $this->news_actualite;
    }

    public function setNewsActualite(?NewsActualite $news_actualite): self
    {
        $this->news_actualite = $news_actualite;

        return $this;
    }

    public function getNewsTag(): ?NewsTag
    {
        return $this->news_tag;
    }

    public function setNewsTag(?NewsTag $news_tag): self
    {
        $this->news_tag = $news_tag;

        return $this;
    }
}
