<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayAlbumRepository")
 * @Vich\Uploadable
 */
class PlayAlbum
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cover1;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 2M.",
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="album_cover1", fileNameProperty="cover1")
     */
    private $fichierCover1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover2;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 2M.",
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="album_cover2", fileNameProperty="cover2")
     */
    private $fichierCover2;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieAlbum", mappedBy="album", orphanRemoval=true)
     */
    private $homeFavorieAlbums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeAlbum", mappedBy="album", orphanRemoval=true)
     */
    private $homeJaimeAlbums;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasAlbum", mappedBy="album", orphanRemoval=true)
     */
    private $homeJaimePasAlbums;

    /**
     * @ORM\OneToMany(targetEntity=PlaySingle::class, mappedBy="album", orphanRemoval=true)
     */
    private $playSingles;
    
    public function __toString() {        
     return "".$this->titre;
}  

    public function __construct()
    {
       $this->dateEnreg = new \DateTime('now');
       $this->homeFavorieAlbums = new ArrayCollection();
       $this->homeJaimeAlbums = new ArrayCollection();
       $this->homeJaimePasAlbums = new ArrayCollection();
       $this->playSingles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCover1(): ?string
    {
        return $this->cover1;
    }

    public function setCover1(string $cover1): self
    {
        $this->cover1 = $cover1;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierCover1
     *
     * @return PlayAlbum
    */
    public function setFichierCover1(File $fichierCover1)
    {
        $this->fichierCover1 = $fichierCover1;
        if ($fichierCover1) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierCover1()
    {
        return $this->fichierCover1;
    }

    public function getCover2(): ?string
    {
        return $this->cover2;
    }

    public function setCover2(?string $cover2): self
    {
        $this->cover2 = $cover2;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierCover2
     *
     * @return PlayAlbum
    */
    public function setFichierCover2(File $fichierCover2)
    {
        $this->fichierCover2 = $fichierCover2;
        if ($fichierCover2) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierCover2()
    {
        return $this->fichierCover2;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    /**
     * @return Collection|HomeFavorieAlbum[]
     */
    public function getHomeFavorieAlbums(): Collection
    {
        return $this->homeFavorieAlbums;
    }

    public function addHomeFavorieAlbum(HomeFavorieAlbum $homeFavorieAlbum): self
    {
        if (!$this->homeFavorieAlbums->contains($homeFavorieAlbum)) {
            $this->homeFavorieAlbums[] = $homeFavorieAlbum;
            $homeFavorieAlbum->setAlbum($this);
        }

        return $this;
    }

    public function removeHomeFavorieAlbum(HomeFavorieAlbum $homeFavorieAlbum): self
    {
        if ($this->homeFavorieAlbums->contains($homeFavorieAlbum)) {
            $this->homeFavorieAlbums->removeElement($homeFavorieAlbum);
            // set the owning side to null (unless already changed)
            if ($homeFavorieAlbum->getAlbum() === $this) {
                $homeFavorieAlbum->setAlbum(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeAlbum[]
     */
    public function getHomeJaimeAlbums(): Collection
    {
        return $this->homeJaimeAlbums;
    }

    public function addHomeJaimeAlbum(HomeJaimeAlbum $homeJaimeAlbum): self
    {
        if (!$this->homeJaimeAlbums->contains($homeJaimeAlbum)) {
            $this->homeJaimeAlbums[] = $homeJaimeAlbum;
            $homeJaimeAlbum->setAlbum($this);
        }

        return $this;
    }

    public function removeHomeJaimeAlbum(HomeJaimeAlbum $homeJaimeAlbum): self
    {
        if ($this->homeJaimeAlbums->contains($homeJaimeAlbum)) {
            $this->homeJaimeAlbums->removeElement($homeJaimeAlbum);
            // set the owning side to null (unless already changed)
            if ($homeJaimeAlbum->getAlbum() === $this) {
                $homeJaimeAlbum->setAlbum(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasAlbum[]
     */
    public function getHomeJaimePasAlbums(): Collection
    {
        return $this->homeJaimePasAlbums;
    }

    public function addHomeJaimePasAlbum(HomeJaimePasAlbum $homeJaimePasAlbum): self
    {
        if (!$this->homeJaimePasAlbums->contains($homeJaimePasAlbum)) {
            $this->homeJaimePasAlbums[] = $homeJaimePasAlbum;
            $homeJaimePasAlbum->setAlbum($this);
        }

        return $this;
    }

    public function removeHomeJaimePasAlbum(HomeJaimePasAlbum $homeJaimePasAlbum): self
    {
        if ($this->homeJaimePasAlbums->contains($homeJaimePasAlbum)) {
            $this->homeJaimePasAlbums->removeElement($homeJaimePasAlbum);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasAlbum->getAlbum() === $this) {
                $homeJaimePasAlbum->setAlbum(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlaySingle[]
     */
    public function getPlaySingles(): Collection
    {
        return $this->playSingles;
    }

    public function addPlaySingle(PlaySingle $playSingle): self
    {
        if (!$this->playSingles->contains($playSingle)) {
            $this->playSingles[] = $playSingle;
            $playSingle->setAlbum($this);
        }

        return $this;
    }

    public function removePlaySingle(PlaySingle $playSingle): self
    {
        if ($this->playSingles->contains($playSingle)) {
            $this->playSingles->removeElement($playSingle);
            // set the owning side to null (unless already changed)
            if ($playSingle->getAlbum() === $this) {
                $playSingle->setAlbum(null);
            }
        }

        return $this;
    }
}
