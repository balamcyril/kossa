<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaySingleGenreMusicauxRepository")
 */
class PlaySingleGenreMusicaux
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playSingleGenreMusicauxes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $play_single;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayGenreMusicaux", inversedBy="playSingleGenreMusicauxes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $play_genre_musicaux;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaySingle(): ?PlaySingle
    {
        return $this->play_single;
    }

    public function setPlaySingle(?PlaySingle $play_single): self
    {
        $this->play_single = $play_single;

        return $this;
    }

    public function getPlayGenreMusicaux(): ?PlayGenreMusicaux
    {
        return $this->play_genre_musicaux;
    }

    public function setPlayGenreMusicaux(?PlayGenreMusicaux $play_genre_musicaux): self
    {
        $this->play_genre_musicaux = $play_genre_musicaux;

        return $this;
    }
}
