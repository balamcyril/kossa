<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeJaimePasAlbumRepository")
 */
class HomeJaimePasAlbum
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_temporaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="homeJaimePasAlbums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayAlbum", inversedBy="homeJaimePasAlbums")
     * @ORM\JoinColumn(nullable=false)
     */
    private $album;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTemporaire(): ?int
    {
        return $this->id_temporaire;
    }

    public function setIdTemporaire(int $id_temporaire): self
    {
        $this->id_temporaire = $id_temporaire;

        return $this;
    }

    public function getUtilisateur(): ?HomeUtilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?HomeUtilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getAlbum(): ?PlayAlbum
    {
        return $this->album;
    }

    public function setAlbum(?PlayAlbum $album): self
    {
        $this->album = $album;

        return $this;
    }



    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }
}
