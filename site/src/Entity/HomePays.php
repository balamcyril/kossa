<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomePaysRepository")
 */
class HomePays
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayArtiste", mappedBy="homePays", orphanRemoval=true)
     */
    private $playArtistes;

    public function __construct()
    {
        $this->playArtistes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function __toString(){

        return $this->nom;
    }

    /**
     * @return Collection|PlayArtiste[]
     */
    public function getPlayArtistes(): Collection
    {
        return $this->playArtistes;
    }

    public function addPlayArtiste(PlayArtiste $playArtiste): self
    {
        if (!$this->playArtistes->contains($playArtiste)) {
            $this->playArtistes[] = $playArtiste;
            $playArtiste->setHomePays($this);
        }

        return $this;
    }

    public function removePlayArtiste(PlayArtiste $playArtiste): self
    {
        if ($this->playArtistes->contains($playArtiste)) {
            $this->playArtistes->removeElement($playArtiste);
            // set the owning side to null (unless already changed)
            if ($playArtiste->getHomePays() === $this) {
                $playArtiste->setHomePays(null);
            }
        }

        return $this;
    }
}
