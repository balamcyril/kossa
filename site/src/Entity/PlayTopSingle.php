<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayTopSingleRepository")
 * @Vich\Uploadable
 */
class PlayTopSingle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single4;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single5;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single6;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single7;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single8;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single9;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single10;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="top_single_photo", fileNameProperty="photo")
     */
    private $fichierPhoto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="playTopSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $auteur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    public function __construct()
    {
       $this->dateEnreg = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSingle1(): ?PlaySingle
    {
        return $this->single1;
    }

    public function setSingle1(?PlaySingle $single1): self
    {
        $this->single1 = $single1;

        return $this;
    }

    public function getSingle2(): ?PlaySingle
    {
        return $this->single2;
    }

    public function setSingle2(?PlaySingle $single2): self
    {
        $this->single2 = $single2;

        return $this;
    }

    public function getSingle3(): ?PlaySingle
    {
        return $this->single3;
    }

    public function setSingle3(?PlaySingle $single3): self
    {
        $this->single3 = $single3;

        return $this;
    }

    public function getSingle4(): ?PlaySingle
    {
        return $this->single4;
    }

    public function setSingle4(?PlaySingle $single4): self
    {
        $this->single4 = $single4;

        return $this;
    }

    public function getSingle5(): ?PlaySingle
    {
        return $this->single5;
    }

    public function setSingle5(?PlaySingle $single5): self
    {
        $this->single5 = $single5;

        return $this;
    }

    public function getSingle6(): ?PlaySingle
    {
        return $this->single6;
    }

    public function setSingle6(?PlaySingle $single6): self
    {
        $this->single6 = $single6;

        return $this;
    }

    public function getSingle7(): ?PlaySingle
    {
        return $this->single7;
    }

    public function setSingle7(?PlaySingle $single7): self
    {
        $this->single7 = $single7;

        return $this;
    }

    public function getSingle8(): ?PlaySingle
    {
        return $this->single8;
    }

    public function setSingle8(?PlaySingle $single8): self
    {
        $this->single8 = $single8;

        return $this;
    }

    public function getSingle9(): ?PlaySingle
    {
        return $this->single9;
    }

    public function setSingle9(?PlaySingle $single9): self
    {
        $this->single9 = $single9;

        return $this;
    }

    public function getSingle10(): ?PlaySingle
    {
        return $this->single10;
    }

    public function setSingle10(?PlaySingle $single10): self
    {
        $this->single10 = $single10;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto
     *
     * @return PlayTopSingle
    */
    public function setFichierPhoto(File $fichierPhoto = null)
    {
        $this->fichierPhoto = $fichierPhoto;
        if ($fichierPhoto) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto()
    {
        return $this->fichierPhoto;
    }

    public function getAuteur(): ?HomeUtilisateur
    {
        return $this->auteur;
    }

    public function setAuteur(?HomeUtilisateur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }
}
