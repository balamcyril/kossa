<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeCommentaireActualiteRepository")
 */
class HomeCommentaireActualite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_temporaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contenu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="homeCommentaireActualites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NewsActualite", inversedBy="homeCommentaireActualites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $actualite;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTemporaire(): ?int
    {
        return $this->id_temporaire;
    }

    public function setIdTemporaire(int $id_temporaire): self
    {
        $this->id_temporaire = $id_temporaire;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getUtilisateur(): ?HomeUtilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?HomeUtilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getActualite(): ?NewsActualite
    {
        return $this->actualite;
    }

    public function setActualite(?NewsActualite $actualite): self
    {
        $this->actualite = $actualite;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }
}
