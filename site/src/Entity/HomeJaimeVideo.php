<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeJaimeVideoRepository")
 */
class HomeJaimeVideo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_temporaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="homeJaimeVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="homeJaimeVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTemporaire(): ?int
    {
        return $this->id_temporaire;
    }

    public function setIdTemporaire(int $id_temporaire): self
    {
        $this->id_temporaire = $id_temporaire;

        return $this;
    }

    public function getUtilisateur(): ?HomeUtilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?HomeUtilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getVideo(): ?PlayVideo
    {
        return $this->video;
    }

    public function setVideo(?PlayVideo $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }
}
