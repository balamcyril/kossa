<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeFavorieArtisteRepository")
 */
class HomeFavorieArtiste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $ide_temporaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="homeFavorieArtistes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayArtiste", inversedBy="homeFavorieArtistes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $artiste;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdeTemporaire(): ?int
    {
        return $this->ide_temporaire;
    }

    public function setIdeTemporaire(int $ide_temporaire): self
    {
        $this->ide_temporaire = $ide_temporaire;

        return $this;
    }

    public function getUtilisateur(): ?HomeUtilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?HomeUtilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getArtiste(): ?PlayArtiste
    {
        return $this->artiste;
    }

    public function setArtiste(?PlayArtiste $artiste): self
    {
        $this->artiste = $artiste;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
