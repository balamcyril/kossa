<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsTagRepository")
 */
class NewsTag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsTagActualite", mappedBy="news_tag", orphanRemoval=true)
     */
    private $newsTagActualites;

    public function __construct()
    {
        $this->newsTagActualites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * @return Collection|NewsTagActualite[]
     */
    public function getNewsTagActualites(): Collection
    {
        return $this->newsTagActualites;
    }

    public function addNewsTagActualite(NewsTagActualite $newsTagActualite): self
    {
        if (!$this->newsTagActualites->contains($newsTagActualite)) {
            $this->newsTagActualites[] = $newsTagActualite;
            $newsTagActualite->setNewsTag($this);
        }

        return $this;
    }

    public function removeNewsTagActualite(NewsTagActualite $newsTagActualite): self
    {
        if ($this->newsTagActualites->contains($newsTagActualite)) {
            $this->newsTagActualites->removeElement($newsTagActualite);
            // set the owning side to null (unless already changed)
            if ($newsTagActualite->getNewsTag() === $this) {
                $newsTagActualite->setNewsTag(null);
            }
        }

        return $this;
    }
}
