<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeFavorieGenreMusicauxRepository")
 */
class HomeFavorieGenreMusicaux
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="homeFavorieGenreMusicauxes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayGenreMusicaux", inversedBy="homeFavorieGenreMusicauxes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $genre_musicaux;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_temporaire;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUtilisateur(): ?HomeUtilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?HomeUtilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    public function getGenreMusicaux(): ?PlayGenreMusicaux
    {
        return $this->genre_musicaux;
    }

    public function setGenreMusicaux(?PlayGenreMusicaux $genre_musicaux): self
    {
        $this->genre_musicaux = $genre_musicaux;

        return $this;
    }

    public function getIdTemporaire(): ?int
    {
        return $this->id_temporaire;
    }

    public function setIdTemporaire(int $id_temporaire): self
    {
        $this->id_temporaire = $id_temporaire;

        return $this;
    }
}
