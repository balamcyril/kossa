<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaySingleRepository")
 * @Vich\Uploadable
 */
class PlaySingle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix = 0;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fichier;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 2Mb.",
     *     maxSize = "4M",
     *     mimeTypes = {"audio/mpeg3", "audio/x-mpeg-3", "audio/mp3", "audio/mpeg"},
     *     mimeTypesMessage = "Les fichiers doivent être de type mp3"
     * )
     * @Vich\UploadableField(mapping="single_audio", fileNameProperty="fichier")
     */
    private $fichierSingleAudio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $extrait;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 2Mb.",
     *     maxSize = "10M",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png", "audio/mpeg", "audio/mpeg"},
     *     mimeTypesMessage = "Les fichiers doivent être de type mp3"
     * )
     * @Vich\UploadableField(mapping="single_extrait", fileNameProperty="extrait")
     */
    private $extraitSingleAudio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 2Mb.",
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="single_photo", fileNameProperty="photo")
     */
    private $fichierPhoto;

    /**
     * @ORM\Column(type="integer")
     */
    private $vue = 0 ;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaySingleGenreMusicaux", mappedBy="play_single", orphanRemoval=true)
     */
    private $playSingleGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaySingleArtiste", mappedBy="play_single", orphanRemoval=true)
     */
    private $playSingleArtistes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayVideo", mappedBy="play_single", orphanRemoval=true)
     */
    private $playVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsActualite", mappedBy="Play_single", orphanRemoval=true)
     */
    private $newsActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayTopSingle", mappedBy="single1", orphanRemoval=true)
     */
    private $playTopSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeSingle", mappedBy="single", orphanRemoval=true)
     */
    private $homeJaimeSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasSingle", mappedBy="single", orphanRemoval=true)
     */
    private $homeJaimePasSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireSingle", mappedBy="single", orphanRemoval=true)
     */
    private $homeCommentaireSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeReplaySingle", mappedBy="single", orphanRemoval=true)
     */
    private $homeReplaySingles;

    /**
     * @ORM\ManyToOne(targetEntity=PlayAlbum::class, inversedBy="playSingles")
     * @ORM\JoinColumn(nullable=true)
     */
    private $album;

   
    public function __construct()
    {
       $this->dateEnreg = new \DateTime('now');
       $this->playSingleGenreMusicauxes = new ArrayCollection();
       $this->playSingleArtistes = new ArrayCollection();
       $this->playVideos = new ArrayCollection();
       $this->newsActualites = new ArrayCollection();
       $this->playTopSingles = new ArrayCollection();
       $this->homeJaimeSingles = new ArrayCollection();
       $this->homeJaimePasSingles = new ArrayCollection();
       $this->homeCommentaireSingles = new ArrayCollection();
       $this->homeReplaySingles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getFichier(): ?string
    {
        return $this->fichier;
    }

    public function setFichier(string $fichier): self
    {
        $this->fichier = $fichier;

        return $this;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichiersingleAudio
     *
     * @return PlaySingle
    */
    public function setFichierSingleAudio(File $fichierSingleAudio)
    {
        $this->fichierSingleAudio = $fichierSingleAudio;
        if ($fichierSingleAudio) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierSingleAudio()
    {
        return $this->fichierSingleAudio;
    }

    public function getExtrait(): ?string
    {
        return $this->extrait;
    }

    public function setExtrait(string $extrait): self
    {
        $this->extrait = $extrait;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $extraitSingleAudio
     *
     * @return PlaySingle
    */
    public function setExtraitSingleAudio(File $extraitSingleAudio)
    {
        $this->extraitSingleAudio = $extraitSingleAudio;
        if ($extraitSingleAudio) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getExtraitSingleAudio()
    {
        return $this->extraitSingleAudio;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto
     *
     * @return PlaySingle
    */
    public function setFichierPhoto(File $fichierPhoto)
    {
        $this->fichierPhoto = $fichierPhoto;
        if ($fichierPhoto) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto()
    {
        return $this->fichierPhoto;
    }


    public function getVue(): ?int
    {
        return $this->vue;
    }

    public function setVue(int $vue): self
    {
        $this->vue = $vue;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    /**
     * @return Collection|PlaySingleGenreMusicaux[]
     */
    public function getPlaySingleGenreMusicauxes(): Collection
    {
        return $this->playSingleGenreMusicauxes;
    }

    public function addPlaySingleGenreMusicaux(PlaySingleGenreMusicaux $playSingleGenreMusicaux): self
    {
        if (!$this->playSingleGenreMusicauxes->contains($playSingleGenreMusicaux)) {
            $this->playSingleGenreMusicauxes[] = $playSingleGenreMusicaux;
            $playSingleGenreMusicaux->setPlaySingle($this);
        }

        return $this;
    }

    public function removePlaySingleGenreMusicaux(PlaySingleGenreMusicaux $playSingleGenreMusicaux): self
    {
        if ($this->playSingleGenreMusicauxes->contains($playSingleGenreMusicaux)) {
            $this->playSingleGenreMusicauxes->removeElement($playSingleGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($playSingleGenreMusicaux->getPlaySingle() === $this) {
                $playSingleGenreMusicaux->setPlaySingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlaySingleArtiste[]
     */
    public function getPlaySingleArtistes(): Collection
    {
        return $this->playSingleArtistes;
    }

    public function addPlaySingleArtiste(PlaySingleArtiste $playSingleArtiste): self
    {
        if (!$this->playSingleArtistes->contains($playSingleArtiste)) {
            $this->playSingleArtistes[] = $playSingleArtiste;
            $playSingleArtiste->setPlaySingle($this);
        }

        return $this;
    }

    public function removePlaySingleArtiste(PlaySingleArtiste $playSingleArtiste): self
    {
        if ($this->playSingleArtistes->contains($playSingleArtiste)) {
            $this->playSingleArtistes->removeElement($playSingleArtiste);
            // set the owning side to null (unless already changed)
            if ($playSingleArtiste->getPlaySingle() === $this) {
                $playSingleArtiste->setPlaySingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlayVideo[]
     */
    public function getPlayVideos(): Collection
    {
        return $this->playVideos;
    }

    public function addPlayVideo(PlayVideo $playVideo): self
    {
        if (!$this->playVideos->contains($playVideo)) {
            $this->playVideos[] = $playVideo;
            $playVideo->setPlaySingle($this);
        }

        return $this;
    }

    public function removePlayVideo(PlayVideo $playVideo): self
    {
        if ($this->playVideos->contains($playVideo)) {
            $this->playVideos->removeElement($playVideo);
            // set the owning side to null (unless already changed)
            if ($playVideo->getPlaySingle() === $this) {
                $playVideo->setPlaySingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|NewsActualite[]
     */
    public function getNewsActualites(): Collection
    {
        return $this->newsActualites;
    }

    public function addNewsActualite(NewsActualite $newsActualite): self
    {
        if (!$this->newsActualites->contains($newsActualite)) {
            $this->newsActualites[] = $newsActualite;
            $newsActualite->setPlaySingle($this);
        }

        return $this;
    }

    public function removeNewsActualite(NewsActualite $newsActualite): self
    {
        if ($this->newsActualites->contains($newsActualite)) {
            $this->newsActualites->removeElement($newsActualite);
            // set the owning side to null (unless already changed)
            if ($newsActualite->getPlaySingle() === $this) {
                $newsActualite->setPlaySingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlayTopSingle[]
     */
    public function getPlayTopSingles(): Collection
    {
        return $this->playTopSingles;
    }

    public function addPlayTopSingle(PlayTopSingle $playTopSingle): self
    {
        if (!$this->playTopSingles->contains($playTopSingle)) {
            $this->playTopSingles[] = $playTopSingle;
            $playTopSingle->setSingle1($this);
        }

        return $this;
    }

    public function removePlayTopSingle(PlayTopSingle $playTopSingle): self
    {
        if ($this->playTopSingles->contains($playTopSingle)) {
            $this->playTopSingles->removeElement($playTopSingle);
            // set the owning side to null (unless already changed)
            if ($playTopSingle->getSingle1() === $this) {
                $playTopSingle->setSingle1(null);
            }
        }

        return $this;
    }

    public function __toString(){

        return $this->titre;
    }

    /**
     * @return Collection|HomeFavorieSingle[]
     */
    public function getHomeFavorieSingles(): Collection
    {
        return $this->homeFavorieSingles;
    }

    public function addHomeFavorieSingle(HomeFavorieSingle $homeFavorieSingle): self
    {
        if (!$this->homeFavorieSingles->contains($homeFavorieSingle)) {
            $this->homeFavorieSingles[] = $homeFavorieSingle;
            $homeFavorieSingle->setSingle($this);
        }

        return $this;
    }

    public function removeHomeFavorieSingle(HomeFavorieSingle $homeFavorieSingle): self
    {
        if ($this->homeFavorieSingles->contains($homeFavorieSingle)) {
            $this->homeFavorieSingles->removeElement($homeFavorieSingle);
            // set the owning side to null (unless already changed)
            if ($homeFavorieSingle->getSingle() === $this) {
                $homeFavorieSingle->setSingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeSingle[]
     */
    public function getHomeJaimeSingles(): Collection
    {
        return $this->homeJaimeSingles;
    }

    public function addHomeJaimeSingle(HomeJaimeSingle $homeJaimeSingle): self
    {
        if (!$this->homeJaimeSingles->contains($homeJaimeSingle)) {
            $this->homeJaimeSingles[] = $homeJaimeSingle;
            $homeJaimeSingle->setSingle($this);
        }

        return $this;
    }

    public function removeHomeJaimeSingle(HomeJaimeSingle $homeJaimeSingle): self
    {
        if ($this->homeJaimeSingles->contains($homeJaimeSingle)) {
            $this->homeJaimeSingles->removeElement($homeJaimeSingle);
            // set the owning side to null (unless already changed)
            if ($homeJaimeSingle->getSingle() === $this) {
                $homeJaimeSingle->setSingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasSingle[]
     */
    public function getHomeJaimePasSingles(): Collection
    {
        return $this->homeJaimePasSingles;
    }

    public function addHomeJaimePasSingle(HomeJaimePasSingle $homeJaimePasSingle): self
    {
        if (!$this->homeJaimePasSingles->contains($homeJaimePasSingle)) {
            $this->homeJaimePasSingles[] = $homeJaimePasSingle;
            $homeJaimePasSingle->setSingle($this);
        }

        return $this;
    }

    public function removeHomeJaimePasSingle(HomeJaimePasSingle $homeJaimePasSingle): self
    {
        if ($this->homeJaimePasSingles->contains($homeJaimePasSingle)) {
            $this->homeJaimePasSingles->removeElement($homeJaimePasSingle);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasSingle->getSingle() === $this) {
                $homeJaimePasSingle->setSingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireSingle[]
     */
    public function getHomeCommentaireSingles(): Collection
    {
        return $this->homeCommentaireSingles;
    }

    public function addHomeCommentaireSingle(HomeCommentaireSingle $homeCommentaireSingle): self
    {
        if (!$this->homeCommentaireSingles->contains($homeCommentaireSingle)) {
            $this->homeCommentaireSingles[] = $homeCommentaireSingle;
            $homeCommentaireSingle->setSingle($this);
        }

        return $this;
    }

    public function removeHomeCommentaireSingle(HomeCommentaireSingle $homeCommentaireSingle): self
    {
        if ($this->homeCommentaireSingles->contains($homeCommentaireSingle)) {
            $this->homeCommentaireSingles->removeElement($homeCommentaireSingle);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireSingle->getSingle() === $this) {
                $homeCommentaireSingle->setSingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeReplaySingle[]
     */
    public function getHomeReplaySingles(): Collection
    {
        return $this->homeReplaySingles;
    }

    public function addHomeReplaySingle(HomeReplaySingle $homeReplaySingle): self
    {
        if (!$this->homeReplaySingles->contains($homeReplaySingle)) {
            $this->homeReplaySingles[] = $homeReplaySingle;
            $homeReplaySingle->setSingle($this);
        }

        return $this;
    }

    public function removeHomeReplaySingle(HomeReplaySingle $homeReplaySingle): self
    {
        if ($this->homeReplaySingles->contains($homeReplaySingle)) {
            $this->homeReplaySingles->removeElement($homeReplaySingle);
            // set the owning side to null (unless already changed)
            if ($homeReplaySingle->getSingle() === $this) {
                $homeReplaySingle->setSingle(null);
            }
        }

        return $this;
    }

    public function getAlbum(): ?PlayAlbum
    {
        return $this->album;
    }

    public function setAlbum(?PlayAlbum $album): self
    {
        $this->album = $album;

        return $this;
    }
}
