<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaySingleArtisteRepository")
 */
class PlaySingleArtiste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playSingleArtistes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $play_single;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayArtiste", inversedBy="playSingleArtistes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $play_artiste;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeSingle", mappedBy="single", orphanRemoval=true)
     */
    private $homeJaimeSingles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasSingle", mappedBy="single", orphanRemoval=true)
     */
    private $homeJaimePasSingles;

    public function __construct()
    {
        $this->homeJaimeSingles = new ArrayCollection();
        $this->homeJaimePasSingles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaySingle(): ?PlaySingle
    {
        return $this->play_single;
    }

    public function setPlaySingle(?PlaySingle $play_single): self
    {
        $this->play_single = $play_single;

        return $this;
    }

    public function getPlayArtiste(): ?PlayArtiste
    {
        return $this->play_artiste;
    }

    public function setPlayArtiste(?PlayArtiste $play_artiste): self
    {
        $this->play_artiste = $play_artiste;

        return $this;
    }

    /**
     * @return Collection|HomeJaimeSingle[]
     */
    public function getHomeJaimeSingles(): Collection
    {
        return $this->homeJaimeSingles;
    }

    public function addHomeJaimeSingle(HomeJaimeSingle $homeJaimeSingle): self
    {
        if (!$this->homeJaimeSingles->contains($homeJaimeSingle)) {
            $this->homeJaimeSingles[] = $homeJaimeSingle;
            $homeJaimeSingle->setSingle($this);
        }

        return $this;
    }

    public function removeHomeJaimeSingle(HomeJaimeSingle $homeJaimeSingle): self
    {
        if ($this->homeJaimeSingles->contains($homeJaimeSingle)) {
            $this->homeJaimeSingles->removeElement($homeJaimeSingle);
            // set the owning side to null (unless already changed)
            if ($homeJaimeSingle->getSingle() === $this) {
                $homeJaimeSingle->setSingle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasSingle[]
     */
    public function getHomeJaimePasSingles(): Collection
    {
        return $this->homeJaimePasSingles;
    }

    public function addHomeJaimePasSingle(HomeJaimePasSingle $homeJaimePasSingle): self
    {
        if (!$this->homeJaimePasSingles->contains($homeJaimePasSingle)) {
            $this->homeJaimePasSingles[] = $homeJaimePasSingle;
            $homeJaimePasSingle->setSingle($this);
        }

        return $this;
    }

    public function removeHomeJaimePasSingle(HomeJaimePasSingle $homeJaimePasSingle): self
    {
        if ($this->homeJaimePasSingles->contains($homeJaimePasSingle)) {
            $this->homeJaimePasSingles->removeElement($homeJaimePasSingle);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasSingle->getSingle() === $this) {
                $homeJaimePasSingle->setSingle(null);
            }
        }

        return $this;
    }
}
