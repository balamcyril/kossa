<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsCategorieActualiteRepository")
 */
class NewsCategorieActualite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $couleur;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsActualite", mappedBy="newCategorieActualite", orphanRemoval=true)
     */
    private $newsActualites;

    public function __construct()
    {
        $this->newsActualites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(?string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function __toString(){

        return $this->nom;
    }

    /**
     * @return Collection|NewsActualite[]
     */
    public function getNewsActualites(): Collection
    {
        return $this->newsActualites;
    }

    public function addNewsActualite(NewsActualite $newsActualite): self
    {
        if (!$this->newsActualites->contains($newsActualite)) {
            $this->newsActualites[] = $newsActualite;
            $newsActualite->setNewCategorieActualite($this);
        }

        return $this;
    }

    public function removeNewsActualite(NewsActualite $newsActualite): self
    {
        if ($this->newsActualites->contains($newsActualite)) {
            $this->newsActualites->removeElement($newsActualite);
            // set the owning side to null (unless already changed)
            if ($newsActualite->getNewCategorieActualite() === $this) {
                $newsActualite->setNewCategorieActualite(null);
            }
        }

        return $this;
    }
}
