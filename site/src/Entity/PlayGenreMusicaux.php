<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayGenreMusicauxRepository")
 * @Vich\Uploadable
 */
class PlayGenreMusicaux
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contenu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="genre_musicaux_photo", fileNameProperty="photo")
     */
    private $fichierPhoto;

    /**
     * @ORM\Column(type="integer")
     */
    private $vue = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaySingleGenreMusicaux", mappedBy="play_genre_musicaux", orphanRemoval=true)
     */
    private $playSingleGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieGenreMusicaux", mappedBy="genre_musicaux", orphanRemoval=true)
     */
    private $homeFavorieGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeGenreMusicaux", mappedBy="genre_musicaux", orphanRemoval=true)
     */
    private $homeJaimeGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasGenreMusicaux", mappedBy="genre_musicaux", orphanRemoval=true)
     */
    private $homeJaimePasGenreMusicauxes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireGenreMusicaux", mappedBy="genre_musicaux", orphanRemoval=true)
     */
    private $homeCommentaireGenreMusicauxes;

    public function __construct()
    {
       $this->dateEnreg = new \DateTime('now');
       $this->playSingleGenreMusicauxes = new ArrayCollection();
       $this->homeFavorieGenreMusicauxes = new ArrayCollection();
       $this->homeJaimeGenreMusicauxes = new ArrayCollection();
       $this->homeJaimePasGenreMusicauxes = new ArrayCollection();
       $this->homeCommentaireGenreMusicauxes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(?string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto
     *
     * @return PlayGenreMusicaux
    */
    public function setFichierPhoto(File $fichierPhoto)
    {
        $this->fichierPhoto = $fichierPhoto;
        if ($fichierPhoto) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto()
    {
        return $this->fichierPhoto;
    }

    public function getVue(): ?int
    {
        return $this->vue;
    }

    public function setVue(int $vue): self
    {
        $this->vue = $vue;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    /**
     * @return Collection|PlaySingleGenreMusicaux[]
     */
    public function getPlaySingleGenreMusicauxes(): Collection
    {
        return $this->playSingleGenreMusicauxes;
    }

    public function addPlaySingleGenreMusicaux(PlaySingleGenreMusicaux $playSingleGenreMusicaux): self
    {
        if (!$this->playSingleGenreMusicauxes->contains($playSingleGenreMusicaux)) {
            $this->playSingleGenreMusicauxes[] = $playSingleGenreMusicaux;
            $playSingleGenreMusicaux->setPlayGenreMusicaux($this);
        }

        return $this;
    }

    public function removePlaySingleGenreMusicaux(PlaySingleGenreMusicaux $playSingleGenreMusicaux): self
    {
        if ($this->playSingleGenreMusicauxes->contains($playSingleGenreMusicaux)) {
            $this->playSingleGenreMusicauxes->removeElement($playSingleGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($playSingleGenreMusicaux->getPlayGenreMusicaux() === $this) {
                $playSingleGenreMusicaux->setPlayGenreMusicaux(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeFavorieGenreMusicaux[]
     */
    public function getHomeFavorieGenreMusicauxes(): Collection
    {
        return $this->homeFavorieGenreMusicauxes;
    }

    public function addHomeFavorieGenreMusicaux(HomeFavorieGenreMusicaux $homeFavorieGenreMusicaux): self
    {
        if (!$this->homeFavorieGenreMusicauxes->contains($homeFavorieGenreMusicaux)) {
            $this->homeFavorieGenreMusicauxes[] = $homeFavorieGenreMusicaux;
            $homeFavorieGenreMusicaux->setGenreMusicaux($this);
        }

        return $this;
    }

    public function removeHomeFavorieGenreMusicaux(HomeFavorieGenreMusicaux $homeFavorieGenreMusicaux): self
    {
        if ($this->homeFavorieGenreMusicauxes->contains($homeFavorieGenreMusicaux)) {
            $this->homeFavorieGenreMusicauxes->removeElement($homeFavorieGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeFavorieGenreMusicaux->getGenreMusicaux() === $this) {
                $homeFavorieGenreMusicaux->setGenreMusicaux(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeGenreMusicaux[]
     */
    public function getHomeJaimeGenreMusicauxes(): Collection
    {
        return $this->homeJaimeGenreMusicauxes;
    }

    public function addHomeJaimeGenreMusicaux(HomeJaimeGenreMusicaux $homeJaimeGenreMusicaux): self
    {
        if (!$this->homeJaimeGenreMusicauxes->contains($homeJaimeGenreMusicaux)) {
            $this->homeJaimeGenreMusicauxes[] = $homeJaimeGenreMusicaux;
            $homeJaimeGenreMusicaux->setGenreMusicaux($this);
        }

        return $this;
    }

    public function removeHomeJaimeGenreMusicaux(HomeJaimeGenreMusicaux $homeJaimeGenreMusicaux): self
    {
        if ($this->homeJaimeGenreMusicauxes->contains($homeJaimeGenreMusicaux)) {
            $this->homeJaimeGenreMusicauxes->removeElement($homeJaimeGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeJaimeGenreMusicaux->getGenreMusicaux() === $this) {
                $homeJaimeGenreMusicaux->setGenreMusicaux(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasGenreMusicaux[]
     */
    public function getHomeJaimePasGenreMusicauxes(): Collection
    {
        return $this->homeJaimePasGenreMusicauxes;
    }

    public function addHomeJaimePasGenreMusicaux(HomeJaimePasGenreMusicaux $homeJaimePasGenreMusicaux): self
    {
        if (!$this->homeJaimePasGenreMusicauxes->contains($homeJaimePasGenreMusicaux)) {
            $this->homeJaimePasGenreMusicauxes[] = $homeJaimePasGenreMusicaux;
            $homeJaimePasGenreMusicaux->setGenreMusicaux($this);
        }

        return $this;
    }

    public function removeHomeJaimePasGenreMusicaux(HomeJaimePasGenreMusicaux $homeJaimePasGenreMusicaux): self
    {
        if ($this->homeJaimePasGenreMusicauxes->contains($homeJaimePasGenreMusicaux)) {
            $this->homeJaimePasGenreMusicauxes->removeElement($homeJaimePasGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasGenreMusicaux->getGenreMusicaux() === $this) {
                $homeJaimePasGenreMusicaux->setGenreMusicaux(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireGenreMusicaux[]
     */
    public function getHomeCommentaireGenreMusicauxes(): Collection
    {
        return $this->homeCommentaireGenreMusicauxes;
    }

    public function addHomeCommentaireGenreMusicaux(HomeCommentaireGenreMusicaux $homeCommentaireGenreMusicaux): self
    {
        if (!$this->homeCommentaireGenreMusicauxes->contains($homeCommentaireGenreMusicaux)) {
            $this->homeCommentaireGenreMusicauxes[] = $homeCommentaireGenreMusicaux;
            $homeCommentaireGenreMusicaux->setGenreMusicaux($this);
        }

        return $this;
    }

    public function removeHomeCommentaireGenreMusicaux(HomeCommentaireGenreMusicaux $homeCommentaireGenreMusicaux): self
    {
        if ($this->homeCommentaireGenreMusicauxes->contains($homeCommentaireGenreMusicaux)) {
            $this->homeCommentaireGenreMusicauxes->removeElement($homeCommentaireGenreMusicaux);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireGenreMusicaux->getGenreMusicaux() === $this) {
                $homeCommentaireGenreMusicaux->setGenreMusicaux(null);
            }
        }

        return $this;
    }
}
