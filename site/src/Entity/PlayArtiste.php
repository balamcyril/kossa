<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayArtisteRepository")
 * @Vich\Uploadable
 */
class PlayArtiste
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo1;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 2M.",
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="artiste_photo1", fileNameProperty="photo1")
     */
    private $fichierPhoto1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo2;

     /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 2M.",
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="artiste_photo2", fileNameProperty="photo2")
     */
    private $fichierPhoto2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomePays", inversedBy="playArtistes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $homePays;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlaySingleArtiste", mappedBy="play_artiste", orphanRemoval=true)
     */
    private $playSingleArtistes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayVideo", mappedBy="play_artiste", orphanRemoval=true)
     */
    private $playVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieArtiste", mappedBy="artiste", orphanRemoval=true)
     */
    private $homeFavorieArtistes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeArtiste", mappedBy="artiste", orphanRemoval=true)
     */
    private $homeJaimeArtistes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasArtiste", mappedBy="artiste", orphanRemoval=true)
     */
    private $homeJaimePasArtistes;

    public function __construct()
    {
       $this->dateEnreg = new \DateTime('now');
       $this->playSingleArtistes = new ArrayCollection();
       $this->playVideos = new ArrayCollection();
       $this->homeFavorieArtistes = new ArrayCollection();
       $this->homeJaimeArtistes = new ArrayCollection();
       $this->homeJaimePasArtistes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPhoto1(): ?string
    {
        return $this->photo1;
    }

    public function setPhoto1(string $photo1): self
    {
        $this->photo1 = $photo1;

        return $this;
    }

    public function getPhoto2(): ?string
    {
        return $this->photo2;
    }

/**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto1
     *
     * @return PlayArtiste
    */
    public function setFichierPhoto1(File $fichierPhoto1)
    {
        $this->fichierPhoto1 = $fichierPhoto1;
        if ($fichierPhoto1) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto1()
    {
        return $this->fichierPhoto1;
    }

    public function setPhoto2(?string $photo2): self
    {
        $this->photo2 = $photo2;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto2
     *
     * @return PlayArtiste
    */
    public function setFichierPhoto2(File $fichierPhoto2)
    {
        $this->fichierPhoto2 = $fichierPhoto2;
        if ($fichierPhoto2) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto2()
    {
        return $this->fichierPhoto2;
    }

    public function getHomePays(): ?HomePays
    {
        return $this->homePays;
    }

    public function setHomePays(?HomePays $homePays): self
    {
        $this->homePays = $homePays;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    /**
     * @return Collection|PlaySingleArtiste[]
     */
    public function getPlaySingleArtistes(): Collection
    {
        return $this->playSingleArtistes;
    }

    public function addPlaySingleArtiste(PlaySingleArtiste $playSingleArtiste): self
    {
        if (!$this->playSingleArtistes->contains($playSingleArtiste)) {
            $this->playSingleArtistes[] = $playSingleArtiste;
            $playSingleArtiste->setPlayArtiste($this);
        }

        return $this;
    }

    public function removePlaySingleArtiste(PlaySingleArtiste $playSingleArtiste): self
    {
        if ($this->playSingleArtistes->contains($playSingleArtiste)) {
            $this->playSingleArtistes->removeElement($playSingleArtiste);
            // set the owning side to null (unless already changed)
            if ($playSingleArtiste->getPlayArtiste() === $this) {
                $playSingleArtiste->setPlayArtiste(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlayVideo[]
     */
    public function getPlayVideos(): Collection
    {
        return $this->playVideos;
    }

    public function addPlayVideo(PlayVideo $playVideo): self
    {
        if (!$this->playVideos->contains($playVideo)) {
            $this->playVideos[] = $playVideo;
            $playVideo->setPlayArtiste($this);
        }

        return $this;
    }

    public function removePlayVideo(PlayVideo $playVideo): self
    {
        if ($this->playVideos->contains($playVideo)) {
            $this->playVideos->removeElement($playVideo);
            // set the owning side to null (unless already changed)
            if ($playVideo->getPlayArtiste() === $this) {
                $playVideo->setPlayArtiste(null);
            }
        }

        return $this;
    }

    public function __toString(){

        return $this->pseudo;
    }

    /**
     * @return Collection|HomeFavorieArtiste[]
     */
    public function getHomeFavorieArtistes(): Collection
    {
        return $this->homeFavorieArtistes;
    }

    public function addHomeFavorieArtiste(HomeFavorieArtiste $homeFavorieArtiste): self
    {
        if (!$this->homeFavorieArtistes->contains($homeFavorieArtiste)) {
            $this->homeFavorieArtistes[] = $homeFavorieArtiste;
            $homeFavorieArtiste->setArtiste($this);
        }

        return $this;
    }

    public function removeHomeFavorieArtiste(HomeFavorieArtiste $homeFavorieArtiste): self
    {
        if ($this->homeFavorieArtistes->contains($homeFavorieArtiste)) {
            $this->homeFavorieArtistes->removeElement($homeFavorieArtiste);
            // set the owning side to null (unless already changed)
            if ($homeFavorieArtiste->getArtiste() === $this) {
                $homeFavorieArtiste->setArtiste(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeArtiste[]
     */
    public function getHomeJaimeArtistes(): Collection
    {
        return $this->homeJaimeArtistes;
    }

    public function addHomeJaimeArtiste(HomeJaimeArtiste $homeJaimeArtiste): self
    {
        if (!$this->homeJaimeArtistes->contains($homeJaimeArtiste)) {
            $this->homeJaimeArtistes[] = $homeJaimeArtiste;
            $homeJaimeArtiste->setArtiste($this);
        }

        return $this;
    }

    public function removeHomeJaimeArtiste(HomeJaimeArtiste $homeJaimeArtiste): self
    {
        if ($this->homeJaimeArtistes->contains($homeJaimeArtiste)) {
            $this->homeJaimeArtistes->removeElement($homeJaimeArtiste);
            // set the owning side to null (unless already changed)
            if ($homeJaimeArtiste->getArtiste() === $this) {
                $homeJaimeArtiste->setArtiste(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasArtiste[]
     */
    public function getHomeJaimePasArtistes(): Collection
    {
        return $this->homeJaimePasArtistes;
    }

    public function addHomeJaimePasArtiste(HomeJaimePasArtiste $homeJaimePasArtiste): self
    {
        if (!$this->homeJaimePasArtistes->contains($homeJaimePasArtiste)) {
            $this->homeJaimePasArtistes[] = $homeJaimePasArtiste;
            $homeJaimePasArtiste->setArtiste($this);
        }

        return $this;
    }

    public function removeHomeJaimePasArtiste(HomeJaimePasArtiste $homeJaimePasArtiste): self
    {
        if ($this->homeJaimePasArtistes->contains($homeJaimePasArtiste)) {
            $this->homeJaimePasArtistes->removeElement($homeJaimePasArtiste);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasArtiste->getArtiste() === $this) {
                $homeJaimePasArtiste->setArtiste(null);
            }
        }

        return $this;
    }
}
