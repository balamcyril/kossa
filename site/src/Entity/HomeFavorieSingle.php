<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeFavorieSingleRepository")
 */
class HomeFavorieSingle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_temporaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="homeFavorieSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="homeFavorieSingles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $single;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdTemporaire(): ?int
    {
        return $this->id_temporaire;
    }

    public function setIdTemporaire(int $id_temporaire): self
    {
        $this->id_temporaire = $id_temporaire;

        return $this;
    }

    public function getUtilisateur(): ?HomeUtilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?HomeUtilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getSingle(): ?PlaySingle
    {
        return $this->single;
    }

    public function setSingle(?PlaySingle $single): self
    {
        $this->single = $single;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }
}
