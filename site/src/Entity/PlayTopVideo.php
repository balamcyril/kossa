<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayTopVideoRepository")
 * @Vich\Uploadable
 */
class PlayTopVideo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video4;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video5;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video6;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video7;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video8;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video9;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayVideo", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $video10;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo;

    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="video_top_photo", fileNameProperty="photo")
     */
    private $fichierPhoto;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HomeUtilisateur", inversedBy="playTopVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $auteur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnreg;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getVideo1(): ?PlayVideo
    {
        return $this->video1;
    }

    public function setVideo1(?PlayVideo $video1): self
    {
        $this->video1 = $video1;

        return $this;
    }

    public function getVideo2(): ?PlayVideo
    {
        return $this->video2;
    }

    public function setVideo2(?PlayVideo $video2): self
    {
        $this->video2 = $video2;

        return $this;
    }

    public function getVideo3(): ?PlayVideo
    {
        return $this->video3;
    }

    public function setVideo3(?PlayVideo $video3): self
    {
        $this->video3 = $video3;

        return $this;
    }

    public function getVideo4(): ?PlayVideo
    {
        return $this->video4;
    }

    public function setVideo4(?PlayVideo $video4): self
    {
        $this->video4 = $video4;

        return $this;
    }

    public function getVideo5(): ?PlayVideo
    {
        return $this->video5;
    }

    public function setVideo5(?PlayVideo $video5): self
    {
        $this->video5 = $video5;

        return $this;
    }

    public function getVideo6(): ?PlayVideo
    {
        return $this->video6;
    }

    public function setVideo6(?PlayVideo $video6): self
    {
        $this->video6 = $video6;

        return $this;
    }

    public function getVideo7(): ?PlayVideo
    {
        return $this->video7;
    }

    public function setVideo7(?PlayVideo $video7): self
    {
        $this->video7 = $video7;

        return $this;
    }

    public function getVideo8(): ?PlayVideo
    {
        return $this->video8;
    }

    public function setVideo8(?PlayVideo $video8): self
    {
        $this->video8 = $video8;

        return $this;
    }

    public function getVideo9(): ?PlayVideo
    {
        return $this->video9;
    }

    public function setVideo9(?PlayVideo $video9): self
    {
        $this->video9 = $video9;

        return $this;
    }

    public function getVideo10(): ?PlayVideo
    {
        return $this->video10;
    }

    public function setVideo10(?PlayVideo $video10): self
    {
        $this->video10 = $video10;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

     /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto
     *
     * @return PlayTopVideo
    */
    public function setFichierPhoto(File $fichierPhoto = null)
    {
        $this->fichierPhoto = $fichierPhoto;
        if ($fichierPhoto) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto()
    {
        return $this->fichierPhoto;
    }

    public function getAuteur(): ?HomeUtilisateur
    {
        return $this->auteur;
    }

    public function setAuteur(?HomeUtilisateur $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

}
