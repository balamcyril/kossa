<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayVideoRepository")
 * @Vich\Uploadable
 */
class PlayVideo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;  

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lien;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlayArtiste", inversedBy="playVideos")
     * @ORM\JoinColumn(nullable=true)
     */
    private $play_artiste;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlaySingle", inversedBy="playVideos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $play_single;

    /**
     * @ORM\Column(type="date")
     */
    private $dateEnreg;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NewsActualite", mappedBy="play_video", orphanRemoval=true)
     */
    private $newsActualites;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PlayTopVideo", mappedBy="video2", orphanRemoval=true)
     */
    private $playTopVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeFavorieVideo", mappedBy="video", orphanRemoval=true)
     */
    private $homeFavorieVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimeVideo", mappedBy="video", orphanRemoval=true)
     */
    private $homeJaimeVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeJaimePasVideo", mappedBy="video", orphanRemoval=true)
     */
    private $homeJaimePasVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeCommentaireVideo", mappedBy="video", orphanRemoval=true)
     */
    private $homeCommentaireVideos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HomeReplayVideo", mappedBy="video", orphanRemoval=true)
     */
    private $homeReplayVideos;

  
    public function __construct()
    {
       $this->dateEnreg = new \DateTime('now');
       $this->newsActualites = new ArrayCollection();
       $this->playTopVideos = new ArrayCollection();
       $this->homeFavorieVideos = new ArrayCollection();
       $this->homeJaimeVideos = new ArrayCollection();
       $this->homeJaimePasVideos = new ArrayCollection();
       $this->homeCommentaireVideos = new ArrayCollection();
       $this->homeReplayVideos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

      /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierPhoto
     *
     * @return PlayVideo
    */
    public function setFichierPhoto(File $fichierPhoto = null)
    {
        $this->fichierPhoto = $fichierPhoto;
        if ($fichierPhoto) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierPhoto()
    {
        return $this->fichierPhoto;
    }

    public function getFichier(): ?string
    {
        return $this->fichier;
    }

    public function setFichier(string $fichier): self
    {
        $this->fichier = $fichier;

        return $this;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $fichierVideo
     *
     * @return PlayVideo
    */
    public function setFichierVideo(File $fichierVideo = null)
    {
        $this->fichierVideo = $fichierVideo;
        if ($fichierVideo) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->dateEnreg = new \DateTime('now');
        }
    }

    public function getFichierVideo()
    {
        return $this->fichierVideo;
    }

    public function getLien(): ?string
    {
        return $this->lien;
    }

    public function setLien(string $lien): self
    {
        $this->lien = $lien;

        return $this;
    }

    public function getPlayArtiste(): ?PlayArtiste
    {
        return $this->play_artiste;
    }

    public function setPlayArtiste(?PlayArtiste $play_artiste): self
    {
        $this->play_artiste = $play_artiste;

        return $this;
    }

    public function getPlaySingle(): ?PlaySingle
    {
        return $this->play_single;
    }

    public function setPlaySingle(?PlaySingle $play_single): self
    {
        $this->play_single = $play_single;

        return $this;
    }

    public function getDateEnreg(): ?\DateTimeInterface
    {
        return $this->dateEnreg;
    }

    public function setDateEnreg(\DateTimeInterface $dateEnreg): self
    {
        $this->dateEnreg = $dateEnreg;

        return $this;
    }

    /**
     * @return Collection|NewsActualite[]
     */
    public function getNewsActualites(): Collection
    {
        return $this->newsActualites;
    }

    public function addNewsActualite(NewsActualite $newsActualite): self
    {
        if (!$this->newsActualites->contains($newsActualite)) {
            $this->newsActualites[] = $newsActualite;
            $newsActualite->setPlayVideo($this);
        }

        return $this;
    }

    public function removeNewsActualite(NewsActualite $newsActualite): self
    {
        if ($this->newsActualites->contains($newsActualite)) {
            $this->newsActualites->removeElement($newsActualite);
            // set the owning side to null (unless already changed)
            if ($newsActualite->getPlayVideo() === $this) {
                $newsActualite->setPlayVideo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PlayTopVideo[]
     */
    public function getPlayTopVideos(): Collection
    {
        return $this->playTopVideos;
    }

    public function addPlayTopVideo(PlayTopVideo $playTopVideo): self
    {
        if (!$this->playTopVideos->contains($playTopVideo)) {
            $this->playTopVideos[] = $playTopVideo;
            $playTopVideo->setVideo1($this);
        }

        return $this;
    }

    public function removePlayTopVideo(PlayTopVideo $playTopVideo): self
    {
        if ($this->playTopVideos->contains($playTopVideo)) {
            $this->playTopVideos->removeElement($playTopVideo);
            // set the owning side to null (unless already changed)
            if ($playTopVideo->getVideo1() === $this) {
                $playTopVideo->setVideo1(null);
            }
        }

        return $this;
    }

   
    public function __toString(){

        return $this->titre;
    }

    /**
     * @return Collection|HomeFavorieVideo[]
     */
    public function getHomeFavorieVideos(): Collection
    {
        return $this->homeFavorieVideos;
    }

    public function addHomeFavorieVideo(HomeFavorieVideo $homeFavorieVideo): self
    {
        if (!$this->homeFavorieVideos->contains($homeFavorieVideo)) {
            $this->homeFavorieVideos[] = $homeFavorieVideo;
            $homeFavorieVideo->setVideo($this);
        }

        return $this;
    }

    public function removeHomeFavorieVideo(HomeFavorieVideo $homeFavorieVideo): self
    {
        if ($this->homeFavorieVideos->contains($homeFavorieVideo)) {
            $this->homeFavorieVideos->removeElement($homeFavorieVideo);
            // set the owning side to null (unless already changed)
            if ($homeFavorieVideo->getVideo() === $this) {
                $homeFavorieVideo->setVideo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimeVideo[]
     */
    public function getHomeJaimeVideos(): Collection
    {
        return $this->homeJaimeVideos;
    }

    public function addHomeJaimeVideo(HomeJaimeVideo $homeJaimeVideo): self
    {
        if (!$this->homeJaimeVideos->contains($homeJaimeVideo)) {
            $this->homeJaimeVideos[] = $homeJaimeVideo;
            $homeJaimeVideo->setVideo($this);
        }

        return $this;
    }

    public function removeHomeJaimeVideo(HomeJaimeVideo $homeJaimeVideo): self
    {
        if ($this->homeJaimeVideos->contains($homeJaimeVideo)) {
            $this->homeJaimeVideos->removeElement($homeJaimeVideo);
            // set the owning side to null (unless already changed)
            if ($homeJaimeVideo->getVideo() === $this) {
                $homeJaimeVideo->setVideo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeJaimePasVideo[]
     */
    public function getHomeJaimePasVideos(): Collection
    {
        return $this->homeJaimePasVideos;
    }

    public function addHomeJaimePasVideo(HomeJaimePasVideo $homeJaimePasVideo): self
    {
        if (!$this->homeJaimePasVideos->contains($homeJaimePasVideo)) {
            $this->homeJaimePasVideos[] = $homeJaimePasVideo;
            $homeJaimePasVideo->setVideo($this);
        }

        return $this;
    }

    public function removeHomeJaimePasVideo(HomeJaimePasVideo $homeJaimePasVideo): self
    {
        if ($this->homeJaimePasVideos->contains($homeJaimePasVideo)) {
            $this->homeJaimePasVideos->removeElement($homeJaimePasVideo);
            // set the owning side to null (unless already changed)
            if ($homeJaimePasVideo->getVideo() === $this) {
                $homeJaimePasVideo->setVideo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeCommentaireVideo[]
     */
    public function getHomeCommentaireVideos(): Collection
    {
        return $this->homeCommentaireVideos;
    }

    public function addHomeCommentaireVideo(HomeCommentaireVideo $homeCommentaireVideo): self
    {
        if (!$this->homeCommentaireVideos->contains($homeCommentaireVideo)) {
            $this->homeCommentaireVideos[] = $homeCommentaireVideo;
            $homeCommentaireVideo->setVideo($this);
        }

        return $this;
    }

    public function removeHomeCommentaireVideo(HomeCommentaireVideo $homeCommentaireVideo): self
    {
        if ($this->homeCommentaireVideos->contains($homeCommentaireVideo)) {
            $this->homeCommentaireVideos->removeElement($homeCommentaireVideo);
            // set the owning side to null (unless already changed)
            if ($homeCommentaireVideo->getVideo() === $this) {
                $homeCommentaireVideo->setVideo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HomeReplayVideo[]
     */
    public function getHomeReplayVideos(): Collection
    {
        return $this->homeReplayVideos;
    }

    public function addHomeReplayVideo(HomeReplayVideo $homeReplayVideo): self
    {
        if (!$this->homeReplayVideos->contains($homeReplayVideo)) {
            $this->homeReplayVideos[] = $homeReplayVideo;
            $homeReplayVideo->setVideo($this);
        }

        return $this;
    }

    public function removeHomeReplayVideo(HomeReplayVideo $homeReplayVideo): self
    {
        if ($this->homeReplayVideos->contains($homeReplayVideo)) {
            $this->homeReplayVideos->removeElement($homeReplayVideo);
            // set the owning side to null (unless already changed)
            if ($homeReplayVideo->getVideo() === $this) {
                $homeReplayVideo->setVideo(null);
            }
        }

        return $this;
    }
}
