<?php

namespace App\Form;

use App\Entity\PlayTopSingle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class PlayTopSingleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description', CKEditorType::class)
            ->add('fichierPhoto', FileType::class)
            ->add('single1')
            ->add('single2')
            ->add('single3')
            ->add('single4')
            ->add('single5')
            ->add('single6')
            ->add('single7')
            ->add('single8')
            ->add('single9')
            ->add('single10')
            ->add('auteur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlayTopSingle::class,
        ]);
    }
}
