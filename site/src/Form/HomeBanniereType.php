<?php

namespace App\Form;

use App\Entity\HomeBanniere;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class HomeBanniereType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description')
            ->add('lien')
            ->add('fichierPhoto', FileType::class)
            ->add('etat', ChoiceType::class, [
                'choices'  => [
                    'Publiée' => true,
                    'Non publiée' => false,
                    
                ],
               ])
               ->add('random')   
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => HomeBanniere::class,
        ]);
    }
}
