<?php

namespace App\Form;

use App\Entity\NewsActualite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class NewsActualiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description')
            ->add('contenu1', CKEditorType::class)
            ->add('fichierPhoto1', FileType::class)
            ->add('contenu2', CKEditorType::class)
            ->add('fichierPhoto2', FileType::class)
            ->add('etat', ChoiceType::class, [
                'choices'  => [
                    'Publiée' => true,
                    'Non publiée' => false,
                    
                ],
               ])
            ->add('dateDePublication', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('newCategorieActualite')
            ->add('Play_single')
            ->add('play_video')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NewsActualite::class,
        ]);
    }
}
