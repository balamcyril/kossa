<?php

namespace App\Form;

use App\Entity\PlayTopVideo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PlayTopVideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description', CKEditorType::class)
            ->add('fichierPhoto', FileType::class)
            ->add('video1')
            ->add('video2')
            ->add('video3')
            ->add('video4')
            ->add('video5')
            ->add('video6')
            ->add('video7')
            ->add('video8')
            ->add('video9')
            ->add('video10')
            ->add('auteur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlayTopVideo::class,
        ]);
    }
}
