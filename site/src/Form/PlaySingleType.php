<?php

namespace App\Form;

use App\Entity\PlaySingle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PlaySingleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    'Gratuit' => 'gratuit',
                    'Payant' => 'payant',
                    
                ],
               ])
            ->add('prix')
                 ->add('album')
            ->add('fichierSingleAudio', FileType::class)
            ->add('extraitSingleAudio', FileType::class)
            ->add('fichierPhoto', FileType::class)
                
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PlaySingle::class,
        ]);
    }
}
