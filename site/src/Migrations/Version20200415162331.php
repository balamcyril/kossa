<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200415162331 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE home_favorie_genre_musicaux ADD genre_musicaux_id INT NOT NULL');
        $this->addSql('ALTER TABLE home_favorie_genre_musicaux ADD CONSTRAINT FK_54C1BA41D21AAC2D FOREIGN KEY (genre_musicaux_id) REFERENCES play_genre_musicaux (id)');
        $this->addSql('CREATE INDEX IDX_54C1BA41D21AAC2D ON home_favorie_genre_musicaux (genre_musicaux_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE home_favorie_genre_musicaux DROP FOREIGN KEY FK_54C1BA41D21AAC2D');
        $this->addSql('DROP INDEX IDX_54C1BA41D21AAC2D ON home_favorie_genre_musicaux');
        $this->addSql('ALTER TABLE home_favorie_genre_musicaux DROP genre_musicaux_id');
    }
}
