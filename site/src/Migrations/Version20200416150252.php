<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200416150252 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE home_jaime_video (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT NOT NULL, video_id INT NOT NULL, id_temporaire INT NOT NULL, date_enreg DATETIME NOT NULL, INDEX IDX_7F885B97FB88E14F (utilisateur_id), INDEX IDX_7F885B9729C1004E (video_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE home_jaime_video ADD CONSTRAINT FK_7F885B97FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES home_utilisateur (id)');
        $this->addSql('ALTER TABLE home_jaime_video ADD CONSTRAINT FK_7F885B9729C1004E FOREIGN KEY (video_id) REFERENCES play_video (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE home_jaime_video');
    }
}
