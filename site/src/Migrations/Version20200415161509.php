<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200415161509 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE home_favorie_artiste (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT NOT NULL, artiste_id INT NOT NULL, ide_temporaire INT NOT NULL, date DATE NOT NULL, INDEX IDX_616506E1FB88E14F (utilisateur_id), INDEX IDX_616506E121D25844 (artiste_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE home_favorie_artiste ADD CONSTRAINT FK_616506E1FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES home_utilisateur (id)');
        $this->addSql('ALTER TABLE home_favorie_artiste ADD CONSTRAINT FK_616506E121D25844 FOREIGN KEY (artiste_id) REFERENCES play_artiste (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE home_favorie_artiste');
    }
}
