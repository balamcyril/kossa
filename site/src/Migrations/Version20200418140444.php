<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200418140444 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE home_jaime_pas_single ADD single_id INT NOT NULL');
        $this->addSql('ALTER TABLE home_jaime_pas_single ADD CONSTRAINT FK_B0E580FBE7C1D92B FOREIGN KEY (single_id) REFERENCES play_single (id)');
        $this->addSql('CREATE INDEX IDX_B0E580FBE7C1D92B ON home_jaime_pas_single (single_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE home_jaime_pas_single DROP FOREIGN KEY FK_B0E580FBE7C1D92B');
        $this->addSql('DROP INDEX IDX_B0E580FBE7C1D92B ON home_jaime_pas_single');
        $this->addSql('ALTER TABLE home_jaime_pas_single DROP single_id');
    }
}
