<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200416143102 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE home_jaime_genre_musicaux (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT NOT NULL, genre_musicaux_id INT NOT NULL, id_temporaire INT NOT NULL, date_enreg DATETIME NOT NULL, INDEX IDX_3732211CFB88E14F (utilisateur_id), INDEX IDX_3732211CD21AAC2D (genre_musicaux_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE home_jaime_genre_musicaux ADD CONSTRAINT FK_3732211CFB88E14F FOREIGN KEY (utilisateur_id) REFERENCES home_utilisateur (id)');
        $this->addSql('ALTER TABLE home_jaime_genre_musicaux ADD CONSTRAINT FK_3732211CD21AAC2D FOREIGN KEY (genre_musicaux_id) REFERENCES play_genre_musicaux (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE home_jaime_genre_musicaux');
    }
}
