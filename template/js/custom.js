$(function(){
      
    var lct = false;
    var lctv = false;
    //les 3 variable qui suit sont pour le chargement et la lecture de la musique
    var master=[];
    var Plist;
    var vplay = true;
    var insca = 0; 
    var inscb = 0; 
    var inscc = 0; 

    $('.tolltip-cnt').hover(function(){
      $(this).next('.tolltip').text( $(this).attr('data-ttle')).toggleClass('d-none bounceInDown2');
      var t1 = $(this).parent().outerWidth()/2;
      var t2 = $(this).next('.tolltip').outerWidth()/2;
      var t3 = t2-t1;
      $('.tolltip').css({left:-t3+'px'});
    });
    $('.une-news').mouseover(function(){  
      $(this).find('.couv').addClass('couv-news');
    }).mouseout(function(){
      $(this).find('.couv').removeClass('couv-news');
    });
    // modif
    $('.btn-cate').removeClass('waves-effect');
    $('.btn-cate').click(function(e){ 
      e.stopPropagation(); 
      $('.lst-elm-lng').addClass('d-none').removeClass('bounceInDown2');
      $('.lst-elm-men').addClass('d-none ').removeClass('bounceInDown2'); 
      $('.artiste-share').addClass('d-none ').removeClass('bounceInDown2');
      $(this).toggleClass('ovv ');
      $('.lst-elm-cate').toggleClass('d-none bounceInDown2');
    });
    $('.elm-cate').click(function(){  
      $('.chx-cate').text($(this).text());
    });

    

    $('.btn-lng').click(function(e){ 
      e.stopPropagation(); 
      $('.lst-elm-cate').addClass('d-none ').removeClass('bounceInDown2'); 
      $('.lst-elm-men').addClass('d-none ').removeClass('bounceInDown2'); 
      $('.artiste-share').addClass('d-none ').removeClass('bounceInDown2');
      $(this).toggleClass('ovv');
      $('.lst-elm-lng').toggleClass('d-none bounceInDown2');
    });
    $('.btn-men').click(function(e){ 
      e.stopPropagation(); 
      $('.lst-elm-men').addClass('d-none ').removeClass('bounceInDown2');
      $('.lst-elm-cate').addClass('d-none ').removeClass('bounceInDown2');  
      $('.artiste-share').addClass('d-none ').removeClass('bounceInDown2');
      $(this).toggleClass('ovv');
      $('.lst-elm-men').toggleClass('d-none bounceInDown2');
    });
    $('.btn-artiste-share').click(function(e){ 
      e.stopPropagation(); 
      $('.lst-elm-men').addClass('d-none ').removeClass('bounceInDown2');
      $('.lst-elm-cate').addClass('d-none ').removeClass('bounceInDown2');  
      $('.lst-elm-lng').addClass('d-none').removeClass('bounceInDown2');
      $(this).toggleClass('ovv');
      $(this).parent().find('.artiste-share').toggleClass('d-none bounceInDown2');
    });
    $('.elm-lng').click(function(){  
      $('.chx-lng').text($(this).text());
    });

    
    $('.a-play-cont').mouseover(function(){  
      $(this).find('.couv').addClass('couv-play');
      $(this).find('.name-art').addClass('cl-r1');
    }).mouseout(function(){
      $(this).find('.couv').removeClass('couv-play');
      $(this).find('.name-art').removeClass('cl-r1');
    });

    $('.a-play-cont').mouseenter(function(e){
      $(this).find('.time-play').stop(true);
      $(this).find('.time-play').velocity({
        top:[100,160],
        opacity:[1,0],
      },{
        duration:300,
        display:'block',
      })
    }).mouseleave(function(e){
      $(this).find('.time-play').velocity('reverse')
    });
    

    $('.play-playlist').slick({
      speed: 650,
      cssEase: 'linear',
      slidesToScroll: 1,
      autoplay: true,
      slidesToShow: 1,
      autoplaySpeed: 2400,
      arrows:false,
      infinite:false,
      lazyLoad:'progressive',
      variableWidth: true,
      responsive: [{

        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          infinite: true
        }
  
      },{
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          infinite: true
        }
  
      }, {
  
        breakpoint: 630,
        settings: {
          slidesToShow: 2,
          infinite:true
        }
  
      }, {
  
        breakpoint: 496,
        settings: {
          infinite:true
        }
  
      }]
    });

    $('.carousel-playlist').slick({
      infinite: true,
      speed: 650,
      cssEase: 'linear',
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      arrows:false,
      lazyLoad:'progressive',
    });

    $('.videotop-playlist').slick({
      infinite: true,
      speed: 650,
      cssEase: 'linear',
      slidesToScroll: 1,
      rows:2,
      slidesPerRow:1,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows:false,
      dots:true,
      vertical:true
    });

    $('.play-makossa').slick({
      infinite: true,
      speed: 850,
      cssEase: 'linear',
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      arrows:false,
      lazyLoad:'ondemand',
      variableWidth: true
    });

    $('.art-semain').slick({
      infinite: true,
      speed: 850,
      cssEase: 'linear',
      slidesToShow:1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      arrows:false,
      lazyLoad:'ondemand',
      variableWidth: true
    });

    $('.inscpt').slick({
      speed: 300,
      cssEase: 'ease-in-out',
      slidesToShow:1,
      slidesToScroll: 1,
      arrows:false,
      variableWidth: false,
      draggable:false,
      infinite:false,
    });
    $('.prev-insc').click(function(e){
      e.preventDefault();
      $('.inscpt').slick('slickPrev');
    });
    $('.next-insc').click(function(e){
      e.preventDefault(); 
      $('.inscpt').slick('slickNext');
    });
    $('.prev-mako').click(function(){
      $('.play-makossa').slick('slickPrev');
    });
    $('.next-mako').click(function(){
      $('.play-makossa').slick('slickNext');
    });
    $('.prev-playlist').click(function(){
      $('.play-playlist').slick('slickPrev');
    });
    $('.next-playlist').click(function(){
      $('.play-playlist').slick('slickNext');
    });

    $('.prev-car-playlist').click(function(){
      $('.carousel-playlist').slick('slickPrev');
    });
    $('.next-car-playlist').click(function(){
      $('.carousel-playlist').slick('slickNext');
    });


    $('[data-toggle="tooltip"]').tooltip();

    $('.tp-lst').hover(function(){
      $(this).find('.tp-act').toggleClass('d-none');
    });

    $('body').click(function(){
      $('.lst-elm-cate').addClass('d-none ').removeClass('bounceInDown2');
      $('.lst-elm-lng').addClass('d-none ').removeClass('bounceInDown2');
      $('.lst-elm-men').addClass('d-none ').removeClass('bounceInDown2');
      $('.artiste-share').addClass('d-none ').removeClass('bounceInDown2');
      $('.cont-plst').addClass('d-none ').removeClass('bounceInDown2');
    });

    $('.bnt-close').click(function(e){
      e.preventDefault();
      if(lct){
        $('.lecteur').addClass('bounceOutRight').removeClass('bounceInRight');
        $('.lecteur-img').removeClass('siut').addClass('sout');
        lct = false;
      }
      return;
    });

    $('.bnt-close-v').click(function(e){
      e.preventDefault();
      if(lctv){
        $('.lecteur-video').addClass('bounceOutRight').removeClass('bounceInRight');
        $('.lecteur-img').removeClass('siut').addClass('sout');
        lctv = false;
      }
      return;
    });

    $('.lecteur-play-video').click(function(e){
      e.preventDefault();
      if(!lctv){
        $('.lecteur-video').addClass('bounceInRight').removeClass('lecteur-out bounceOutRight');
        $('.lecteur-img').removeClass('sout').addClass('siut');
        lctv = true;
      }
    });

    $('.bnt-plyay-mus').click(function(e){

      // On charge la musique ici. Tjrs sous forme tableau d'objet comme suit mm si c'est une seule musique
      master = [
        {
            title:"Cro Magnon Man j",
            artist:"Stanley enow san",
            mp3:"music/5SecondsofSummer.mp3",
            poster: "img/enow.jpg"
        },
        {
            title:"Thin Ice",
            artist:"Serge benow",
            mp3:"music/NF-LetYouDown.mp3",
            poster: "img/enow4.jpg"
        }
      ];
      
      e.preventDefault();
      if(!lct){
        $('.lecteur').addClass('bounceInRight').removeClass('lecteur-out bounceOutRight');
        $('.lecteur-img').removeClass('sout').addClass('siut');
        lct = true;
      }

      // la fonction qui lance la music
      goMusic();

    });   


    
    //ce qui suit jusqu'au prochain commentaire n'est qu'un exemple à supprimer lors de l'implémentation
    $('.bnt-plyay-mus1').click(function(e){
      master =[{
          title:"Cro Magnon Man",
          artist:"Stanley enow san",
          mp3:"music/5SecondsofSummer.mp3",
          poster: "img/enow4.jpg"
      }];
      e.preventDefault();
      if(!lct){
      $('.lecteur').addClass('bounceInRight').removeClass('lecteur-out bounceOutRight');
      $('.lecteur-img').removeClass('sout').addClass('siut');
      lct = true;
      }
      goMusic();
      }); 
      $('.bnt-plyay-mus2').click(function(e){
      master = [{
          title:"Thin Ice",
          artist:"Serge benow",
          mp3:"music/NF-LetYouDown.mp3",
          poster: "img/enow.jpg"
      }];
      e.preventDefault();
      if(!lct){
      $('.lecteur').addClass('bounceInRight').removeClass('lecteur-out bounceOutRight');
      $('.lecteur-img').removeClass('sout').addClass('siut');
      lct = true;
      }
      goMusic();
      }); 
      //ce qui est au dessus jusqu'au prochain commentaire n'est qu'un exemple à supprimer lors de l'implémentation


    $('.btn-plst').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.cont-plst').addClass('d-none ').removeClass('bounceInDown2');
      $(this).next().html($('#laplaylist').html()).removeClass('d-none').addClass('bounceInDown2');
      //$(this).next().removeClass('d-none').addClass('bounceInDown2');
    });  
    $('.btn-plst2').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.cont-plst').addClass('d-none ').removeClass('bounceInDown2');
      $(this).parent().next().html($('#laplaylist').html()).removeClass('d-none').addClass('bounceInDown2');
      // $(this).parent().next().removeClass('d-none').addClass('bounceInDown2');
    });
    $('.cont-plst').on('click','.add-plst',function(){
      $('.cont-plst').addClass('d-none').removeClass('bounceInDown2');
      $inp = $(this).parent().prev();
      //$(this).parent().parent().prev().append('<div class="elt-plst text-left pl-1 pt-2 pb-2 cur-pointer">'+$inp.val()+'</div>');
      $('#laplaylist .lst-nbr-plst').append('<div class="elt-plst text-left pl-1 pt-2 pb-2 cur-pointer">'+$inp.val()+'</div>');
      newplst($inp.parent().parent().attr('alb-sng'),$inp.val());
    });
    $('.cont-tik').on('keyup','.inp-tk',function(){
      //pour concert.1 
      //var bs=$(this).parent().parent().parent().parent().attr('data-prix');
      //$prx=$(this).parent().parent().find('.prx');
      $px=$('.prx');
      var prx = $('.prx-ori').text();
      $px.text( parseInt(prx)*parseInt($(this).val()) || 0);
    });
    
    $('.cont-plst').on('click','.lst-nbr-plst .elt-plst',function(){
      $('.cont-plst').addClass('d-none').removeClass('bounceInDown2');
      newplst($(this).parent().parent().attr('alb-sng'),$(this).text());
    });

    $('.ret-plst').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.cont-all-plst').append('<div class="black info-playlist p-lst p-3 z-depth-3 position-fixed animated dr-025 text-right text-white ls3 delay-out"><i class="fa fa-trash mr-2"></i>Le morceau à été retirer de la playlist.</div>');
    });
    $('.valid-vote').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.cont-addr').removeClass('siut').addClass('sout d-none');
      $('.cont-all-plst').append('<div class="black info-vote p-lst p-3 z-depth-3 position-fixed animated text-right text-white ls3 fadeInUp dr-03"><i class="fa fa-thumbs-up mr-2 text-success"></i> votre vote à été enregistrer. <br><span class="fs-12 text-white-50">NB: Le premier uniquement sera prix en compte.</span><div class="text-right"> <button type="button" class="btn btn-outline-success btn-sm ok-vote">ok</button> </div></div>');
    });
    $('.valid-part').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.cont-addr').removeClass('siut').addClass('sout d-none');
      $('.cont-all-plst').append('<div class="black info-part p-lst p-3 z-depth-3 position-fixed animated text-right text-white ls3 fadeInUp dr-03"><i class="fa fa-thumbs-up mr-2 text-success"></i> votre demande à été pris en compte. <br><span class="fs-12 text-white-50">Un mail vous sera envoyé.</span><div class="text-right"> <button type="button" class="btn btn-outline-success btn-sm ok-vote">ok</button> </div></div>');
    });
    $('.cont-all-plst').on('click','.ok-vote',function(){
      $('.info-vote , .info-part').addClass('d-none');
    });
    $('.cont-plst').click(function(e){
      e.stopPropagation();
    });

    $('.btn-cate-ticket').removeClass('waves-effect');
    $('.btn-cate-ticket').click(function(e){ 
      e.preventDefault();
      e.stopPropagation(); 
      $('.lst-elm-lng').addClass('d-none').removeClass('bounceInDown2');
      $('.lst-elm-men').addClass('d-none ').removeClass('bounceInDown2'); 
      $('.artiste-share').addClass('d-none ').removeClass('bounceInDown2');
      $('.lst-elm-cate').addClass('d-none ').removeClass('bounceInDown2');
      $('.lst-elm-ticket').addClass('d-none ').removeClass('bounceInDown2');
      $(this).toggleClass('ovv ');
      $(this).next('.lst-elm-ticket').toggleClass('d-none bounceInDown2');
    });
    $('.elm-ticket').click(function(e){  
      e.preventDefault();
      e.stopPropagation();
      $(this).parent().addClass('d-none ').removeClass('bounceInDown2');
      $(this).parent().parent().addClass('d-none b-tkt');
      //new
      var prx = $(this).parent().attr('data-prix');
      $('.cont-tik').removeClass('d-none sout').addClass('siut');
      $('.cont-tik').find('.prx').text(parseInt(prx)*$('.inp-tk').val() || 0);
      $('.prx-ori').text(prx);  
      $('.cont-tik').find('.tpticket').text($(this).text());
      //pour concert.1
      // $prt = $(this).parent().parent().parent().prev();
      // $prt.addClass('cont-ti').removeClass('d-none');
      // $prt.find('.formtk-out').removeClass('d-none sout').addClass('siut t-ouv');
      // $prt.find('.tpticket').text($(this).text());
    });
    $('.bnt-closet').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      // pr cncert.1 // $('.t-ouv').removeClass('siut t-ouv').addClass('sout');
      $('.b-tkt').removeClass('d-none b-tkt').addClass('fadeIn');
      $('.cont-tik , .cont-addr').removeClass('siut').addClass('sout d-none');
      // pr cncert.1 // $('.cont-ti').removeClass('cont-ti').addClass('d-none');
    });
    $('.btn-participe,.btn-vote').click(function(){
      $('.cont-addr').removeClass('d-none sout').addClass('siut');
    });
    $('.btn-ret-favo').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      var elm=$(this).attr('data-favo');
      retfavo(elm);
    })
    // //
    // $('.tpm').click(function(){
    //   $(this).removeClass('tpm').addClass('tpmc');
    // });
    $('.elm-tpm').on('click','.tpmc,.tpm',function(){
      $(this).toggleClass('tpm tpmc').find('.slect').toggleClass('proov');
      $chkb = $(this).find('.hkbox');
      
      if($chkb.is(':checked')){
        $chkb.attr('checked',false);
        if(insca>0)
          insca = insca-1;
      }
      else{
        $chkb.attr('checked',true);
        insca = insca+1; 
      }

      if(insca>=1)
        $('.sv-tpm').removeClass('disabled');
      else
        $('.sv-tpm').addClass('disabled');

    });
    $('.elm-gdm').on('click','.gdmc,.gdm',function(){
      $(this).toggleClass('gdm gdmc').find('.slect').toggleClass('proov');
      $chkb = $(this).find('.hkbox');
      
      if($chkb.is(':checked')){
        $chkb.attr('checked',false);
        if(inscb>0)
          inscb = inscb-1;
      }
      else{
        $chkb.attr('checked',true);
        inscb = inscb+1; 
      }

      if(inscb>=3)
        $('.sv-gdm').removeClass('disabled');
      else
        $('.sv-gdm').addClass('disabled');

    });

    $('.elm-gda').on('click','.gdac,.gda',function(){
      $(this).toggleClass('gda gdac').find('.slta').toggleClass('d-none d-inline-block');
      $(this).find('.content-mus-vi').toggleClass('brd-bl brd-vt');
      $chkb = $(this).find('.hkbox');
      
      if($chkb.is(':checked')){
        $chkb.attr('checked',false);
        if(inscc>0)
          inscc = inscc-1;
      }
      else{
        $chkb.attr('checked',true);
        inscc = inscc+1; 
      }

      if(inscc>=3)
        $('.sv-gda').removeClass('disabled');
      else
        $('.sv-gda').addClass('disabled');

    });

    function newplst(elm,plst){
      $('.cont-all-plst').append('<div class="info-playlist z-depth-3 position-fixed p-3 animated p-lst dr-025 text-right text-white ls3 delay-out">'+
    '<span class="nm-add-plst">'+elm+'</span> à été ajouter à la playlist <br>'+ 
    '<span class="nm-plst">'+plst+'</span></div>');
    }
    function retfavo(elm){
      $('.cont-all-plst').append('<div class="info-playlist z-depth-3 position-fixed p-3 animated p-lst dr-025 text-right text-white ls3 delay-out">'+
    '<i class="fa fa-thumbs-up mr-2 text-success"></i><span class="nm-add-plst">'+elm+'</span> à été retirer du favorie. </div>');
    }

    //tous ce qui suit concerne le lecteur et plus haut aussi sur l'utilisation des "bnt-plyay-mus"+(1 et 2 des exemples)
    Plist = new jPlayerPlaylist({
      jPlayer: "#open-player",
      cssSelectorAncestor: "#lecteur",
          
    },master, {
      playlistOptions:{
          autoPlay:true,
          enableRemoveControls:true,
          loopOnPrevious:true,
          shuffleOnLoop:true,
      },
      swfPath: "music",
      supplied: "mp3",
      cssSelector:{
      play:".lct-btn-play-mus", 
      seekBar:".seek-bar",
      playBar:".play-bar",
      duration:".duree-mus",
      currentTime:".temps-mus",
      mute:".vol-mute",
      volumeBar:".volume-bar",
      volumeBarValue:".volume-bar-value",
      stop:".lct-btn-stop-mus, .bnt-close",
      repeat:".repeter-mus"
      },
      
      wmode: "window",
      volume:0.2, 
      useStateClassSkin: true,
      autoBlur: false,
      smoothPlayBar: true,
      keyEnabled: true,
      remainingDuration: true,
      toggleDuration: true
      });
        
    $('.lct-btn-next-mus').click(function(){
      $('.lct-btn-play-mus').addClass('cl-v1').html('<i class="fa fa-play fs-12"></i>');
      $('.lct-btn-stop-mus').removeClass('cl-v1');
      vplay=true;
      Plist.next();
      setPlay();
    }); 
    $('.lct-btn-prev-mus').click(function(){
      $('.lct-btn-play-mus').addClass('cl-v1').html('<i class="fa fa-play fs-12"></i>');
      $('.lct-btn-stop-mus').removeClass('cl-v1');
      vplay=true;
      Plist.previous();
      setPlay();
    });  
    $('.lct-btn-list-mus').click(function(){
      $('.jp-type-playlist').toggleClass('d-none');
    });
    $('.lct-btn-list-mus, .repeter-mus').click(function(){
        $(this).toggleClass('cl-v1');
    });
    $('.lct-btn-stop-mus').click(function(){
        $(this).toggleClass('cl-v1');
        $('.lct-btn-play-mus').html('<i class="fa fa-play fs-12"></i>');
        if(vplay)
          vplay=false;

        $('.lct-btn-play-mus').removeClass('cl-v1');
    });
    $('.lct-btn-play-mus').click(function(){
      if(vplay){
        $(this).html('<i class="fa fa-pause fs-12"></i>');
        vplay=false;}
      else{
        $(this).html('<i class="fa fa-play fs-12"></i>');
        vplay=true;
      }
      $(this).toggleClass('cl-v1');
      $('.lct-btn-stop-mus').removeClass('cl-v1');
    });

    function setPlay(){
      $('.lecteur-img').css("background-image","url('"+Plist.original[Plist.current].poster+"')");
      $('.titre-mus').empty().append(Plist.original[Plist.current].title);
      $('.artiste-mus').empty().append(Plist.original[Plist.current].artist);
    }
    function goMusic(){
      Plist.setPlaylist(master);
      $('.lct-btn-stop-mus').removeClass('cl-v1');
      $('.lct-btn-play-mus').addClass('cl-v1');
      setPlay()
    }
    
  });